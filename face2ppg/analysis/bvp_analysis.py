import os
import numpy as np
from scipy.signal import find_peaks, stft, lfilter, butter, welch
from plotly.subplots import make_subplots
from plotly.colors import n_colors
import plotly.graph_objects as go
from scipy.interpolate import interp1d
from scipy import signal as sg
from numpy import linalg as lg
import pandas as pd



class BVPSignal:
    """
        Manage (multi-channel, row-wise) BVP signals
    """
    nFFT = 4096  # freq. resolution for STFTs
    step = 1  # step in seconds
    loadComputedGTHR = True

    def __init__(self, data, fs, filename=None, channels=1, startTime=0, minHz=0.75, maxHz=4., verb=False, win_size=None, f_method="welch"):

        self.data = data
        self.num_channels = channels
        self.fs = fs  # sample rate
        self.startTime = startTime
        self.verb = verb
        self.minHz = minHz
        self.maxHz = maxHz
        self.filename = filename
        self.win_size = int(win_size)
        self.times = []
        self.bpm = []
        self.Pfreqs = []
        self.Power = []

        if f_method == "welch":
            self.storedComputedGTHR = "computedGTHR_WELCH_" + str(self.win_size) + ".csv"
        elif f_method == "arcovar":
            self.storedComputedGTHR = "computedGTHR_ARCOVAR_" + str(self.win_size) + ".csv"
        else:
            self.storedComputedGTHR = "computedGTHR_MUSIC_" + str(self.win_size) + ".csv"

        self.existGTHR = False
        self.computedGTHR = pd.DataFrame()
        self.f_method = f_method


    def get_gt_hr(self):
        """ Check if csv file with HEART RATE signal exists"""

        # -- check if cropped faces already exists on disk
        path, name = os.path.split(self.filename)
        filenamez = path + '/' + self.storedComputedGTHR
        # print("Tino in Video file: {}, {}, {}".format(path, name, filenamez))

        # -- if compressed exists... load it
        stored = False
        if self.loadComputedGTHR and os.path.isfile(filenamez):
            stored = True
            self.computedGTHR = pd.read_csv(filenamez, index_col=False)

        self.existGTHR = stored
        return stored

    def get_gt_hr_data(self):
        """ Check if csv file with RGB signals exists"""
        self.times = self.computedGTHR["timesGT"]
        self.bpm = self.computedGTHR["bpmGT"]
        return self.computedGTHR["bpmGT"], self.computedGTHR["timesGT"]


    def get_bpm(self, timestep=1.0, startTimeInput=None, endTimeInput=None, dataset_fps=30, save_data=False):

        if startTimeInput is None:
            startTime = 0.0
        else:
            startTime = startTimeInput

        bpmGT = []
        timesGT = []
        if endTimeInput is None:
            endTime = len(self.data)/self.fs
        else:
            endTime = endTimeInput
        print("     >> [BVPSignal - getBPM] EndTime:{}, fs:{}, WinSize:{}".format(endTime, self.fs, self.win_size))


        mydata = self.data
        time_raw_signal = len(mydata)/self.fs
        samples_down = time_raw_signal*dataset_fps
        samples_norm = time_raw_signal*self.fs


        print("     >> [BVPSignal - getBPM] Length of GT BVP signal at {} HZ is: {} ({})".format(self.fs, len(mydata), samples_norm))
        print("     >> [BVPSignal - getBPM] Length for trans GT BVP signal at {} HZ is: {}.({})".format(dataset_fps, self.fs/dataset_fps, samples_down))
        print("     >> [BVPSignal - getBPM] Lengths check: {} vs {}".format(len(mydata), np.arange(0, len(mydata)).shape))

        mydata = np.interp(np.arange(0, len(mydata), self.fs/dataset_fps), np.arange(0, len(mydata)), mydata)
        self.fs = dataset_fps
        print("     >> [BVPSignal - getBPM] Length of GT BVP signal at {} HZ is: {}.({})".format(self.fs, len(mydata), samples_down))


        if startTime == 0 or startTime < self.win_size/2:
            T = int(self.win_size/2)
        else:
            T = startTime  # times where bpm are estimated

        RADIUS = self.win_size / 2



        while T < endTime:
            endSample = np.min([int(endTime * self.fs), int((T + RADIUS) * self.fs)])
            # startTime = np.max([initial_startTime, T - RADIUS])
            startTime = T - RADIUS
            startSample = int(startTime*self.fs)

            samplesSubset = mydata[startSample:endSample]
            # print("First info in the Loop: T:{}, startSample:{}, endFrame:{}, endTime:{}, Lseg:{}".format(T, startSample, endSample, endTime, len(mydata)))

            bpm = self.PSD2BPM(samplesSubset, chooseBest=True)

            # -- save the estimate
            bpmGT.append(bpm[0])
            timesGT.append(T)

            # -- define the frame range for each time step
            T += timestep

        self.computedGTHR["bpmGT"] = np.array(bpmGT)
        self.computedGTHR["timesGT"] = np.array(timesGT)

        if save_data:
            path, name = os.path.split(self.filename)
            filenamez = path + '/' + self.storedComputedGTHR
            self.computedGTHR.to_csv(filenamez, index=False, header=True)
        self.bpm = np.array(bpmGT)
        self.times = np.array(timesGT)
        return np.array(bpmGT), np.array(timesGT)


    def PSD2BPM(self, data, chooseBest=True):
        """
            Compute power spectral density using Welch’s method and estimate
            BPMs from video frames
        """

        # -- interpolation for less than 256 samples
        c = 1
        n = len(data)
        if n < 256:
            seglength = n
            overlap = int(0.8 * n)  # fixed overlapping
        else:
            seglength = 256
            overlap = 200

        if self.f_method == "welch":
            # -- periodogram by Welch
            F, P = welch(data, nperseg=seglength, noverlap=overlap, window='hamming', fs=self.fs, nfft=self.nFFT * 2)
        elif self.f_method == "arcovar":
            print(">>>>>>>>>>>>> Method not available <<<<<<<<<<<<<<<<")
            # ar_values, error = arcovar(self.data[0], 50)
            # PSD = arma2psd(ar_values, NFFT=self.nFFT, T=self.fs)
            # PSD = PSD[len(PSD):len(PSD) // 2:-1]
            # P = np.array([PSD])
            # F = np.linspace(0, self.fs / 2, len(PSD))
            # P = P[0]
        else:
            F, P = welch(data, nperseg=seglength, noverlap=overlap, window='hamming', fs=self.fs, nfft=self.nFFT * 2)


        # -- freq subband (0.75 Hz - 4.0 Hz)
        band = np.argwhere((F > self.minHz) & (F < self.maxHz)).flatten()
        self.Pfreqs = 60 * F[band]
        self.Power = P[band]

        if chooseBest:
            winner = 0
            self.Power = self.Power.reshape(1, -1)

        # -- BPM estimate by PSD
        Pmax = np.argmax(self.Power, axis=1)  # power max
        self.bpm = self.Pfreqs[Pmax]  # freq max
        # print("    > PSD2BPM2 Tino 5. Pmax:{}, self.bpm:{}, self.Pfreqs[Pmax]:{}".format(Pmax, self.bpm, self.Pfreqs[Pmax]))

        return self.bpm
