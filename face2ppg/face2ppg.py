import os
import sys
from os.path import join

import cv2
import numpy as np
import pandas as pd

sys.path.insert(0, os.path.abspath('..'))

from face2ppg.face.face_detection import FaceDetector
from face2ppg.face.face_landmarks_detection import FaceLandmarksDetector
import glob
from datetime import datetime
from face2ppg.segmentation.dnn_skin_detection import skinDetector
from face2ppg.plotting.VHRTPlotter import RT2DPlotter
from scipy import signal
from face2ppg.helpers.common import *
import face2ppg.analysis.spectral_analysis as sa
from face2ppg.extraction.pos_method import pos_method
from face2ppg.extraction.lgi_method import lgi_method
from face2ppg.extraction.chrom_method import chrom_method
from face2ppg.extraction.omit_method import omit_method
from face2ppg.filtering.filtering import Filtering
import ffmpeg


def check_rotation(path_video_file):
    # this returns meta-data of the video file in form of a dictionary
    meta_dict = ffmpeg.probe(path_video_file)

    # from the dictionary, meta_dict['streams'][0]['tags']['rotate'] is the key
    # we are looking for
    # print(meta_dict['streams'][0]['tags'])

    rotateCode = None
    if 'tags' in meta_dict['streams'][0]:
        if 'rotate' in meta_dict['streams'][0]['tags']:
            # print("YES")
            if int(meta_dict['streams'][0]['tags']['rotate']) == -90:
                # print(f" Rotate 1: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                rotateCode = cv2.ROTATE_180
            elif int(meta_dict['streams'][0]['tags']['rotate']) == 180:
                # print(f" Rotate 2: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                # rotateCode = cv2.ROTATE_180
                rotateCode = cv2.ROTATE_90_CLOCKWISE
            elif int(meta_dict['streams'][0]['tags']['rotate']) == 270:
                # print(f" Rotate 3: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                # rotateCode = cv2.ROTATE_90_COUNTERCLOCKWISE
                rotateCode = cv2.ROTATE_180

            print(f"    >> Video Rotation needed: {meta_dict['streams'][0]['tags']['rotate']}")
        else:
            print(f"    >> No video Rotation needed...")
    else:
        print(f"    >> Video metadata not found...")

    return rotateCode


def overlay_image_alpha(img, img_overlay, pos):
    """Overlay img_overlay on top of img at the position specified by
    pos and blend using alpha_mask.

    Alpha mask must contain values within the range [0, 1] and be the
    same size as img_overlay.
    """

    x, y = pos

    # Image ranges
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

    # Overlay ranges
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

    # Exit if nothing to do
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return

    channels = img.shape[2]

    # alpha = alpha_mask[y1o:y2o, x1o:x2o]
    # alpha_inv = 1.0 - alpha

    for c in range(channels):
        img[y1:y2, x1:x2, c] = img_overlay[y1o:y2o, x1o:x2o, c]


def interweavingMerge(array_a, array_b):
    merged_array = np.empty((array_a.size + array_b.size,), dtype=array_a.dtype)
    merged_array[0::2] = array_a
    merged_array[1::2] = array_b
    return merged_array


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


class Face2PPG:

    def __init__(self, face_detector, landmarks_detector, triangles_number, skin_segmentation, save_video, show_output, format_results, save_results, multi_region,
                 number_of_regions=64, face_detector_confidence=0.7, ppg_method="POS", fps=30):
        self.frameRate = fps
        self.postfiltering = True  # Filter POS signal after converting
        self.triangles_number = triangles_number
        self.save_video = save_video
        self.show_output = show_output
        self.format_results = format_results
        self.save_results = save_results
        self.ppg_method = ppg_method
        self.multiregion = multi_region
        self.number_of_regions = number_of_regions
        ##########################################
        # Detectors settings and initializations
        ##########################################

        # Default method for face detector "DNN_TF_OCV" ( DNN TensorFlow int8 model)
        self.faceDetector = FaceDetector(fd_method=face_detector, conf_threshold=face_detector_confidence)

        # Default method for face landmarks detector "OpenCV LBF"
        self.faceLandmarksDetector = FaceLandmarksDetector(number_of_triangles=triangles_number, fld_method=landmarks_detector)

        # Skin detector
        self.detectorSkin = skinDetector(fss_method=skin_segmentation)

    def extract_ppg_from_face(self, video_path, input_frame_rate=None, video_index=0, total_number_of_videos=1):
        # Starting point
        video = video_path.split('/')
        print("    >> Starting to extract rPPG from video: " + str(video[-1]))
        print("    >> Video path: " + str(video_path))

        # For saving results in HDF5. TimeStamp. Note: It could be taken before saving the results.
        dateTime = datetime.now()
        timestampStr = dateTime.strftime("%d-%b-%Y_(%H:%M:%S.%f)")

        # Record data video. Containers for saving the information. Note: More efficient containers??
        df_signals = pd.DataFrame()
        face_detected = []
        frame_number = []
        r_raw_signal = []
        g_raw_signal = []
        b_raw_signal = []

        r_raw_signal_left = []
        g_raw_signal_left = []
        b_raw_signal_left = []

        r_raw_signal_right = []
        g_raw_signal_right = []
        b_raw_signal_right = []
        number_of_faces = []

        # Video Reader
        cap = cv2.VideoCapture(video_path)
        hasFrame, frame = cap.read()
        if not hasFrame:
            exit(-1)

        rotate_code = None
        rotate_code = check_rotation(video_path)

        main_height, main_width, channels_main = frame.shape
        length_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        # rotation_meta = int(cap.get(cv2.CAP_PROP_ORIENTATION_META))
        if input_frame_rate is not None:
            recording_fps = input_frame_rate
        else:
            recording_fps = cap.get(cv2.CAP_PROP_FPS)

        fs = recording_fps
        # Video Recorder.
        if self.save_video:
            savePath = '../data/results/video_output'
            if not os.path.exists(savePath):
                os.makedirs(savePath)
            vid_writer = cv2.VideoWriter("{}/output-fba-{}.mp4".format(savePath, str(video[-1]).split(".")[0]),
                                         cv2.VideoWriter_fourcc(*"mp4v"), int(20), (int(main_width), main_height))

        ##########################################
        # Detectors settings and initializations
        ##########################################
        # Landmark detector
        self.faceLandmarksDetector.setParameters(width=main_width, height=main_height, image_data_type=frame.dtype)

        # Filtering
        filtering = Filtering(fps=fs)

        # Color method
        if self.ppg_method == "POS":
            colorTransformation = pos_method(fps=fs)
        elif self.ppg_method == "CHROM":
            colorTransformation = chrom_method(fps=fs)
        elif self.ppg_method == "LGI":
            colorTransformation = lgi_method(fps=fs)
        elif self.ppg_method == "OMIT":
            colorTransformation = omit_method(fps=fs)
            self.postfiltering = False
        else:
            colorTransformation = pos_method(fps=fs)

        print(f"    >> Using: {self.ppg_method} as rPPG method...")

        # colorTransformation = pos_method(fps=fs)

        # Create a plotter class object. TODO: It needs be arranged according to the input resolution.
        if main_width > main_height:
            plot_width = main_width // 2
            plot_height = main_height // 4
            text_x_position = int(main_height * 0.02)
            text_y_position = int(main_height * 0.04)
        else:
            plot_width = main_height // 2
            plot_height = main_width // 4
            text_x_position = int(main_width * 0.02)
            text_y_position = int(main_width * 0.04)

        plotterRawPPG = RT2DPlotter(plot_width, plot_height, [3], number_of_signals=1, title='rPPG signal', index=1,
                                    axisLabels=['Seconds (s)', 'Amplitude'], colors=['blue'],
                                    signalLabels=['rPPG signal'], fps=fs)

        # General Variables
        frame_count = 0
        frame_count_detection = 0
        tt_FaceDetector = 0
        tt_FaceLandmarksDetector = 0
        tt_FaceSkinDetector = 0
        face_triangles_number = self.triangles_number
        window_analysis = 3  # seconds
        previous_rgb_values = [0, 0, 0]
        previous_rgb_left_values = [0, 0, 0]
        previous_rgb_rigth_values = [0, 0, 0]

        # Visual settings
        if self.show_output:
            cv2.namedWindow("Video Output", cv2.WINDOW_NORMAL)
            cv2.startWindowThread()

        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)

        #########################
        #       MAIN LOOP       #
        #########################

        while 1:

            if not hasFrame:
                break

            frame_number.append(frame_count)
            if rotate_code is not None:
                frame = cv2.rotate(frame, rotate_code)

            output_frame = frame.copy()  # Frame for drawing results
            cv2.putText(output_frame, "{}/{} processed frames.".format(frame_count, length_frames), (text_x_position, text_y_position * 4), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        (0, 0, 255),
                        1, cv2.LINE_AA)

            # Face Detection
            t1 = time.time()
            bboxes = self.faceDetector.detectFaces(frame)
            number_of_faces.append(len(bboxes))
            tt_FaceDetector += time.time() - t1
            fpsFaceDetector = frame_count / tt_FaceDetector

            label = self.faceDetector.getLabelFaceDetector(fpsFaceDetector)
            cv2.putText(output_frame, label, (text_x_position, text_y_position), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

            # Print face rectangles
            getFirstFace = True  # Temporal. Get only first face for normalization.
            for box in bboxes:
                if getFirstFace:
                    cv2.rectangle(output_frame, (box[0], box[1], box[2], box[3]), (0, 255, 0), int(round(output_frame.shape[0] * 0.001)))
                    faceRectImage, adaptedFaceRect = self.faceDetector.getFaceInRect(frame, box)
                    face_detected.append(int(1))
                    getFirstFace = False

            # Face Landmarks detection
            if len(bboxes) > 0:
                t2 = time.time()
                average_landmarks = np.zeros((68, 2))
                average_landmarks_factor = 1
                for i in range(average_landmarks_factor):
                    status, landmarks_s = self.faceLandmarksDetector.detectLandmarks(frame, bboxes)
                    average_landmarks = average_landmarks + landmarks_s[0][0]
                average_landmarks = average_landmarks / average_landmarks_factor

                landmarks = np.array([average_landmarks])

                tt_FaceLandmarksDetector += time.time() - t2
                fpsFaceLandmarksDetector = frame_count / tt_FaceLandmarksDetector
                label = self.faceLandmarksDetector.getLabelFaceLandmarksDetector(fpsFaceLandmarksDetector)
                cv2.putText(output_frame, label, (text_x_position, text_y_position * 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

                # Compute landmarks for every face rects detected.
                getFirstFace = True  # Temporal. Get only first face for normalization.
                for f in range(len(landmarks)):
                    if getFirstFace:
                        lm_x, lm_y = self.faceLandmarksDetector.transformLandmars2arrays(f)  # Internally it uses landmarks[f]

                        if all(x == lm_x[0] for x in lm_x) and all(x == lm_y[0] for x in lm_y):
                            print("  >> No correct face landmarks Detected in frame " + str(frame_count) + "!!")
                            # [TODO] Review well this part. Tino: 06-03-2023

                            r_raw_signal.append(previous_rgb_values[0])
                            g_raw_signal.append(previous_rgb_values[1])
                            b_raw_signal.append(previous_rgb_values[2])

                            r_raw_signal_left.append(previous_rgb_left_values[0])
                            g_raw_signal_left.append(previous_rgb_left_values[1])
                            b_raw_signal_left.append(previous_rgb_left_values[2])

                            r_raw_signal_right.append(previous_rgb_right_values[0])
                            g_raw_signal_right.append(previous_rgb_right_values[0])
                            b_raw_signal_right.append(previous_rgb_right_values[0])
                            getFirstFace = False


                        else:
                            landmarks85 = self.faceLandmarksDetector.get85Landmarks(frame, f)

                            # If we assume that it can be more faces than one. Later discuss
                            if len(landmarks) > 1:
                                number_of_faces.append(len(bboxes))
                                # frame_number.append(frame_count)


                            # cv2.face.drawFacemarks(output_frame, landmarks[f])
                            cv2.face.drawFacemarks(output_frame, np.array([landmarks85[f]]), color=[255, 255, 255])

                            if face_triangles_number == 17:
                                normalizedFace = self.faceLandmarksDetector.getNormalizedFace(faceRectImage, landmarks[f], adaptedFaceRect, number_of_triangles=17)
                            elif face_triangles_number == 131:
                                normalizedFace = self.faceLandmarksDetector.getNormalizedFace(faceRectImage, np.array([landmarks85[f]]), adaptedFaceRect,
                                                                                              number_of_triangles=131)

                            if self.detectorSkin.fss_method == 'uNET':
                                t3 = time.time()
                                im_rgb = cv2.cvtColor(faceRectImage, cv2.COLOR_BGR2RGB)
                                skin_mask_bw = self.detectorSkin.find_skin(im_rgb)

                                # skin_mask_bw = detectorSkin.find_skin_fast(normalizedFace)
                                hsm, wsm = skin_mask_bw.shape
                                # Pass to color
                                skin_mask = np.zeros((hsm, wsm, 3), skin_mask_bw.dtype)
                                skin_mask[:, :, 0] = skin_mask_bw
                                skin_mask[:, :, 1] = skin_mask_bw
                                skin_mask[:, :, 2] = skin_mask_bw

                                # locs = np.where(skin_mask_bw == 255)
                                # pixels = faceRectImage[locs]
                                colored_portion = cv2.bitwise_or(faceRectImage, faceRectImage, mask=skin_mask_bw)
                                normalizedFace_face_withmask = self.faceLandmarksDetector.getNormalizedFace(colored_portion, np.array([landmarks85[f]]), adaptedFaceRect,
                                                                                                            number_of_triangles=131)
                                norMask = cv2.cvtColor(normalizedFace_face_withmask, cv2.COLOR_RGB2GRAY)
                                _, norMaskThres = cv2.threshold(norMask, thresh=2, maxval=255, type=cv2.THRESH_BINARY)
                                # cv2.imshow("Algoooo", normalizedFace_face_withmask)
                                # cv2.imshow("Algoooo2", norMaskThres)
                                locs = np.where(normalizedFace_face_withmask > 0)
                                pixels = normalizedFace_face_withmask[locs]
                                avg_color = np.average(pixels)

                                mean_pixels = cv2.mean(normalizedFace_face_withmask, mask=norMaskThres)

                                ht, wt, _ = normalizedFace_face_withmask.shape
                                left_mask = norMaskThres[0:ht, 0:(wt // 2 - 1)]
                                left_NormalizedFace = normalizedFace_face_withmask[0:ht, 0:(wt // 2 - 1)]
                                mean_pixels_right = cv2.mean(left_NormalizedFace, mask=left_mask)

                                right_mask = norMaskThres[0:ht, (wt // 2 - 1):wt - 1]
                                right_NormalizedFace = normalizedFace_face_withmask[0:ht, (wt // 2 - 1):wt - 1]
                                mean_pixels_left = cv2.mean(right_NormalizedFace, mask=right_mask)

                                tt_FaceSkinDetector += time.time() - t3
                                fpsFaceSkinDetector = frame_count / tt_FaceSkinDetector
                                label = self.detectorSkin.getLabelFaceSkinDetector(fpsFaceSkinDetector)
                                cv2.putText(output_frame, label, (text_x_position, text_y_position * 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

                            if self.detectorSkin.fss_method == 'LANDMARKS':
                                t3 = time.time()
                                # normalizedFace_selection = self.faceLandmarksDetector.getNormalizedFace(faceRectImage, np.array([landmarks85[f]]), adaptedFaceRect,
                                # number_of_triangles=43)
                                normalizedFace_selection = self.faceLandmarksDetector.getNormalizedFace(faceRectImage, np.array([landmarks85[f]]), adaptedFaceRect,
                                                                                                        number_of_triangles=131)
                                norMask = cv2.cvtColor(normalizedFace_selection, cv2.COLOR_RGB2GRAY)
                                _, norMaskThres = cv2.threshold(norMask, thresh=2, maxval=255, type=cv2.THRESH_BINARY)
                                mean_pixels = cv2.mean(normalizedFace_selection, mask=norMaskThres)

                                ht, wt, _ = normalizedFace_selection.shape
                                left_mask = norMask[0:ht, 0:(wt // 2 - 1)]
                                left_NormalizedFace = normalizedFace_selection[0:ht, 0:(wt // 2 - 1)]
                                mean_pixels_right = cv2.mean(left_NormalizedFace, mask=left_mask)

                                right_mask = norMask[0:ht, (wt // 2 - 1):wt - 1]
                                right_NormalizedFace = normalizedFace_selection[0:ht, (wt // 2 - 1):wt - 1]
                                mean_pixels_left = cv2.mean(right_NormalizedFace, mask=right_mask)

                                # mean_pixels = cv2.mean(normalizedFace_selection[normalizedFace_selection > 0])
                                tt_FaceSkinDetector += time.time() - t3
                                fpsFaceSkinDetector = frame_count / tt_FaceSkinDetector
                                label = self.detectorSkin.getLabelFaceSkinDetector(fpsFaceSkinDetector)
                                cv2.putText(output_frame, label, (text_x_position, text_y_position * 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                            # mean_pixels = cv2.mean(normalizedFace_face_withmask[normalizedFace_face_withmask>0])

                            # mean_pixels = np.asarray(mean_pixels)
                            r_raw_signal.append(mean_pixels[0])
                            g_raw_signal.append(mean_pixels[1])
                            b_raw_signal.append(mean_pixels[2])
                            previous_rgb_values = [mean_pixels[0], mean_pixels[1], mean_pixels[2]]

                            r_raw_signal_left.append(mean_pixels_left[0])
                            g_raw_signal_left.append(mean_pixels_left[1])
                            b_raw_signal_left.append(mean_pixels_left[2])
                            previous_rgb_left_values = [mean_pixels_left[0], mean_pixels_left[1], mean_pixels_left[2]]

                            r_raw_signal_right.append(mean_pixels_right[0])
                            g_raw_signal_right.append(mean_pixels_right[1])
                            b_raw_signal_right.append(mean_pixels_right[2])
                            previous_rgb_right_values = [mean_pixels_right[0], mean_pixels_right[1], mean_pixels_right[2]]

                            getFirstFace = False
                            # Plot in every frame the normalized face and the skin mask.
                            h, w, _ = output_frame.shape
                            hl, wl, _ = normalizedFace.shape
                            starty = int(hl * 0.1)
                            endy = hl - int(hl * 0.1)
                            normalizedFace_adapted = normalizedFace[starty:endy, :].copy()

            else:
                print("  >> No Face Detected in frame " + str(frame_count) + "!!")
                face_detected.append(int(0))
                # mean_pixels = np.asarray(mean_pixels)
                # value = 0
                r_raw_signal.append(previous_rgb_values[0])
                g_raw_signal.append(previous_rgb_values[1])
                b_raw_signal.append(previous_rgb_values[2])

                r_raw_signal_left.append(previous_rgb_left_values[0])
                g_raw_signal_left.append(previous_rgb_left_values[1])
                b_raw_signal_left.append(previous_rgb_left_values[2])

                r_raw_signal_right.append(previous_rgb_right_values[0])
                g_raw_signal_right.append(previous_rgb_right_values[0])
                b_raw_signal_right.append(previous_rgb_right_values[0])

            # Be careful. I am stamping the time also used for printing the results.
            # TODO: Flag for drawing the results or partitioned time measurement.
            time_consumed_for_processing_current_frame = time.time()

            if self.show_output:
                # Provisional RT-PPG for visualization. Different signal to the final one with Butterworth filter.
                plotRawPPG = plotterRawPPG.plot(np.mean(mean_pixels))
                hp, wp, _ = plotRawPPG.shape
                overlay_image_alpha(output_frame, plotRawPPG, (int(main_height * 0.02), main_height - hp - int(main_height * 0.02)))
                overlay_image_alpha(output_frame, normalizedFace, (text_x_position, text_y_position * 5))
                if self.detectorSkin.fss_method == 'uNET':
                    overlay_image_alpha(output_frame, normalizedFace_face_withmask, (text_x_position, text_y_position * 6 + wl))
                if self.detectorSkin.fss_method == 'LANDMARKS':
                    overlay_image_alpha(output_frame, normalizedFace_selection, (text_x_position, text_y_position * 6 + wl))

                cv2.imshow("Video Output", output_frame)

            # resized = cv2.resize(output_frame, (int(frame.shape[1]/4), int(frame.shape[0]/4)), interpolation = cv2.INTER_AREA)
            if self.save_video:
                vid_writer.write(output_frame)

            if frame_count == 1:
                tt_FaceDetector = 0

            frame_count += 1
            if len(bboxes) > 0:
                frame_count_detection += 1

            k = cv2.waitKey(1)
            if k == 27:
                break
            printProgressBar(frame_count, length_frames, prefix=f"          >> Progress for video {video_index}/{total_number_of_videos}", suffix="of the video frames.")




            hasFrame, frame = cap.read()

        # Save Dataframe results.
        print("  >> Finalized to extract rPPG from video: " + str(video[-1]))
        print("     Shape frame array: {}".format(len(frame_number)))
        print("     Shape r_signal array: {}".format(len(r_raw_signal)))
        print("     Shape g_signal array: {}".format(len(g_raw_signal)))

        df_signals["Frame number"] = frame_number
        df_signals["Face detected"] = face_detected

        print("  >> Saving results of video " + str(video[-1]) + " with " + str(len(frame_number)) + " frames...")

        number_of_analysis_samples = window_analysis * fs
        RGBSignal = np.array([r_raw_signal, g_raw_signal, b_raw_signal])
        RGBSignal_left = np.array([r_raw_signal_left, g_raw_signal_left, b_raw_signal_left])
        RGBSignal_right = np.array([r_raw_signal_right, g_raw_signal_right, b_raw_signal_right])

        if self.postfiltering:
            rppg_signal = colorTransformation.apply(RGBSignal)
            rppg_signal = filtering.detrend(rppg_signal, method='Tarvainen')
            rppg_signal = filtering.BPfilter(rppg_signal, minHz=0.75, maxHz=4.0, order=6)

            rppg_signal_left = colorTransformation.apply(RGBSignal_left)
            rppg_signal_left = filtering.detrend(rppg_signal_left, method='Tarvainen')
            rppg_signal_left = filtering.BPfilter(rppg_signal_left, minHz=0.75, maxHz=4.0, order=6)

            rppg_signal_right = colorTransformation.apply(RGBSignal_right)
            rppg_signal_right = filtering.detrend(rppg_signal_right, method='Tarvainen')
            rppg_signal_right = filtering.BPfilter(rppg_signal_right, minHz=0.75, maxHz=4.0, order=6)

            # Just for showing the R channel filtered.
            RGBSignal[0] = filtering.detrend(RGBSignal[0], method='Tarvainen')
            RGBSignal[0] = filtering.BPfilter(RGBSignal[0], minHz=0.75, maxHz=4.0, order=6)
            RGBSignal[1] = filtering.detrend(RGBSignal[1], method='Tarvainen')
            RGBSignal[1] = filtering.BPfilter(RGBSignal[1], minHz=0.75, maxHz=4.0, order=6)
            RGBSignal[2] = filtering.detrend(RGBSignal[2], method='Tarvainen')
            RGBSignal[2] = filtering.BPfilter(RGBSignal[2], minHz=0.75, maxHz=4.0, order=6)
        else:
            RGBSignal[0] = filtering.detrend(RGBSignal[0], method='Tarvainen')
            RGBSignal[1] = filtering.detrend(RGBSignal[1], method='Tarvainen')
            RGBSignal[2] = filtering.detrend(RGBSignal[2], method='Tarvainen')
            # RGBSignal = filtering.detrend(RGBSignal, method='Tarvainen')
            RGBSignal = filtering.BPfilter(RGBSignal, minHz=0.75, maxHz=4.0, order=6)
            rppg_signal = colorTransformation.apply(RGBSignal)

            RGBSignal_left[0] = filtering.detrend(RGBSignal_left[0], method='Tarvainen')
            RGBSignal_left[1] = filtering.detrend(RGBSignal_left[1], method='Tarvainen')
            RGBSignal_left[2] = filtering.detrend(RGBSignal_left[2], method='Tarvainen')
            # RGBSignal = filtering.detrend(RGBSignal, method='Tarvainen')
            RGBSignal_left = filtering.BPfilter(RGBSignal_left, minHz=0.75, maxHz=4.0, order=6)
            rppg_signal_left = colorTransformation.apply(RGBSignal_left)

            RGBSignal_right[0] = filtering.detrend(RGBSignal_right[0], method='Tarvainen')
            RGBSignal_right[1] = filtering.detrend(RGBSignal_right[1], method='Tarvainen')
            RGBSignal_right[2] = filtering.detrend(RGBSignal_right[2], method='Tarvainen')
            # RGBSignal = filtering.detrend(RGBSignal, method='Tarvainen')
            RGBSignal_right = filtering.BPfilter(RGBSignal_right, minHz=0.75, maxHz=4.0, order=6)
            rppg_signal_right = colorTransformation.apply(RGBSignal_right)



            # TODO: Make the same for Left and Right parts of the face when filtering before converting.

        print("     Shape POS array: {}".format(len(rppg_signal)))
        print("     Shape RGB array: {}".format(RGBSignal.shape))
        if self.show_output:
            T1 = len(r_raw_signal) / fs  # duration, in seconds
            t1 = np.linspace(0, T1, len(r_raw_signal))
            t1 = np.linspace(0, 10, len(r_raw_signal[int(30 * fs):int(40 * fs)]))
            import matplotlib.pyplot as plt
            fig = plt.figure(figsize=(4, 3))

            show_type_signal = "spectra"
            if show_type_signal == "raw":
                plt.subplot(311)
                plt.plot(t1, r_raw_signal[int(30 * fs):int(40 * fs)], 'r')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['raw_r_signal'], loc='upper right', fontsize=14)
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.3)
                # plt.xlabel('time (s)')
                plt.ylabel('amplitude', fontsize=14)
                plt.title("Extracted RED signal", fontsize=20)

                plt.subplot(312)
                plt.plot(t1, g_raw_signal[int(30 * fs):int(40 * fs)], 'g')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['raw_g_signal'], loc='upper right', fontsize=14)
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.3)
                # plt.xlabel('time (s)')
                plt.ylabel('amplitude', fontsize=14)
                plt.title("Extracted GREEN signal", fontsize=20)

                plt.subplot(313)
                plt.plot(t1, g_raw_signal[int(30 * fs):int(40 * fs)], 'b')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['raw_b_signal'], loc='upper right', fontsize=14)
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.3)
                plt.xlabel('time (s)', fontsize=14)
                plt.ylabel('amplitude', fontsize=14)
                plt.title("Extracted BLUE signal", fontsize=20)

            if show_type_signal == "filtered":
                plt.subplot(311)
                plt.plot(t1, RGBSignal[0][int(30 * fs):int(40 * fs)], 'r')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['filtered_r_signal'], loc='upper right', fontsize=14)
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.3)
                # plt.xlabel('time (s)')
                plt.ylabel('amplitude', fontsize=14)
                plt.title("Filtered RED signal", fontsize=20)

                plt.subplot(312)
                plt.plot(t1, RGBSignal[1][int(30 * fs):int(40 * fs)], 'g')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['filtered_g_signal'], loc='upper right', fontsize=14)
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.3)
                # plt.xlabel('time (s)')
                plt.ylabel('amplitude', fontsize=14)
                plt.title("Filtered GREEN signal", fontsize=20)

                plt.subplot(313)
                plt.plot(t1, RGBSignal[2][int(30 * fs):int(40 * fs)], 'b')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['filtered_b_signal'], loc='upper right', fontsize=14)
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.3)
                plt.xlabel('time (s)', fontsize=14)
                plt.ylabel('amplitude', fontsize=14)
                plt.title("Filtered BLUE signal", fontsize=20)

            if show_type_signal == "processed":
                plt.subplot(311)
                plt.plot(t1, rppg_signal[int(30 * fs):int(40 * fs)], 'r')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['ppg_signal'], loc='upper right', fontsize=14)
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.3)
                plt.xlabel('time (s)', fontsize=14)
                plt.ylabel('amplitude', fontsize=14)
                plt.title("Extracted PPG signal", fontsize=20)

                plt.subplot(312)
                plt.plot(t1, RGBSignal[1][int(30 * fs):int(40 * fs)], 'g')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['raw_g_signal'], loc='upper right')
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                # plt.xlabel('time (s)')
                plt.ylabel('amplitude')
                # plt.title("Filtered GREEN signal")

                plt.subplot(313)
                plt.plot(t1, RGBSignal[2][int(30 * fs):int(40 * fs)], 'b')
                plt.grid(b=True, which='major', color='#666666', linestyle='-')
                plt.legend(['raw_b_signal'], loc='upper right')
                plt.minorticks_on()
                plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                plt.xlabel('time (s)')
                plt.ylabel('amplitude')
                plt.title("Filtered BLUE signal")

            if show_type_signal == "spectra":
                sa.spectrogram(np.array([rppg_signal]), self.frameRate)

            # plt.subplot(512)
            # plt.plot(t1, pos_signal, 'g')
            # plt.grid(b=True, which='major', color='#666666', linestyle='-')
            # plt.legend(['POS_BVP_signal'], loc='upper right')
            # plt.minorticks_on()
            # plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            # plt.xlabel('time (s)')
            # plt.ylabel('amplitude')
            # plt.title("Extracted POS signal")
            #
            # plt.subplot(513)
            # plt.plot(t1, RGBSignal[0], 'r')
            # plt.grid(b=True, which='major', color='#666666', linestyle='-')
            # plt.legend(['filtered_r_signal'], loc='upper right')
            # plt.minorticks_on()
            # plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            # plt.xlabel('time (s)')
            # plt.ylabel('amplitude')
            # plt.title("Filtered red signal")

            # plt.subplot(514)
            # plt.plot(t1, pos_signal_left, 'b')
            # plt.grid(b=True, which='major', color='#666666', linestyle='-')
            # plt.legend(['filtered_POS_signal LEFT'], loc='upper right')
            # plt.minorticks_on()
            # plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            # plt.xlabel('time (s)')
            # plt.ylabel('amplitude')
            # plt.title("Extracted POS signal (Left Face part)")
            #
            # plt.subplot(515)
            # plt.plot(t1, pos_signal_right, 'm')
            # plt.grid(b=True, which='major', color='#666666', linestyle='-')
            # plt.legend(['filtered_POS_signal RIGHT'], loc='upper right')
            # plt.minorticks_on()
            # plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            # plt.xlabel('time (s)')
            # plt.ylabel('amplitude')
            # plt.title("Extracted POS signal (Right Face part)")

            # fig.savefig('Basic.png', dpi=300)
            plt.show()

        df_signals[f"filtered_{self.ppg_method}"] = rppg_signal
        df_signals[f"filtered_{self.ppg_method}_Left"] = rppg_signal_left
        df_signals[f"filtered_{self.ppg_method}_Right"] = rppg_signal_right
        df_signals["raw_r_signal"] = r_raw_signal
        df_signals["raw_g_signal"] = g_raw_signal
        df_signals["raw_b_signal"] = b_raw_signal

        if self.save_results:
            # output_path = video_path.replace(str(video[-1]), '')
            output_path = '../data/results/'
            print(" >> Output path: " + output_path)

            if self.format_results == "HDF5":
                store_hdf5 = df_signals.HDFStore(output_path + str(video[-1].split(".")[0]) + '.h5')

            if self.format_results == "HDF5":
                store_hdf5.append(str(video[-1]).split(".")[0], df_signals)
                # df.to_hdf('face-biosignals-results_' + timestampStr +  '.h5', key=str(video[-1]), mode='w')
            elif self.format_results == "CSV":
                df_signals.to_csv(output_path + str(video[-1].split(".")[0]) + '.csv', index=False, header=True)

        if self.show_output:
            cv2.destroyAllWindows()

        return rppg_signal
