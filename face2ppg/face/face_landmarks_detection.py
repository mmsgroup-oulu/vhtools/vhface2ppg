# -*- coding: utf-8 -*-
"""
    Copyright (c) 2020 Constantino Álvarez Casado constantino.lvarezcasado@oulu.fi

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import division
import cv2
import warnings
import numpy as np
import dlib
from imutils import face_utils
from face2ppg.face.DAN.FaceAlignment import FaceAlignment
from face2ppg.face.DAN.DanUtils import *
import copy

# FOR THE FACE-ALIGNMENT
canonical_face_x = np.array(
    [0, 0.0031491, 0.017858, 0.04622, 0.098961, 0.17921, 0.27684, 0.38235, 0.5, 0.61765, 0.72316, 0.82079, 0.90104, 0.95378, 0.98214,
     0.99685, 1, 0.088736, 0.15089, 0.24244, 0.33716, 0.4245, 0.5755, 0.66284, 0.75756, 0.84911, 0.91126, 0.5, 0.5, 0.5, 0.5, 0.39488,
     0.44559, 0.5, 0.55441, 0.60512, 0.19216, 0.24772, 0.31339, 0.37289, 0.31022, 0.24375, 0.62711, 0.68661, 0.75228, 0.80784, 0.75625,
     0.68978, 0.30369, 0.37641, 0.4498, 0.5, 0.5502, 0.62359, 0.69631, 0.62539, 0.55461, 0.5, 0.44539, 0.37461, 0.33321, 0.44905, 0.5,
     0.55095, 0.66679, 0.55199, 0.5, 0.44801])
canonical_face_y = np.array(
    [0.18154, 0.3199, 0.4571, 0.59077, 0.71438, 0.82062, 0.90863, 0.97893, 1, 0.97893, 0.90863, 0.82062, 0.71438, 0.59077, 0.4571,
     0.3199, 0.18154, 0.083538, 0.019965, 0, 0.013752, 0.052983, 0.052983, 0.013752, 0, 0.019965, 0.083538, 0.15964, 0.24807, 0.33543,
     0.4259, 0.48942, 0.50773, 0.52316, 0.50773, 0.48942, 0.17046, 0.13783, 0.13864, 0.18552, 0.19548, 0.19453, 0.18552, 0.13864, 0.13783,
     0.17046, 0.19453, 0.19548, 0.64961, 0.62165, 0.60536, 0.61907, 0.60536, 0.62165, 0.64961, 0.72347, 0.7562, 0.76236, 0.7562, 0.72347,
     0.6535, 0.64844, 0.6545, 0.64844, 0.6535, 0.68948, 0.69621, 0.68948])

# FOR THE SWAPPING AND SIMETRY MEASUREMENT
indexes17triangles = [[1, 19, 27], [1, 27, 31], [27, 31, 33], [1, 31, 48], [1, 4, 48], [4, 8, 48], [31, 33, 48],
                      [8, 33, 48], [15, 24, 27], [15, 27, 35],
                      [27, 33, 35], [15, 35, 54], [12, 15, 54], [8, 12, 54], [33, 35, 54], [8, 33, 54], [19, 24, 27]]

indexes131triangles = [[0, 1, 70], [0, 17, 36], [0, 70, 36], [1, 2, 70], [2, 70, 69], [2, 3, 69], [3, 4, 69], [4, 69, 48], [4, 5, 48], [5, 6, 48],
                       [6, 48, 59], [0, 17, 36], [6, 7, 59], [7, 59, 58], [7, 8, 58], [8, 58, 57], [8, 57, 56], [8, 9, 56], [0, 17, 36], [9, 56, 55],
                       [9, 10, 55], [10, 55, 54], [10, 11, 54], [11, 12, 54], [12, 54, 72], [12, 13, 72], [13, 14, 72], [14, 72, 73], [0, 17, 36], [14, 15, 73],
                       [73, 15, 16], [73, 45, 16], [45, 26, 16], [25, 26, 45], [25, 44, 45], [0, 17, 36], [24, 25, 44], [0, 17, 36], [23, 24, 44], [23, 43, 44],
                       [22, 23, 43], [22, 42, 43], [22, 27, 42], [21, 22, 27], [21, 27, 39], [21, 38, 39], [20, 21, 38], [20, 37, 38], [19, 20, 37], [18, 19, 37],
                       [18, 36, 37], [17, 18, 36], [36, 41, 70], [41, 68, 70], [40, 41, 68], [39, 40, 68], [28, 39, 68], [27, 28, 39], [27, 28, 42], [28, 42, 71],
                       [42, 47, 71], [46, 47, 71], [46, 71, 73], [45, 46, 73], [71, 72, 73], [71, 72, 35], [29, 35, 71], [28, 29, 71], [28, 29, 68], [68, 69, 70],
                       [31, 68, 69], [29, 31, 68], [29, 30, 31], [29, 30, 35], [30, 34, 35], [30, 33, 34], [30, 32, 33], [30, 31, 32], [31, 49, 69], [48, 49, 69],
                       [31, 49, 50], [31, 32, 50], [32, 33, 50], [33, 50, 51], [33, 51, 52], [33, 34, 52], [34, 35, 52], [35, 52, 53], [35, 53, 72], [53, 54, 72],
                       [53, 54, 64], [53, 64, 63], [52, 53, 63], [52, 63, 62], [51, 52, 62], [50, 51, 62], [50, 62, 61], [49, 50, 61], [49, 60, 61], [48, 49, 60],
                       [48, 59, 60], [59, 60, 67], [58, 59, 67], [58, 66, 67], [57, 58, 66], [56, 57, 66], [56, 65, 66], [55, 56, 65], [55, 64, 65], [54, 55, 64],
                       [0, 17, 74], [17, 18, 74], [18, 74, 75], [18, 19, 75], [19, 75, 76], [19, 20, 76], [20, 76, 77], [20, 21, 77], [21, 77, 78], [21, 78, 79],
                       [21, 22, 79], [22, 79, 80], [22, 80, 81], [22, 23, 81], [23, 81, 82], [23, 24, 82], [24, 82, 83], [24, 25, 83], [25, 83, 84], [25, 26, 84],
                       [16, 26, 84]]

# indexesROItriangles50 = [[0, 1, 70], [36, 41, 70], [41, 68, 70], [40, 41, 68], [39, 40, 68], [28, 39, 68], [1, 2, 70], [2, 70, 69], [2, 3, 69], [3, 4, 69], [12, 13, 72],
#                        [13, 14, 72], [14, 72, 73], [14, 15, 73], [73, 15, 16], [71, 72, 73], [71, 72, 35], [29, 35, 71], [28, 29, 71], [28, 42, 71], [42, 47, 71], [46, 47, 71],
#                        [46, 71, 73], [45, 46, 73], [28, 29, 68], [68, 69, 70], [31, 68, 69], [29, 31, 68], [0, 17, 74], [17, 18, 74], [18, 74, 75], [18, 19, 75], [19, 75, 76],
#                        [19, 20, 76], [20, 76, 77], [20, 21, 77], [21, 77, 78], [21, 78, 79], [21, 22, 79], [22, 79, 80], [22, 80, 81], [22, 23, 81], [23, 81, 82], [23, 24, 82],
#                        [24, 82, 83], [24, 25, 83], [25, 83, 84], [25, 26, 84], [16, 26, 84], [28, 39, 70]]


indexesROItrianglesESP = [[0, 1, 70], [27, 68, 70], [0, 68, 70], [27, 28, 68], [1, 2, 70], [2, 70, 69], [2, 3, 69], [3, 4, 69], [12, 13, 72], [13, 14, 72], [14, 72, 73],
                          [14, 15, 73],
                          [73, 15, 16], [71, 72, 73], [29, 71, 72], [28, 29, 71], [27, 71, 73], [27, 28, 71], [16, 71, 73], [28, 29, 68], [68, 69, 70], [29, 68, 69],
                          [0, 17, 74], [17, 18, 74], [18, 74, 75], [18, 19, 75], [19, 75, 76], [19, 20, 76], [20, 76, 77], [20, 21, 77], [21, 77, 78], [21, 78, 79],
                          [21, 22, 79], [22, 79, 80], [22, 80, 81], [22, 23, 81], [23, 81, 82], [23, 24, 82], [24, 82, 83], [24, 25, 83], [25, 83, 84], [25, 26, 84],
                          [16, 26, 84]]


def faceBoxCorrection(face_box, method):
    if method == "DAN":
        # Para transformar de rectángulo a cuadrado. Para las face DNNs
        x_center = face_box[0] + face_box[2] / 2
        y_center = face_box[1] + face_box[3] / 2

        face_box[0] = x_center - 1.3 * face_box[2] / 2
        face_box[1] = y_center - 1.3 * face_box[2] / 2
        face_box[3] = 1.3 * face_box[2]
        face_box[2] = face_box[3]
    elif method == "DLIB_ERT":
        x_center = face_box[0] + face_box[2] / 2
        y_center = face_box[1] + face_box[3] / 2

        face_box[0] = x_center - 0.7 * face_box[2] / 2
        face_box[1] = y_center - 0.6 * face_box[2] / 2
        face_box[3] = face_box[2] * 0.7
        face_box[2] = face_box[3]

    return face_box


def faceBoxCorrectionForFAN(face_box):
    # Corrección para la API de Bullat.
    face_box[2] = face_box[0] + face_box[2]
    face_box[3] = face_box[1] + face_box[3]

    return face_box


def create_blank(width, height, rgb_color=(0, 0, 0)):
    """Create new image(numpy array) filled with certain color in RGB"""
    # Create black blank image
    image = np.zeros((height, width, 3), np.uint8)

    # Since OpenCV uses BGR, convert the color first
    color = tuple(reversed(rgb_color))
    # Fill image with color
    image[:] = color
    return image


class FaceLandmarksDetector:
    ## Available face landmarks detectors.
    # 1. LBF OpenCV
    # 2. FAN Adrian Bulat

    # Variables
    fld_method = "OCV_LBF"
    conf_threshold = 0.7
    detector = []
    landmarks = []

    def __init__(self, number_of_triangles=131, fld_method=None, conf_threshold=None):
        """
        Initialize the class with the given parameters.
        :param method: method used for face detection.
        :return:
        """

        self.height = -1
        self.width = -1
        self.image_channels = -1
        self.image_data_type = -1
        self.frame_number = 0
        self.number_face_triangles = number_of_triangles
        self.height_monster_face = -1
        self.width_monster_face = -1
        self.monster_face = []

        self.resized_canonical_face_y = -1
        self.resized_canonical_face_x = -1

        self.canonical_face_landmarks = []
        self.canonical_face_landmarks85 = []

        if fld_method is not None:
            self.fld_method = fld_method
        if conf_threshold is not None:
            self.conf_threshold = conf_threshold

        if fld_method == "DLIB_ERT":
            print("  >> Loading DLIB model for face landmarks detection...")
            modelFile = "../models/face_alignment/shape_predictor_68_face_landmarks_GTX.dat"
            self.detector = dlib.shape_predictor(modelFile)

        elif fld_method == "DAN":
            print("  >> Loading DAN model for face landmarks detection...")
            # modelFile = "models/DAN.npz"
            # modelFile = "../models/DAN-Menpo.npz"
            # self.detector = FaceAlignment(112, 112, 1, nStages=2)
            modelFile = "../models/face_alignment/DAN-Menpo-tracking.npz"
            self.detector = FaceAlignment(112, 112, 1, 1)
            self.detector.loadNetwork(modelFile)
        else:
            print("  >> Loading LBF model for face landmarks detection...")
            modelFile = "../models/face_alignment/LBF686_GTX.yaml"
            # TODO: Modified Opencv C++ code for making accesible the struct Params to silent of the verbose flag.
            self.detector = cv2.face.createFacemarkLBF()
            self.detector.loadModel(modelFile)

        self.canonical_face_x = canonical_face_x
        self.canonical_face_y = canonical_face_y

    def setParameters(self, width, height, image_data_type, image_channels=3):

        self.height = height
        self.width = width
        self.image_channels = image_channels
        self.image_data_type = image_data_type
        self.frame_number = 0

        print("          >> [FaceLandmarksDetector - Info] Used resolution: width:{} x height:{}".format(width, height))

        if self.number_face_triangles == 17:
            if self.width > self.height:
                self.height_monster_face = self.height // 5
                self.width_monster_face = self.height // 5
            else:
                self.height_monster_face = self.width // 5
                self.width_monster_face = self.width // 5
        elif self.number_face_triangles == 131:
            if self.width > self.height:
                self.height_monster_face = int(self.height / 2)
                self.width_monster_face = int(self.height / 2.2)
            else:
                self.height_monster_face = int(self.width / 2)
                self.width_monster_face = int(self.width / 2.2)
        else:
            if self.width > self.height:
                self.height_monster_face = int(self.height / 2)
                self.width_monster_face = int(self.height / 2.2)
            else:
                self.height_monster_face = int(self.width / 2)
                self.width_monster_face = int(self.width / 2.2)

        # self.monster_face = np.zeros((self.height_monster_face, self.width_monster_face, channels), face_roi_image.dtype)
        self.monster_face = np.zeros((self.height_monster_face, self.width_monster_face, self.image_channels), self.image_data_type)
        self.resized_canonical_face_x = self.canonical_face_x * self.width_monster_face
        if self.number_face_triangles == 17:
            self.resized_canonical_face_y = self.canonical_face_y * self.height_monster_face * 0.85 + 35
        elif self.number_face_triangles == 131:
            # Valores que dependen del espacio de los puntos interpolados. 81 landmarks. Revisar esta parte para automatizarlo según la base de datos
            self.resized_canonical_face_y = self.canonical_face_y * self.height_monster_face * 0.72 + self.height_monster_face / 4
            self.resized_canonical_face_x = self.canonical_face_x * self.width_monster_face * 0.92 + self.width_monster_face / 25
        else:
            self.resized_canonical_face_y = self.canonical_face_y * self.height_monster_face * 0.9 + self.height_monster_face / 3
            self.resized_canonical_face_x = self.canonical_face_x * self.height_monster_face * 0.85 + self.width_monster_face / 30

        self.canonical_face_landmarks = np.zeros((1, 68, 2))
        self.canonical_face_landmarks[0][:, 0] = self.resized_canonical_face_x
        self.canonical_face_landmarks[0][:, 1] = self.resized_canonical_face_y

        self.canonical_face_landmarks85 = self.get85CanonicalLandmarks()

    def getEmptyMonsterFace(self):
        self.monster_face = np.zeros((self.height_monster_face, self.width_monster_face, self.image_channels), self.image_data_type)
        return self.monster_face

    def detectLandmarks(self, frame, in_boxes):

        # boxes = in_boxes.copy()
        boxes = copy.deepcopy(in_boxes)
        # print(type(in_boxes))
        # print("----------> Number of faces before: {}".format(len(boxes)))
        # print(boxes)

        for n in range(len(boxes)):
            boxes[n] = faceBoxCorrection(boxes[n], self.fld_method)
            break  # ToDo: Track face. Select bigger face

        # print("----------> Number of faces: {}".format(len(boxes)))
        # print(boxes[0])
        # print(in_boxes)
        if self.fld_method == "OCV_LBF":
            status, landmarks = self.detector.fit(frame, np.array(boxes))
        elif self.fld_method == "FAN":
            for n in range(len(boxes)):
                boxes[n] = faceBoxCorrectionForFAN(boxes[n])  # Change coordinates.
            landmarks = self.detector.get_landmarks(frame, detected_faces=np.array(boxes))
            status = "okay"
            # Convertion for adapting the output to the OpenCV format.
            after = [np.array([x]) for x in landmarks]
            landmarks = after
        elif self.fld_method == "DAN":
            landmarks = []
            gray = np.mean(frame, axis=2).astype(np.uint8)
            status = "okay"
            for n in range(len(boxes)):
                rect = [int(boxes[n][0]), int(boxes[n][1]), int(boxes[n][0]) + int(boxes[n][2]), int(boxes[n][1]) + int(boxes[n][3])]
                initLandmarks = bestFitRect(None, self.detector.initLandmarks, rect)
                landmarksuser = self.detector.processImg(gray[np.newaxis], initLandmarks)
                # print(type(landmarksuser))
                # # print(landmarksuser)
                for ll in range(len(landmarksuser)):
                    if landmarksuser[ll, 0] > self.width:
                        landmarksuser[ll, 0] = self.width - 2
                    if landmarksuser[ll, 0] < 0:
                        landmarksuser[ll, 0] = 1

                    if landmarksuser[ll, 1] > self.height:
                        landmarksuser[ll, 1] = self.height - 2
                    if landmarksuser[ll, 1] < 0:
                        landmarksuser[ll, 1] = 1

                landmarks.append(landmarksuser)
            after = [np.array([x]) for x in landmarks]
            landmarks = after
        elif self.fld_method == "DLIB_ERT":
            # determine the facial landmarks for the face region, then
            # convert the facial landmark (x, y)-coordinates to a NumPy
            # array
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            landmarks = []
            status = "okay"
            for box in boxes:
                shape = self.detector(gray, dlib.rectangle(int(box[0]), int(box[1]), int(box[0] + box[2]),
                                                           int(box[1] + box[3])))

                # cv2.rectangle(gray, (int(box[0]), int(box[1])), (int(box[0] + box[2]), int(box[1] + box[3])), (255, 255, 255), 5)
                # cv2.imshow("Algo", gray)
                # k = cv2.waitKey(0)
                # if k == 27:
                #     break
                shape = face_utils.shape_to_np(shape)
                landmarks.append(shape)
            after = [np.array([x]) for x in landmarks]
            landmarks = after

        self.landmarks = landmarks
        self.landmarks85 = self.landmarks.copy()

        # print(self.landmarks)
        # print(type(self.landmarks))
        # print(len(landmarks))
        # cv2.face.drawFacemarks(frame, np.array([landmarks[0][0]]), color=[255, 255, 255])
        # cv2.imshow("Dentro de landmaarks", frame)
        # cv2.waitKey(0)

        # print("Landmarks list: " + str(len(self.landmarks)))
        # print("Landmarks list2: " + str(len(landmarks)))
        # print(len(landmarks[0][0]))
        # print(landmarks)
        # input("Inside")

        return status, landmarks

    def detectLandmarks2(self, frame, in_boxes, face_detector="OpenCV_DNN"):

        boxes = copy.deepcopy(in_boxes)

        if face_detector == "OpenCV_DNNb":
            for n in range(len(boxes)):
                boxes[n] = faceBoxCorrection(boxes[n])
                break  # ToDo: Track face. Select bigger face

        if self.fld_method == "OCV_LBF":
            status, landmarks = self.detector.fit(frame, np.array(boxes))
        elif self.fld_method == "FAN":
            for n in range(len(boxes)):
                boxes[n] = faceBoxCorrectionForFAN(boxes[n])  # Change coordinates.
            landmarks = self.detector.get_landmarks(frame, detected_faces=np.array(boxes))
            status = "okay"
            # Convertion for adapting the output to the OpenCV format.
            after = [np.array([x]) for x in landmarks]
            landmarks = after
        elif self.fld_method == "DAN":
            landmarks = []
            gray = np.mean(frame, axis=2).astype(np.uint8)
            status = "okay"
            for n in range(len(boxes)):
                rect = [int(boxes[n][0]), int(boxes[n][1]), int(boxes[n][0]) + int(boxes[n][2]), int(boxes[n][1]) + int(boxes[n][3])]
                initLandmarks = bestFitRect(None, self.detector.initLandmarks, rect)
                landmarksuser = self.detector.processImg(gray[np.newaxis], initLandmarks)
                # print(type(landmarksuser))
                # # print(landmarksuser)
                for ll in range(len(landmarksuser)):
                    if landmarksuser[ll, 0] > self.width:
                        landmarksuser[ll, 0] = self.width - 1
                    if landmarksuser[ll, 0] < 0:
                        landmarksuser[ll, 0] = 0

                    if landmarksuser[ll, 1] > self.height:
                        landmarksuser[ll, 1] = self.height - 1
                    if landmarksuser[ll, 1] < 0:
                        landmarksuser[ll, 1] = 0

                landmarks.append(landmarksuser)
            after = [np.array([x]) for x in landmarks]
            landmarks = after
        elif self.fld_method == "DLIB_ERT":

            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            landmarks = []
            status = "okay"
            for box in boxes:
                shape = self.detector(gray, dlib.rectangle(int(box[0]), int(box[1]), int(box[0] + box[2]),
                                                           int(box[1] + box[3])))
                shape = face_utils.shape_to_np(shape)
                landmarks.append(shape)
            after = [np.array([x]) for x in landmarks]
            landmarks = after

        self.landmarks = landmarks
        self.landmarks85 = self.landmarks.copy()


        return status, landmarks


    def transformLandmars2arrays(self, index):
        # print("transformLandmars2arrays:")
        # print(len(self.landmarks))
        # print(self.landmarks[index].shape)
        # print(self.landmarks[index])
        temp = self.landmarks[index].transpose()
        self.landmarks_x = temp[0].transpose()[0]
        self.landmarks_y = temp[1].transpose()[0]
        return self.landmarks_x, self.landmarks_y

    def getNormalizedFace(self, face_roi_image, landmarks, adapted_face_box, number_of_triangles=None):
        if number_of_triangles != 17 and number_of_triangles != 131 and number_of_triangles != len(indexesROItrianglesESP) and number_of_triangles != None:
            print("Face normalization >> Warning: Input number of triangles passed is incorrect. Assuming default configuration (130 triangles). ")
            self.getNormalizedFace131(face_roi_image, landmarks, adapted_face_box)
        elif number_of_triangles == 17:
            self.getNormalizedFace17(face_roi_image, landmarks, adapted_face_box)
        elif number_of_triangles == 131:
            self.getNormalizedFaceESP2(face_roi_image, landmarks, adapted_face_box)
        elif number_of_triangles == len(indexesROItrianglesESP):
            self.getNormalizedFaceESP(face_roi_image, landmarks, adapted_face_box)
        else:
            self.getNormalizedFace131(face_roi_image, landmarks, adapted_face_box)

        return self.monster_face.copy()

    def getNormalizedFace131(self, face_roi_image, landmarks, adapted_face_box):
        face_rect_image = face_roi_image.copy()
        self.number_face_triangles = 131
        height, width, channels = face_roi_image.shape
        self.monster_face = np.zeros((self.height_monster_face, self.width_monster_face, self.image_channels), self.image_data_type)
        # OJO! Hacer una copia. Estoy modificando los de input.
        landmarks[0][:, 0] -= adapted_face_box[0]
        landmarks[0][:, 1] -= adapted_face_box[1]
        cv2.face.drawFacemarks(face_rect_image, landmarks, [255, 255, 255])
        # cv2.face.drawFacemarks(face_rect_image, np.array([self.canonical_face_landmarks85]), [0, 255, 0])

        # cv2.imshow("temp1", face_rect_image)
        # cv2.waitKey(0)

        # print("Hola2")
        # print(len(indexes130triangles))
        # print(landmarks.shape)
        for i in range(131):
            # print("Caca:" +str(i))
            self.transformTriangle(face_roi_image, landmarks, indexes131triangles[i])

        # cv2.imshow("Face normalization", self.monster_face)
        return self.monster_face

    def getNormalizedFaceESP2(self, face_roi_image, landmarks, adapted_face_box):
        face_rect_image = face_roi_image.copy()
        face_rect_image2 = face_roi_image.copy()

        number_face_triangles = len(indexes131triangles)
        # print( "Tamanhoooo de indexes = {}".format(number_face_triangles))
        height, width, channels = face_roi_image.shape
        self.monster_face = np.zeros((self.height_monster_face, self.width_monster_face, self.image_channels), self.image_data_type)
        # OJO! Hacer una copia. Estoy modificando los de input.
        landmarks[0][:, 0] -= adapted_face_box[0]
        landmarks[0][:, 1] -= adapted_face_box[1]

        # cv2.face.drawFacemarks(face_rect_image2, landmarks, [255, 255, 255])
        # cv2.face.drawFacemarks(self.monster_face, np.array([self.canonical_face_landmarks85]), [0, 255, 0])

        # cv2.imshow("landmarks rectif", face_rect_image2)
        # cv2.imshow("landmarks norma", self.monster_face)
        # cv2.waitKey(0)
        # if cv2.waitKey(0) & 0xFF == ord('q'):
        #     break

        for i in range(number_face_triangles):
            if face_roi_image.shape[0] == 0 and face_roi_image.shape[1] == 0:
                print("Me salto triangulo {}.....".format(i))
                continue
            else:
                # print("Pinto aquí el numero de triangulo procesado: {} ({})".format(i, indexes131triangles[i]))
                self.transformTriangle(face_roi_image, landmarks, indexes131triangles[i])
                # cv2.imshow("landmarks norma", self.monster_face)
                # cv2.waitKey(0)

        half_point = int((self.canonical_face_landmarks85[27][1] + self.canonical_face_landmarks85[28][1]) / 2)
        # print("Algunos valosres. {},{},{},{},{}".format(half_point, self.landmarks_x[17], self.landmarks_y[18], self.landmarks_x[48], self.landmarks_y[50]))

        # Para tapar los ojos y la boca.
        # cv2.rectangle(self.monster_face, (int(self.canonical_face_landmarks85[17][0]), int(self.canonical_face_landmarks85[18][1])), (int(self.canonical_face_landmarks85[26][0]), half_point), (0, 0, 0), -1)
        # cv2.rectangle(self.monster_face, (int(self.canonical_face_landmarks85[48][0]), int(self.canonical_face_landmarks85[33][1])), (int(self.canonical_face_landmarks85[54][0]), int(self.canonical_face_landmarks85[57][1])), (0, 0, 0), -1)

        # cv2.imshow("Face normalization", self.monster_face)
        # cv2.waitKey(0)

        return self.monster_face

    def getNormalizedFaceESP(self, face_roi_image, landmarks, adapted_face_box):
        face_rect_image = face_roi_image.copy()
        number_face_triangles = len(indexesROItrianglesESP)
        # print( "Tamanhoooo de indexes = {}".format(number_face_triangles))
        height, width, channels = face_roi_image.shape
        self.monster_face = np.zeros((self.height_monster_face, self.width_monster_face, self.image_channels), self.image_data_type)
        # OJO! Hacer una copia. Estoy modificando los de input.
        landmarks[0][:, 0] -= adapted_face_box[0]
        landmarks[0][:, 1] -= adapted_face_box[1]
        cv2.face.drawFacemarks(face_rect_image, landmarks, [255, 255, 255])
        # cv2.face.drawFacemarks(face_rect_image, np.array([self.canonical_face_landmarks85]), [0, 255, 0])

        for i in range(number_face_triangles):
            self.transformTriangle(face_roi_image, landmarks, indexesROItrianglesESP[i])

        # cv2.imshow("Face normalization", self.monster_face)
        return self.monster_face

    def getNormalizedFace17(self, face_roi_image, landmarks, adapted_face_box):

        face_rect_image = face_roi_image.copy()
        height, width, channels = face_roi_image.shape

        self.number_face_triangles = 17
        # OJO! Hacer una copia. Estoy modificando los de input.
        landmarks[0][:, 0] -= adapted_face_box[0]
        landmarks[0][:, 1] -= adapted_face_box[1]
        cv2.face.drawFacemarks(face_rect_image, landmarks, [255, 255, 255])

        # if self.width > self.height:
        #     self.height_monster_face = self.height//5
        #     self.width_monster_face = self.height//5
        # else:
        #     self.height_monster_face = self.height // 4
        #     self.width_monster_face = self.height//4
        #
        # self.monster_face = np.zeros((self.height_monster_face, self.width_monster_face, channels), face_roi_image.dtype)
        # resized_canonical_face_x = self.canonical_face_x*self.width_monster_face
        # resized_canonical_face_y = self.canonical_face_y*self.height_monster_face
        # self.canonical_face_landmarks = np.zeros((1,68,2))
        # self.canonical_face_landmarks[0][:,0] = resized_canonical_face_x
        # self.canonical_face_landmarks[0][:,1] = resized_canonical_face_y

        cv2.face.drawFacemarks(face_rect_image, self.canonical_face_landmarks, [0, 255, 0])

        for i in range(17):
            self.transformTriangle(face_roi_image, landmarks, indexes17triangles[i])

        cv2.imshow("Face normalization", self.monster_face)
        return self.monster_face

    def transformTriangle(self, face_image, landmarks, indexesTriangle):
        # print(indexesTriangle)
        tri1 = np.float32([landmarks[0][indexesTriangle[0]], landmarks[0][indexesTriangle[1]], landmarks[0][indexesTriangle[2]]])

        if self.number_face_triangles == 17:
            tri2 = np.float32([self.canonical_face_landmarks[0][indexesTriangle[0]],
                               self.canonical_face_landmarks[0][indexesTriangle[1]],
                               self.canonical_face_landmarks[0][indexesTriangle[2]]])
        elif self.number_face_triangles == 131:
            tri2 = np.float32([self.canonical_face_landmarks85[indexesTriangle[0]],
                               self.canonical_face_landmarks85[indexesTriangle[1]],
                               self.canonical_face_landmarks85[indexesTriangle[2]]])
        elif self.number_face_triangles == 50:
            tri2 = np.float32([self.canonical_face_landmarks85[indexesTriangle[0]],
                               self.canonical_face_landmarks85[indexesTriangle[1]],
                               self.canonical_face_landmarks85[indexesTriangle[2]]])
        else:
            print(" >>>>> Error in transformTriangle 001")

        # Find bounding box
        # print(face_image.shape)
        r1 = cv2.boundingRect(tri1)
        r2 = cv2.boundingRect(tri2)

        r1t = list(r1)
        # print(r1t)
        for i, x in enumerate(r1t):
            if x < 0:
                r1t[i] = 0

        if r1t[1] + r1t[3] > face_image.shape[0]:
            r1t[1] = face_image.shape[0] - 3 - r1t[3]

        if r1t[0] + r1t[2] > face_image.shape[1]:
            r1t[0] = face_image.shape[1] - 3 - r1t[2]

        r1 = tuple(r1t)

        r2t = list(r2)
        # print(r2t)
        # print("\n")
        for i, x in enumerate(r2t):
            if x < 0:
                r2t[i] = 0

        if r2t[1] + r2t[3] > self.monster_face.shape[0]:
            r2t[1] = self.monster_face.shape[0] - 3 - r2t[3]

        if r2t[0] + r2t[2] > self.monster_face.shape[1]:
            r2t[0] = self.monster_face.shape[1] - 3 - r2t[2]

        r2 = tuple(r2t)

        # print("\n      >>> From triangle warping. Dimension:{}".format(face_image.shape))
        # print("      >>> From triangle warping. Tri1:{}, Tri2:{}, r1:{},r2:{}".format(tri1, tri2, r1, r2))

        # cv2.imshow("input for warp", face_image)
        tri1Cropped = []
        tri2Cropped = []
        for i in range(3):
            tri1Cropped.append(((tri1[i][0] - r1[0]), (tri1[i][1] - r1[1])))
            tri2Cropped.append(((tri2[i][0] - r2[0]), (tri2[i][1] - r2[1])))

        img1Cropped = face_image[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
        # cv2.imshow("triangle", img1Cropped)

        h1, w1, _ = self.monster_face[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]].shape
        # print("      >>> From triangle warping. tri1Cropped:{}, tri2Cropped:{}, h1:{},w1:{}".format(tri1Cropped, tri2Cropped, h1, w1))

        # Given a pair of triangles, find the affine transform.
        warpMat = cv2.getAffineTransform(np.float32(tri1Cropped), np.float32(tri2Cropped))

        # Apply the Affine Transform just found to the src image
        # print("      >>> From triangle warping. img1Cropped:{}, warpMat:{}, h1:{},w1:{}".format(img1Cropped.shape, tri2Cropped, h1, w1))
        img2Cropped = cv2.warpAffine(img1Cropped, warpMat, (w1, h1), None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101)
        # img2Cropped = cv2.warpAffine(img1Cropped, warpMat, (w1, h1), None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER)

        # Get mask by filling triangle
        mask = np.zeros((h1, w1, 3), dtype=np.float32)
        cv2.fillConvexPoly(mask, np.int32(tri2Cropped), (1.0, 1.0, 1.0), 16, 0);

        # Apply mask to cropped region
        img2Cropped = img2Cropped * mask

        # Copy triangular region of the rectangular patch to the output image

        h2, w2, _ = mask.shape
        offsett_x = w1 - w2
        offsett_y = h1 - h2
        # print(self.monster_face[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]].shape)
        # print(mask.shape)
        self.monster_face[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]] = self.monster_face[r2[1]:r2[1] + r2[3],
                                                                      r2[0]:r2[0] + r2[2]] * ((1.0, 1.0, 1.0) - mask)
        self.monster_face[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]] = self.monster_face[r2[1]:r2[1] + r2[3],
                                                                      r2[0]:r2[0] + r2[2]] + img2Cropped

        # cv2.imshow("Trozo A", self.monster_face)

        return self.monster_face

    def get85Landmarks(self, image, index):

        # Hacer aquí la copia no vale, porque se sobreescribirían los resultados.
        # La copia es hecha cuándo se calculan los 68 puntos.
        # self.landmarks82 = self.landmarks.copy()

        # Time for computing strategic points.
        # print(self.landmarks_x[48])
        # print(self.landmarks_y[48])

        # TODO: Revisar los vectores "self.landmarks_x" y "self.landmarks_y".
        # Ahora mismo son los datos de la cara que se está analizando.
        # point = (self.landmarks_x[48],self.landmarks_y[48])
        # print(point[0])
        # print(point[1])
        # for i in range(len(self.landmarks_x)):
        #     if self.landmarks_x[i] < 0:
        #         self.landmarks_x[i] = 1
        #
        #     if self.landmarks_x[i] > self.width:
        #         self.landmarks_x[i] = self.width - 2
        #
        #
        # for i in range(len(self.landmarks_y)):
        #     if self.landmarks_y[i] < 0:
        #         self.landmarks_y[i] = 1
        #
        #     if self.landmarks_y[i] > self.height:
        #         self.landmarks_y[i] = self.height - 2

        # Computing strategic points
        it = self.createLineIterator((self.landmarks_x[36], self.landmarks_y[36]),
                                     (self.landmarks_x[31], self.landmarks_y[31]), image)
        central_left = (it[0][0:2] + it[-1][0:2]) / 2
        # central_left_np = np.array(central_left).reshape(1,1,2)

        # Nose --> face [Left side]
        it = self.createLineIterator((self.landmarks_x[31], self.landmarks_y[31]),
                                     (self.landmarks_x[3], self.landmarks_y[3]), image)
        # it = self.createLineIterator(central_left,(self.landmarks_x[2], self.landmarks_y[2]), image)
        # print("Value of landmark {}:({},{}). Value of landmark {}:({},{}).".format(31, self.landmarks_x[31], self.landmarks_y[31], 3, self.landmarks_x[3], self.landmarks_y[3]))
        # print(len(it))
        if len(it) == 0:
            cheek_left = np.array([self.landmarks_x[31], self.landmarks_y[31]])
        else:
            cheek_left = (it[0][0:2] + it[-1][0:2]) / 2
        # print(type(cheek_left))

        # cheek_left_np = np.array(cheek_left).reshape(1, 1, 2)

        # Nose --> face [Left side]
        it = self.createLineIterator((self.landmarks_x[36], self.landmarks_y[36]),
                                     (self.landmarks_x[2], self.landmarks_y[2]), image)
        # it = self.createLineIterator(central_left,(self.landmarks_x[2], self.landmarks_y[2]), image)
        cheek_left2 = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_left_np = np.array(cheek_left).reshape(1, 1, 2)

        # Mouth --> eyes[Right side]
        it = self.createLineIterator((self.landmarks_x[35], self.landmarks_y[35]),
                                     (self.landmarks_x[45], self.landmarks_y[45]), image)
        central_right = (it[0][0:2] + it[-1][0:2]) / 2
        # central_right_np = np.array(central_right).reshape(1, 1, 2)

        # Nose --> face [Right side]
        it = self.createLineIterator((self.landmarks_x[35], self.landmarks_y[35]),
                                     (self.landmarks_x[13], self.landmarks_y[13]), image)
        # it = self.createLineIterator(central_right,(self.landmarks_x[14], self.landmarks_y[14]), image)
        # print(" Valor de it right cheeck: {}. ({},{}]),({},{})".format(it,self.landmarks_x[35], self.landmarks_y[35],self.landmarks_x[13], self.landmarks_y[13]))

        if it.any():
            cheek_right = (it[0][0:2] + it[-1][0:2]) / 2
            # print("right cheeck: {}. {}".format(type(cheek_right), cheek_right))
        else:

            cheek_right = np.array([self.landmarks_x[35], self.landmarks_y[35]])
            print("right cheeck eeeeeeeeeeeeeeeemmmmmmmmmmmmmmmpttttttttttttttyyyyyyyyyyyy:  {}".format(cheek_right))
        # cheek_right_np = np.array(cheek_right).reshape(1, 1, 2)

        # Nose --> face [Right side]
        it = self.createLineIterator((self.landmarks_x[45], self.landmarks_y[45]),
                                     (self.landmarks_x[14], self.landmarks_y[14]), image)
        # it = self.createLineIterator(central_right,(self.landmarks_x[14], self.landmarks_y[14]), image)
        cheek_right2 = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_right_np = np.array(cheek_right).reshape(1, 1, 2)

        # Add new points to the shape face
        # self.landmarks82[index] = np.vstack((self.landmarks82[index][0], np.array([cheek_left])))
        # print("Cacolinhas2")
        # print(self.landmarks85[index].shape)
        self.landmarks85[index] = np.vstack((self.landmarks85[index][0], np.array([central_left])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_left])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_left2])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([central_right])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_right])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_right2])))

        # print( self.landmarks85[index].shape)
        # self.landmarks82[index].append(cheek_left)
        # self.landmarks82[index].append(central_left)
        # self.landmarks82[index].append(cheek_right)
        # self.landmarks82[index].append(central_right)

        # Add extra points forehead
        database = "NOBMAH"
        if database == "MAHNOB":
            distanceNosePointsY = (self.landmarks_y[29] - self.landmarks_y[27]) * 1.1
        else:
            distanceNosePointsY = (self.landmarks_y[29] - self.landmarks_y[27]) * 1.5

        extra_points = 0
        for i in range(17, 27):
            if 19 <= i <= 24:
                if i == 19:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[i] - distanceNosePointsY - distanceNosePointsY * 0.05)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 20:
                    temp_point = (self.landmarks_x[i], (self.landmarks_y[19] + self.landmarks_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.1)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 21:
                    temp_point = (self.landmarks_x[i], (self.landmarks_y[19] + self.landmarks_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.13)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 21:
                    temp_point = (self.landmarks_x[27], (self.landmarks_y[19] + self.landmarks_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.15)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 22:
                    temp_point = (self.landmarks_x[i], (self.landmarks_y[19] + self.landmarks_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.13)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 23:
                    temp_point = (self.landmarks_x[i], (self.landmarks_y[19] + self.landmarks_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.1)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 24:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[i] - distanceNosePointsY - distanceNosePointsY * 0.05)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))
            else:
                temp_point = (self.landmarks_x[i], self.landmarks_y[i] - distanceNosePointsY)
                self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))
            # Central point over the eyebrowns
            # if i == 21:
            #     temp_point = (
            #     self.landmarks_x[27], self.landmarks_y[27] - distanceNosePointsY - distanceNosePointsY / 2)
            #     self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

        # print("CENTRAL: ")
        # print(self.landmarks[index].shape)
        # print(self.landmarks82[index].shape)
        # print(len(self.landmarks))
        # print(len(self.landmarks82))

        # print(self.landmarks85[index].shape)

        return self.landmarks85

    def get85CanonicalLandmarks(self):

        # Hacer aquí la copia si vale, porque se hace una sola vez en la inicialización.
        self.canonical_face_landmarks85 = self.canonical_face_landmarks.copy()

        # monster_face2 = self.monster_face.copy()
        # cv2.face.drawFacemarks(monster_face2, np.array([self.canonical_face_landmarks85[0]]))
        # cv2.imshow("Video CACA", monster_face2)
        # cv2.waitKey(0)

        # # Time for computing strategic points.
        # # Computing strategic points
        # it = self.createLineIterator((self.resized_canonical_face_x[48], self.resized_canonical_face_y[48]),
        #                              (self.resized_canonical_face_x[39], self.resized_canonical_face_y[39]), self.monster_face)
        # central_left = (it[0][0:2] + it[-1][0:2]) / 2
        # # central_left_np = np.array(central_left).reshape(1,1,2)
        #
        # # Mouth --> eyes[Right side]
        # it = self.createLineIterator((self.resized_canonical_face_x[54], self.resized_canonical_face_y[54]),
        #                              (self.resized_canonical_face_x[42], self.resized_canonical_face_y[42]), self.monster_face)
        # central_right = (it[0][0:2] + it[-1][0:2]) / 2
        # # central_right_np = np.array(central_right).reshape(1, 1, 2)
        #
        # # Nose --> face [Left side]
        # it = self.createLineIterator(central_left, (self.resized_canonical_face_x[2], self.resized_canonical_face_y[2]), self.monster_face)
        # cheek_left = (it[0][0:2] + it[-1][0:2]) / 2
        # # cheek_left_np = np.array(cheek_left).reshape(1, 1, 2)
        #
        # # Nose --> face [Right side]
        # it = self.createLineIterator(central_right, (self.resized_canonical_face_x[14], self.resized_canonical_face_y[14]), self.monster_face)
        # cheek_right = (it[0][0:2] + it[-1][0:2]) / 2
        # # cheek_right_np = np.array(cheek_right).reshape(1, 1, 2)
        #
        # # Add new points to the shape face
        # self.canonical_face_landmarks82 = np.vstack((self.canonical_face_landmarks82[0], np.array([cheek_left])))
        # self.canonical_face_landmarks82 = np.vstack((self.canonical_face_landmarks82, np.array([central_left])))
        # self.canonical_face_landmarks82 = np.vstack((self.canonical_face_landmarks82, np.array([cheek_right])))
        # self.canonical_face_landmarks82 = np.vstack((self.canonical_face_landmarks82, np.array([central_right])))
        # # self.landmarks82[index].append(cheek_left)
        # # self.landmarks82[index].append(central_left)
        # # self.landmarks82[index].append(cheek_right)
        # # self.landmarks82[index].append(central_right)

        # Computing strategic points
        it = self.createLineIterator((self.resized_canonical_face_x[36], self.resized_canonical_face_y[36]),
                                     (self.resized_canonical_face_x[31], self.resized_canonical_face_y[31]),
                                     self.monster_face)
        central_left = (it[0][0:2] + it[-1][0:2]) / 2
        # central_left_np = np.array(central_left).reshape(1,1,2)

        # Nose --> face [Left side]
        it = self.createLineIterator((self.resized_canonical_face_x[31], self.resized_canonical_face_y[31]),
                                     (self.resized_canonical_face_x[3], self.resized_canonical_face_y[3]),
                                     self.monster_face)
        # it = self.createLineIterator(central_left,(self.landmarks_x[2], self.landmarks_y[2]), image)
        cheek_left = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_left_np = np.array(cheek_left).reshape(1, 1, 2)

        # Nose --> face [Left side]
        # print((self.resized_canonical_face_x[36], self.resized_canonical_face_y[36]))
        # print((self.resized_canonical_face_x[2], self.resized_canonical_face_y[2]))
        # print(self.monster_face.shape)
        # print(self.resized_canonical_face_x.shape)
        # print(self.resized_canonical_face_y.shape)
        it = self.createLineIterator((self.resized_canonical_face_x[36], self.resized_canonical_face_y[36]),
                                     (self.resized_canonical_face_x[3], self.resized_canonical_face_y[3]),
                                     self.monster_face)
        # it = self.createLineIterator(central_left,(self.landmarks_x[2], self.landmarks_y[2]), image)
        cheek_left2 = (it[0][0:2] + it[-1][0:2]) // 2
        # cheek_left_np = np.array(cheek_left).reshape(1, 1, 2)

        # Mouth --> eyes[Right side]
        it = self.createLineIterator((self.resized_canonical_face_x[35], self.resized_canonical_face_y[35]),
                                     (self.resized_canonical_face_x[45], self.resized_canonical_face_y[45]),
                                     self.monster_face)
        central_right = (it[0][0:2] + it[-1][0:2]) / 2
        # central_right_np = np.array(central_right).reshape(1, 1, 2)

        # Nose --> face [Right side]
        it = self.createLineIterator((self.resized_canonical_face_x[35], self.resized_canonical_face_y[35]),
                                     (self.resized_canonical_face_x[13], self.resized_canonical_face_y[13]),
                                     self.monster_face)
        # it = self.createLineIterator(central_right,(self.landmarks_x[14], self.landmarks_y[14]), image)
        cheek_right = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_right_np = np.array(cheek_right).reshape(1, 1, 2)

        # Nose --> face [Right side]
        it = self.createLineIterator((self.resized_canonical_face_x[45], self.resized_canonical_face_y[45]),
                                     (self.resized_canonical_face_x[13], self.resized_canonical_face_y[13]),
                                     self.monster_face)
        # it = self.createLineIterator(central_right,(self.landmarks_x[14], self.landmarks_y[14]), image)
        cheek_right2 = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_right_np = np.array(cheek_right).reshape(1, 1, 2)

        # Add new points to the shape face
        # self.landmarks82[index] = np.vstack((self.landmarks82[index][0], np.array([cheek_left])))
        # print("Hola")
        # print(self.canonical_face_landmarks85)
        # print(central_left)
        # print(np.transpose(central_left).shape)
        # print(type(self.canonical_face_landmarks85))
        # print(type(central_left))

        self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85[0], np.array([central_left])))
        # print(self.canonical_face_landmarks85.shape)

        self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([cheek_left])))
        self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([cheek_left2])))
        self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([central_right])))
        self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([cheek_right])))
        self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([cheek_right2])))
        # print(self.canonical_face_landmarks85.shape)

        #
        # # Add extra points forehead
        # distanceNosePointsY = self.resized_canonical_face_y[29] - self.resized_canonical_face_y[27]
        #
        # for i in range(17, 27):
        #     temp_point = (self.resized_canonical_face_x[i], self.resized_canonical_face_y[i] - distanceNosePointsY)
        #     self.canonical_face_landmarks82 = np.vstack((self.canonical_face_landmarks82, np.array([temp_point])))
        #     # Central point over the eyebrowns
        #     # if i == 21:
        #     #     temp_point = (self.landmarks_x[27], self.landmarks_y[27] - distanceNosePointsY - distanceNosePointsY//2)
        #     #     self.landmarks82[index] = np.vstack((self.landmarks82[index], np.array([temp_point])))
        #
        # print("CENTRAL CANONICAL: ")
        # print(self.canonical_face_landmarks82.shape)
        #

        # Add extra points forehead
        database = "NOBMAH"
        if database == "MAHNOB":
            distanceNosePointsY = (self.resized_canonical_face_y[29] - self.resized_canonical_face_y[27]) * 1.5
        else:
            distanceNosePointsY = (self.resized_canonical_face_y[29] - self.resized_canonical_face_y[27]) * 1.5

        extra_points = 0
        for i in range(17, 27):
            if 19 <= i <= 24:

                if i == 19:
                    temp_point = (self.resized_canonical_face_x[i], self.resized_canonical_face_y[i] - distanceNosePointsY - distanceNosePointsY * 0.05)
                    self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

                if i == 20:
                    temp_point = (self.resized_canonical_face_x[i],
                                  (self.resized_canonical_face_y[19] + self.resized_canonical_face_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.1)
                    self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

                if i == 21:
                    temp_point = (self.resized_canonical_face_x[i],
                                  (self.resized_canonical_face_y[19] + self.resized_canonical_face_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.13)
                    self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

                if i == 21:
                    temp_point = (self.resized_canonical_face_x[27],
                                  (self.resized_canonical_face_y[19] + self.resized_canonical_face_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.15)
                    self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

                if i == 22:
                    temp_point = (self.resized_canonical_face_x[i],
                                  (self.resized_canonical_face_y[19] + self.resized_canonical_face_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.13)
                    self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

                if i == 23:
                    temp_point = (self.resized_canonical_face_x[i],
                                  (self.resized_canonical_face_y[19] + self.resized_canonical_face_y[24]) / 2 - distanceNosePointsY - distanceNosePointsY * 0.1)
                    self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

                if i == 24:
                    temp_point = (self.resized_canonical_face_x[i], self.resized_canonical_face_y[i] - distanceNosePointsY - distanceNosePointsY * 0.05)
                    self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))
            else:
                temp_point = (self.resized_canonical_face_x[i], self.resized_canonical_face_y[i] - distanceNosePointsY)
                self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

        # for i in range(17, 27):
        #     temp_point = (self.resized_canonical_face_x[i], self.resized_canonical_face_y[i] - distanceNosePointsY)
        #     self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))
        #     # Central point over the eyebrowns
        #     if i == 21:
        #         temp_point = (self.resized_canonical_face_x[27],
        #                       self.resized_canonical_face_y[27] - distanceNosePointsY - distanceNosePointsY / 2)
        #         self.canonical_face_landmarks85 = np.vstack((self.canonical_face_landmarks85, np.array([temp_point])))

        # print("CENTRAL CANONICAL: ")
        # print(self.canonical_face_landmarks85.shape)

        return self.canonical_face_landmarks85

    def createLineIterator(self, P1, P2, img):
        """
        Produces and array that consists of the coordinates and intensities of each pixel in a line between two points

        Parameters:
            -P1: a numpy array that consists of the coordinate of the first point (x,y)
            -P2: a numpy array that consists of the coordinate of the second point (x,y)
            -img: the image being processed

        Returns:
            -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])
        """
        # define local variables for readability

        imageH = img.shape[0]
        imageW = img.shape[1]
        P1X = P1[0]
        P1Y = P1[1]
        P2X = P2[0]
        P2Y = P2[1]
        # P1X = np.trunc(P1[0])
        # P1Y = np.trunc(P1[1])
        # P2X = np.trunc(P2[0])
        # P2Y = np.trunc(P2[1])

        # difference and absolute difference between points
        # used to calculate slope and relative location between points
        dX = P2X - P1X
        dY = P2Y - P1Y
        dXa = int(round(np.abs(dX)))
        dYa = int(round(np.abs(dY)))

        # predefine numpy array for output based on distance between points
        itbuffer = np.empty(shape=(np.maximum(int(dYa), int(dXa)), 3), dtype=np.float32)
        itbuffer.fill(np.nan)

        # Obtain coordinates along the line using a form of Bresenham's algorithm
        negY = P1Y > P2Y
        negX = P1X > P2X
        # print("Tino INFO: {},{},{},{},{},{},{} - {},{},{},{}".format(itbuffer.shape, P1Y, P2Y, P1X, P2X, negY, negX,dX,dXa,dY,dYa))

        if P1X == P2X:  # vertical line segment
            itbuffer[:, 0] = P1X
            if negY:
                itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
            else:
                itbuffer[:, 1] = np.arange(round(P1Y) + 1, round(P1Y) + dYa + 1)
        elif P1Y == P2Y:  # horizontal line segment
            itbuffer[:, 1] = P1Y
            if negX:
                itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
        else:  # diagonal line segment
            steepSlope = dYa > dXa
            if steepSlope:
                slope = dX.astype(np.float32) / dY.astype(np.float32)
                if negY:
                    itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
                else:
                    itbuffer[:, 1] = np.arange(round(P1Y) + 1, round(P1Y) + dYa + 1)

                itbuffer[:, 0] = (slope * (itbuffer[:, 1] - P1Y)).astype(np.int) + P1X
            else:
                slope = dY.astype(np.float32) / dX.astype(np.float32)
                if negX:
                    itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
                else:
                    # itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
                    itbuffer[:, 0] = np.arange(round(P1X) + 1, round(P1X) + dXa + 1)

                itbuffer[:, 1] = (slope * (itbuffer[:, 0] - P1X)).astype(np.int) + P1Y

        # Remove points outside of image
        colX = itbuffer[:, 0]
        colY = itbuffer[:, 1]
        itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

        # Get intensities from img ndarray
        # print(itbuffer[:, 1].astype(np.uint))
        # print(itbuffer[:, 0].astype(np.uint))
        itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint), itbuffer[:, 0].astype(np.uint), 2]

        return itbuffer

    def getLabelFaceLandmarksDetector(self, time):
        label = ""
        if self.fld_method == "OCV_LBF":
            label = "OpenCV LBF Face Landmarks Detector - FPS : {:.2f}".format(time)
        elif self.fld_method == "FAN":
            label = "FAN Face Landmarks Detector - FPS : {:.2f}".format(time)
        elif self.fld_method == "DLIB_ERT":
            label = "DLIB ERT Face Landmarks Detector - FPS : {:.2f}".format(time)
        elif self.fld_method == "DAN":
            label = "DAN Face Landmarks Detector - FPS : {:.2f}".format(time)
        else:
            abel = "Face Landmarks Detector - FPS : {:.2f}".format(time)
        return label
