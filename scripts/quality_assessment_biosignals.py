# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Copyright (c) 2019 - 2023, Constantino Álvarez (CMVS - University of Oulu), Miguel
# Bordallo (CMVS - University of Oulu) and Face2PPG Contributors.
# All rights reserved.
#
# The full license is in the file LICENSE.txt, distributed with this software.
# -----------------------------------------------------------------------------


import os
import sys
import matplotlib.pyplot as plt
from scipy.signal import spectrogram, welch
import heartpy as hp
from hrvanalysis import get_time_domain_features, get_geometrical_features, get_sampen, get_csi_cvi_features, get_frequency_domain_features
import pyhrv.tools as tools
from scipy.signal import find_peaks, stft, lfilter, butter, welch, hilbert, firwin, lfilter_zi, filtfilt
from scipy.stats import pearsonr, spearmanr, kendalltau
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, mean_absolute_error, mean_squared_error, r2_score

sys.path.insert(0, os.path.abspath('../'))
from face2ppg.helpers.common import *



def plot_biosignals(signal_1, signal_2, signal_3, sample_rate):
    print(len(signal_1))
    print(len(signal_2))
    time_duration = len(signal_1) / sample_rate
    print(time_duration)
    timeline = np.linspace(0, time_duration, len(signal_1))
    plt.figure(figsize=(12, 4))
    plt.plot(timeline, signal_1, "b", timeline, signal_2, "r", timeline, signal_3, "g")
    # plt.plot(time, audio_data_resampled, "b")
    plt.xlabel("Time [s]", fontsize=18)
    plt.ylabel("Amplitude", fontsize=18)
    plt.title("rPPG2PPG Learning-based Filtering", fontsize=20)
    plt.legend(['GT PPG', 'Fake PPG', 'rPPG input'], loc='lower right', fontsize=14)
    plt.grid(True, which='major', color='#666666', linestyle='-')
    plt.grid(True, which='minor', color='#999999', linestyle='-', alpha=0.3)
    plt.minorticks_on()
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    # plt.ylim(-4 * 10e8, 4 * 10e8)
    plt.show()


def plot_spectrogram(input_signal, sampling_rate, name=""):
    duration = len(input_signal) / sampling_rate
    t = np.linspace(0., duration, len(input_signal))

    # Compute Welch of the signal
    F, P = welch(input_signal, nperseg=500, noverlap=300, window='hamming', fs=sampling_rate, nfft=4096)

    band = np.argwhere((F > 0.5) & (F < 4.0)).flatten()
    Pfreqs = F[band]
    Power = P[band]

    # Plot the spectrum on a logarithmic frequency scale
    plt.figure(figsize=(8, 6))
    # plt.semilogx(freq_axis[low_idx:high_idx + 1], np.real(spectrum_band))
    # plt.semilogx(Pfreqs, Power)
    # plt.subplot(121)
    plt.plot(Pfreqs, Power)
    plt.xlabel('Frequency (Hz)', fontsize=18)
    plt.ylabel('Magnitude', fontsize=18)
    # plt.title(f'Spectrum of {name} signal (Log Scale)', fontsize=20)
    plt.title(f'Spectrum of {name} signal', fontsize=20)
    plt.minorticks_on()
    plt.xticks(fontsize=14, weight="bold")
    plt.yticks(fontsize=14, weight="bold")
    plt.grid()

    # Calculate the spectrogram using STFT
    plt.figure(figsize=(8, 6))
    freq, t, stft = spectrogram(input_signal, sampling_rate, nfft=4096, nperseg=250, noverlap=240)

    # Normalize the amplitude of the spectrogram
    normalized_spectrogram = np.abs(stft) / np.max(np.abs(stft))
    # normalized_spectrogram = np.abs(stft)

    # Apply logarithmic scaling to the amplitude values
    # log_normalized_spectrogram = 10 * np.log10(normalized_spectrogram)

    # Frequency range of interest (e.g., 20 Hz to 300 Hz)
    freq_range = (0, 5)
    freq_indices = np.where((freq >= freq_range[0]) & (freq <= freq_range[1]))[0]

    # Plot the spectrogram using pcolormesh
    # plt.subplot(122)
    plt.pcolormesh(t, freq[freq_indices], normalized_spectrogram[freq_indices, :], shading='gouraud')
    # plt.pcolormesh(t, freq, log_normalized_spectrogram, shading='gouraud')

    plt.title(f'Spectrogram of {name} signal', fontsize=20)
    plt.ylabel('Frequency [Hz]', fontsize=18)
    plt.xlabel('Time [seconds]', fontsize=18)
    plt.colorbar(label='Normalized Amplitude (dB)')
    plt.xticks(fontsize=14, weight="bold")
    plt.yticks(fontsize=14, weight="bold")

    plt.show()


def mmslab_hpass_filter(input_signal, cutoff_freq, sampling_rate):
    """
    Apply a high-pass filter to an input signal.

    Parameters:
    - input_signal (numpy array): The input signal to be filtered.
    - cutoff_freq (float): The cutoff frequency of the high-pass filter in Hz.
    - sampling_rate (float): The sampling rate of the input signal in Hz.

    Returns:
    - filtered_signal (numpy array): The filtered output signal.
    """

    # Calculate the Nyquist frequency (half of the sampling rate)
    nyquist_freq = 0.5 * sampling_rate

    # Normalize the cutoff frequency by the Nyquist frequency
    cutoff_norm = cutoff_freq / nyquist_freq

    # Design a Butterworth high-pass filter
    # The second argument (4 in this case) is the filter order, you can adjust it as needed
    b, a = butter(4, cutoff_norm, btype='high', analog=False)

    # Apply the high-pass filter to the input signal
    filtered_signal = filtfilt(b, a, input_signal)

    return filtered_signal


def mmslab_lpass_filter(input_signal, cutoff_freq, sampling_rate):
    """
    Apply a low-pass filter to an input signal.

    Parameters:
    - input_signal (numpy array): The input signal to be filtered.
    - cutoff_freq (float): The cutoff frequency of the low-pass filter in Hz.
    - sampling_rate (float): The sampling rate of the input signal in Hz.

    Returns:
    - filtered_signal (numpy array): The filtered output signal.
    """

    # Calculate the Nyquist frequency (half of the sampling rate)
    nyquist_freq = 0.5 * sampling_rate

    # Normalize the cutoff frequency by the Nyquist frequency
    cutoff_norm = cutoff_freq / nyquist_freq

    # Design a Butterworth low-pass filter
    # The second argument (4 in this case) is the filter order, you can adjust it as needed
    b, a = butter(4, cutoff_norm, btype='low', analog=False)

    # Apply the low-pass filter to the input signal
    filtered_signal = filtfilt(b, a, input_signal)

    return filtered_signal


def bandpass_firwin(ntaps, lowcut, highcut, fs, window='hamming'):
    nyq = 0.5 * fs
    taps = firwin(ntaps, [lowcut, highcut], nyq=nyq, pass_zero=False, window=window, scale=False)
    return taps


def mmslab_bandpass_filter(input_signal, frameRate, minHz=0.5, maxHz=4.0):
    window_type = ('kaiser', 35)
    b = bandpass_firwin(10, minHz, maxHz, frameRate, window=window_type)
    a = 1
    filtered_signal = lfilter(b, a, input_signal)
    return filtered_signal


def get_hrv(input_signal, frameRate, prefix="hrv"):
    # heart_signal = input_signal.to_numpy()
    heart_signal = input_signal

    try:
        # working_data, heart_measures = hp.process(heart_signal, sample_rate=self.frameRate, bpmmin=45, bpmmax=220, windowsize=1.5)
        # working_data, heart_measures = hp.process(heart_signal, sample_rate=frameRate, bpmmin=50, bpmmax=220, windowsize=1.5, welch_wsize=10)
        working_data, heart_measures = hp.process(heart_signal, sample_rate=frameRate, bpmmin=50, bpmmax=220, windowsize=2.0)
        peaks = np.asarray(working_data['peaklist']) / frameRate


    except hp.exceptions.BadSignalWarning as w:
        # print(heart_signal)
        print(" Que pasa aquiiiii: {}".format(w))  # display message
        working_data = []
        heart_measures = {f'bpm': -999, f'ibi': -999, f'sdnn': -999,
                          f'sdsd': -999, f'rmssd': -999, f'pnn20': -999,
                          f'pnn50': -999, f'hr_mad': -999, f'sd1': -999,
                          f'sd2': -999, f's': -999, f'sd1/sd2': -999,
                          f'breathingrate': -999}
        pass  # or just pass over it if you want

    # print(heart_measures)

    # input("stoppp")

    heart_measures[f'{prefix}_PPG_bpm'] = heart_measures.pop('bpm')
    heart_measures[f'{prefix}_PPG_ibi'] = heart_measures.pop('ibi')
    heart_measures[f'{prefix}_PPG_sdnn'] = heart_measures.pop('sdnn')
    heart_measures[f'{prefix}_PPG_sdsd'] = heart_measures.pop('sdsd')
    heart_measures[f'{prefix}_PPG_rmssd'] = heart_measures.pop('rmssd')
    heart_measures[f'{prefix}_PPG_pnn20'] = heart_measures.pop('pnn20')
    heart_measures[f'{prefix}_PPG_pnn50'] = heart_measures.pop('pnn50')
    heart_measures[f'{prefix}_PPG_br'] = heart_measures.pop('breathingrate')

    if bool(working_data):

        nni = tools.nn_intervals(peaks)

        if len(nni) == 1:
            nni = np.append(nni, 100)

        try:
            time_domain_features = get_time_domain_features(nni)
            # Compute SDNN
            # non_linear_features = nl.nonlinear(rpeaks=peaks, sampling_rate=self.frameRate)
            heart_measures[f'{prefix}_PPG_nni20'] = time_domain_features['nni_20']
            heart_measures[f'{prefix}_PPG_nni50'] = time_domain_features['nni_50']
            heart_measures[f'{prefix}_PPG_median_nni'] = time_domain_features['median_nni']
            heart_measures[f'{prefix}_PPG_mean_nni'] = time_domain_features['mean_nni']
            heart_measures[f'{prefix}_PPG_range_nni'] = time_domain_features['range_nni']
            heart_measures[f'{prefix}_PPG_max_hr'] = time_domain_features['max_hr']
            heart_measures[f'{prefix}_PPG_min_hr'] = time_domain_features['min_hr']
            heart_measures[f'{prefix}_PPG_std_hr'] = time_domain_features['std_hr']


        except hp.exceptions.BadSignalWarning as w:
            print("pepito")
            heart_measures[f'{prefix}_PPG_nni20'] = -999
            heart_measures[f'{prefix}_PPG_nni50'] = -999
            heart_measures[f'{prefix}_PPG_median_nni'] = -999
            heart_measures[f'{prefix}_PPG_mean_nni'] = -999
            heart_measures[f'{prefix}_PPG_range_nni'] = -999
            heart_measures[f'{prefix}_PPG_max_hr'] = -999
            heart_measures[f'{prefix}_PPG_min_hr'] = -999
            heart_measures[f'{prefix}_PPG_std_hr'] = -999

            pass  # or just pass over it if you want

    return heart_measures


def compute_similarity(signal1, signal2):
    """
    Compute the Pearson correlation coefficient to measure the similarity between two signals.

    Parameters:
    - signal1 (array-like): First input signal.
    - signal2 (array-like): Second input signal.

    Returns:
    - similarity (float): Pearson correlation coefficient between the two signals.
    """
    # Ensure that the input signals have the same length
    if len(signal1) != len(signal2):
        raise ValueError("Input signals must have the same length.")

    # Compute the Pearson correlation coefficient
    # similarity, _ = pearsonr(signal1, signal2)
    # similarity, _ = spearmanr(signal1, signal2)
    similarity, _ = kendalltau(signal1, signal2)

    return similarity


from scipy.signal import correlate


def find_best_time_shift(series1, series2):
    """
    Find the best time shift (lag) to align series1 with series2 and compute the similarity.

    Parameters:
    - series1, series2: Input time series arrays of equal length.

    Returns:
    - best_shift: The optimal time shift (lag) for alignment.
    - similarity: The similarity score between the aligned time series.
    """

    # Ensure both series have the same length
    if len(series1) != len(series2):
        raise ValueError("Input time series must have the same length.")

    # Compute the cross-correlation between the two series
    cross_corr = correlate(series1, series2, mode='full')

    # Find the time lag (shift) that maximizes the cross-correlation
    best_shift = np.argmax(cross_corr) - len(series1) + 1

    # Compute the similarity as the maximum value of the cross-correlation
    similarity = np.max(cross_corr)

    return best_shift, similarity


from fastdtw import fastdtw
from scipy.spatial.distance import euclidean


def compute_similarity_dtw(time_series_1, time_series_2):
    # Convert the time series to numpy arrays
    ts1 = np.array(time_series_1).reshape(-1, 1)
    ts2 = np.array(time_series_2).reshape(-1, 1)

    # Compute DTW distance using the fastdtw library
    distance, _ = fastdtw(ts1, ts2, dist=euclidean)

    # Return the DTW distance as the measure of similarity
    # Note that a lower distance indicates higher similarity
    return distance


def normalized_cross_correlation_similarity(ts1, ts2):
    # Normalizing time series
    ts1_normalized = (ts1 - np.mean(ts1)) / np.std(ts1)
    ts2_normalized = (ts2 - np.mean(ts2)) / np.std(ts2)

    # Calculate cross-correlation
    correlation = np.correlate(ts1_normalized, ts2_normalized, 'full')

    # Normalize by the length of the time series
    normalized_corr = correlation / len(ts1)

    # Finding the max normalized correlation and corresponding time lag
    max_corr = np.max(normalized_corr)
    time_lag = np.argmax(normalized_corr) - (len(ts1) - 1)

    return max_corr, time_lag


def find_invalid_indices(arr, threshold=float('inf')):
    """
    Find the indices of NaN, infinite, or numbers exceeding a specified threshold in an array.

    Parameters:
    - arr (numpy array): The input array to check for invalid values.
    - threshold (float): The maximum allowed value. Default is positive infinity.

    Returns:
    - invalid_indices (numpy array): An array of indices where invalid values are located.
    """
    # Check for NaN, infinity, or values exceeding the threshold
    invalid_indices = np.where(np.isnan(arr) | np.isinf(arr) | (np.abs(arr) > threshold))

    return invalid_indices


def clean_array(arr):
    """
    Clean a NumPy array by replacing NaN values with the mean of the array.

    Parameters:
    - arr (numpy array): The input array to clean.

    Returns:
    - cleaned_arr (numpy array): The cleaned array with NaN values replaced by the mean.
    """
    # Calculate the mean of the non-NaN values
    mean_value = np.nanmean(arr)

    # Replace NaN values with the mean
    cleaned_arr = np.where(np.isnan(arr), mean_value, arr)

    return cleaned_arr


def r_squared_tino(ground_truth, predictions):
    # Mean of the ground truth values
    y_mean = np.mean(ground_truth)

    # Subtract the mean from the arrays
    ground_truth -= y_mean
    predictions -= y_mean

    # Total sum of squares
    ss_tot = np.sum(ground_truth ** 2)

    # Residual sum of squares
    ss_res = np.sum(predictions ** 2)

    # Check if ss_res is greater than or equal to ss_tot
    if ss_res >= ss_tot:
        r2 = 0  # Model is no better than a horizontal line
    else:
        # R^2 calculation
        r2 = 1 - (ss_res / ss_tot)

    # r = np.corrcoef(ground_truth, predictions)
    # print(r)
    # r2 = (r[0, 1]) ** 2

    return r2


def main():
    print('\n')
    print('********************************************************************')
    print('*      FACE2PPG: Quality Assessment of Biosignals  v0.01 Beta      *')
    print('********************************************************************')
    print('\n')

    # Load biosignals
    data_input_path = "../data/signals"
    gt_ppg_signals = np.load(f"{data_input_path}/GT.npy")
    rppg_signals = np.load(f"{data_input_path}/input.npy")
    fake_ppg_signals = np.load(f"{data_input_path}/fake.npy")

    # Configuration
    sample_rate = 51.2  # Fixed by Manu.
    plot_freq_analysis = False
    number_of_signals_in_container = 280  # Fixed by Honghan
    # number_of_signals = len(gt_ppg_signals) // number_of_signals_in_container

    if plot_freq_analysis:
        i = 42
        rppg_window = rppg_signals[512 * i:512 * i + 512]
        rppg_filtered = mmslab_hpass_filter(rppg_window, 0.5, sampling_rate=sample_rate)
        # rppg_filtered = mmslab_bandpass_filter(rppg_filtered, frameRate=sample_rate, minHz=0.5, maxHz=4.0)
        rppg_filtered = mmslab_lpass_filter(rppg_filtered, 3.9, sampling_rate=sample_rate)

        # plot_biosignals(gt_ppg_signals[512 * i:512 * i + 512], fake_ppg_signals[512 * i:512 * i + 512], rppg_signals[512 * i:512 * i + 512], sample_rate)
        plot_biosignals(gt_ppg_signals[512 * i:512 * i + 512], fake_ppg_signals[512 * i:512 * i + 512], rppg_filtered, sample_rate)
        plot_spectrogram(gt_ppg_signals[512 * i:512 * i + 512], sampling_rate=sample_rate, name="GT PPG")
        plot_spectrogram(rppg_filtered, sampling_rate=sample_rate, name="rPPG")

    # Containers parameters
    hr_gt_ppg = []
    hr_fake_ppg = []
    hr_rppg = []

    sdnn_gt_ppg = []
    sdnn_fake_ppg = []
    sdnn_rppg = []

    pcc_gt_vs_fake = []
    pcc_gt_vs_rppg = []

    # Main Loop
    for i in range(number_of_signals_in_container):
        print(f" - Processing window number {i}...")
        print(f"        * Window range: {512 * i} to {512 * i + 512} // ({len(gt_ppg_signals)})")
        window_gt_ppg = gt_ppg_signals[512 * i:512 * i + 512]
        window_fake_ppg = fake_ppg_signals[512 * i:512 * i + 512]
        window_rppg = rppg_signals[512 * i:512 * i + 512]

        window_rppg = mmslab_hpass_filter(window_rppg, 0.5, sampling_rate=sample_rate)
        window_rppg = mmslab_lpass_filter(window_rppg, 3.9, sampling_rate=sample_rate)

        gt_parameters = get_hrv(window_gt_ppg, frameRate=sample_rate)
        fake_parameters = get_hrv(window_fake_ppg, frameRate=sample_rate)
        rppg_parameters = get_hrv(window_rppg, frameRate=sample_rate)

        # if i == 42:
        #     print(gt_parameters)
        #     print(fake_parameters)
        #     print(rppg_parameters)
        #     input("stiooooop")

        hr_gt_ppg.append(gt_parameters["hrv_PPG_bpm"])
        hr_fake_ppg.append(fake_parameters["hrv_PPG_bpm"])
        hr_rppg.append(rppg_parameters["hrv_PPG_bpm"])

        sdnn_gt_ppg.append(gt_parameters["hrv_PPG_sdnn"])
        sdnn_fake_ppg.append(fake_parameters["hrv_PPG_sdnn"])
        sdnn_rppg.append(rppg_parameters["hrv_PPG_sdnn"])

        # similarity_fake = compute_similarity(window_gt_ppg, window_fake_ppg)
        # _, similarity_fake = find_best_time_shift(window_gt_ppg, window_fake_ppg)
        # similarity_fake = compute_similarity_dtw(window_gt_ppg, window_fake_ppg)
        similarity_fake, _ = normalized_cross_correlation_similarity(window_gt_ppg, window_fake_ppg)
        # print("Pearson Correlation Coefficient GT vs FAKE:", similarity_fake)

        # similarity_rppg = compute_similarity(window_gt_ppg, window_rppg)
        # _, similarity_rppg = find_best_time_shift(window_gt_ppg, window_rppg)
        # similarity_rppg = compute_similarity_dtw(window_gt_ppg, window_rppg)
        similarity_rppg, _ = normalized_cross_correlation_similarity(window_gt_ppg, window_rppg)
        # print("Pearson Correlation Coefficient GT vs rPPG:", similarity_rppg)
        pcc_gt_vs_fake.append(similarity_fake)
        pcc_gt_vs_rppg.append(similarity_rppg)

    # DEBUGGING
    # print(hr_gt_ppg)
    # # print(hr_fake_ppg)
    # invalid_indices = find_invalid_indices(hr_gt_ppg)
    # print(invalid_indices)
    # print(hr_gt_ppg[invalid_indices[0]])
    print("\n")
    print("    #########  RESULTS #########")
    hr_gt_ppg = clean_array(hr_gt_ppg)
    sdnn_gt_ppg = clean_array(sdnn_gt_ppg)

    print("\n")
    print("*************  Mean and std of the HR values computed from the GT PPG signals  **************")
    print(f"    >> MEAN ± SD of HR GT: {np.mean(hr_gt_ppg):.2f} ± {np.std(hr_gt_ppg):.2f}")
    print(f"    >> MEAN ± SD of HR_sdnn GT: {np.mean(sdnn_gt_ppg):.2f} ± {np.std(sdnn_gt_ppg):.2f}")

    print("\n")
    print("*************  Metrics Performance for HR values of GT vs Fake  **************")
    rmse_fake_hr = mean_squared_error(np.array(hr_gt_ppg), np.array(hr_fake_ppg), squared=False)
    mae_fake_hr = mean_absolute_error(np.array(hr_gt_ppg), np.array(hr_fake_ppg))
    mape_fake_hr = np.mean(np.abs((np.array(hr_gt_ppg) - np.array(hr_fake_ppg)) / np.array(hr_gt_ppg))) * 100
    mae_std_fake_hr = np.std(np.abs(np.array(hr_gt_ppg) - np.array(hr_fake_ppg)))
    r2_value_fake = r_squared_tino(np.array(hr_gt_ppg), np.array(hr_fake_ppg))

    print(f"    >> MAE ± SD of HR Fake: {mae_fake_hr:.2f} ± {mae_std_fake_hr:.2f}")
    print(f"    >> MAPE of HR Fake: {mape_fake_hr:.2f}")
    print(f"    >> RMSE of HR Fake: {rmse_fake_hr:.2f}")
    print(f"    >> R2 of HR Fake: {r2_value_fake:.2f}")

    rmse_rppg_hr = mean_squared_error(np.array(hr_gt_ppg), np.array(hr_rppg), squared=False)
    mae_rppg_hr = mean_absolute_error(np.array(hr_gt_ppg), np.array(hr_rppg))
    mape_rppg_hr = np.mean(np.abs((np.array(hr_gt_ppg) - np.array(hr_rppg)) / np.array(hr_gt_ppg))) * 100
    mae_std_rppg_hr = np.std(np.abs(np.array(hr_gt_ppg) - np.array(hr_rppg)))
    r2_value_rppg = r_squared_tino(np.array(hr_gt_ppg), np.array(hr_rppg))

    print(f"    >> MAE ± SD of HR rPPGs: {mae_rppg_hr:.2f} ± {mae_std_rppg_hr:.2f}")
    print(f"    >> MAPE of HR rPPGs: {mape_rppg_hr:.2f}")
    print(f"    >> RMSE of HR rPPGs: {rmse_rppg_hr:.2f}")
    print(f"    >> R2 of HR rPPGs: {r2_value_rppg:.2f}")

    print("\n")
    print("*************  Metrics Performance for HR values of GT vs rPPG  **************")
    rmse_fake_hr_sdnn = mean_squared_error(np.array(sdnn_gt_ppg), np.array(sdnn_fake_ppg), squared=False)
    mae_fake_hr_sdnn = mean_absolute_error(np.array(sdnn_gt_ppg), np.array(sdnn_fake_ppg))
    mape_fake_hr_sdnn = np.mean(np.abs((np.array(sdnn_gt_ppg) - np.array(sdnn_fake_ppg)) / np.array(sdnn_gt_ppg))) * 100
    mae_std_fake_hr_sdnn = np.std(np.abs(np.array(sdnn_gt_ppg) - np.array(sdnn_fake_ppg)))
    r2_value_fake_sdnn = r_squared_tino(np.array(sdnn_gt_ppg), np.array(sdnn_fake_ppg))

    print(f"    >> MAE ± SD of HR_sdnn Fake: {mae_fake_hr_sdnn:.2f} ± {mae_std_fake_hr_sdnn:.2f}")
    print(f"    >> MAPE of HR_sdnn Fake: {mape_fake_hr_sdnn:.2f}")
    print(f"    >> RMSE of HR_sdnn Fake: {rmse_fake_hr_sdnn:.2f}")
    print(f"    >> R2 of HR_sdnn Fake: {r2_value_fake_sdnn:.2f}")

    rmse_rppg_hr_sdnn = mean_squared_error(np.array(sdnn_gt_ppg), np.array(sdnn_rppg), squared=False)
    mae_rppg_hr_sdnn = mean_absolute_error(np.array(sdnn_gt_ppg), np.array(sdnn_rppg))
    mape_rppg_hr_sdnn = np.mean(np.abs((np.array(sdnn_gt_ppg) - np.array(sdnn_rppg)) / np.array(sdnn_gt_ppg))) * 100
    mae_std_rppg_hr_sdnn = np.std(np.abs(np.array(sdnn_gt_ppg) - np.array(sdnn_rppg)))
    r2_value_rppg_sdnn = r_squared_tino(np.array(sdnn_gt_ppg), np.array(sdnn_rppg))

    print(f"    >> MAE ± SD of HR_sdnn rPPGs: {mae_rppg_hr_sdnn:.2f} ± {mae_std_rppg_hr_sdnn:.2f}")
    print(f"    >> MAPE of HR_sdnn rPPGs: {mape_rppg_hr_sdnn:.2f}")
    print(f"    >> RMSE of HR_sdnn rPPGs: {rmse_rppg_hr_sdnn:.2f}")
    print(f"    >> R2 of HR_sdnn rPPGs: {r2_value_rppg_sdnn:.2f}")

    print("\n")
    print("*************  Metrics Performance for PCC of GT vs Fake signals  **************")
    mean_fake_pcc = np.mean(pcc_gt_vs_fake)
    std_fake_pcc = np.std(pcc_gt_vs_fake)
    print(f"    >> MEAN ± SD of PCC of GT vs Fake: {mean_fake_pcc:.2f} ± {std_fake_pcc:.2f}")

    print("\n")
    print("*************  Metrics Performance for PCC of GT vs rPPG signals  **************")
    mean_rppg_pcc = np.mean(pcc_gt_vs_rppg)
    std_rppg_pcc = np.std(pcc_gt_vs_rppg)
    print(f"    >> MEAN ± SD of PCC of GT vs Fake: {mean_rppg_pcc:.2f} ± {std_rppg_pcc:.2f}")


if __name__ == "__main__":
    main()
