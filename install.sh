#!/bin/bash

#1) Create and activate environment
ENVS=$(conda info --envs | awk '{print $1}' )
if [[ $ENVS = *"Face2PPG"* ]]; then
   source ~/anaconda3/etc/profile.d/conda.sh
   conda activate Face2PPG
else
   echo "Creating a new conda environment for Face2PPG project..."
   #conda env create -n Face2PPG python=3.8
   conda env create -f environment.yml
   source ~/anaconda3/etc/profile.d/conda.sh
   conda activate Face2PPG
   #python setup.py install
   #exit
fi;

