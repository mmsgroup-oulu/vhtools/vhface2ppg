# -*- coding: utf-8 -*-
"""
    Copyright (c) 2020 Constantino Álvarez Casado constantino.lvarezcasado@oulu.fi

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import division
import cv2
import dlib
import numpy as np
import copy

class FaceDetector:
    # Variables

    # Available face detectors.
    # OpenCV DNN supports 2 networks.
    # 1. FP16 version of the original caffe implementation ( 5.4 MB )
    # 2. 8 bit Quantized version using Tensorflow ( 2.7 MB )

    # 3. HOG Dlib.

    fd_method = "DNN_TF_OCV"
    conf_threshold = 0.7
    detector = []
    net = []

    def __init__(self, fd_method=None, conf_threshold=None, gpu_support=0):
        """
        Initialize the class with the given parameters.
        :param method: method used for face detection.
        :return:
        """

        GPU_SUPPORT = gpu_support

        if fd_method is not None:
            self.fd_method = fd_method
        if conf_threshold is not None:
            self.conf_threshold = conf_threshold

        if self.fd_method == "DNN_CAFFE_OCV":
            print("  >> Loading DNN_CAFFE_OCV model for face detection...")
            modelFile = "../models/face_detection/res10_300x300_ssd_iter_140000_fp16.caffemodel"
            configFile = "../models/face_detection/deploy.prototxt"
            self.detector = cv2.dnn.readNetFromCaffe(configFile, modelFile)
            if GPU_SUPPORT:
                self.detector.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
                self.detector.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
        elif self.fd_method == "DLIB_HOG":
            print("  >> Loading DLIB HOG for face detection...")
            self.detector = dlib.get_frontal_face_detector()
        elif self.fd_method == "YUNET":
            print("  >> Loading DNN_YUNET_OCV model for face detection...")
            modelFile = ""
            configFile = ""
            modelFile = "../models/face_detection/face_detection_yunet_2022mar.onnx"
            self.detector = cv2.FaceDetectorYN.create(modelFile, "", (320, 320))
        else:  # default Face Detector "DNN_TF_OCV"
            print("  >> Loading DNN_TF_OCV model for face detection...")
            modelFile = "../models/face_detection/opencv_face_detector_uint8.pb"
            configFile = "../models/face_detection/opencv_face_detector.pbtxt"
            self.detector = cv2.dnn.readNetFromTensorflow(modelFile, configFile)
            if GPU_SUPPORT:
                self.detector.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
                self.detector.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)



    def detectFaces(self, frame):
        if self.fd_method == "DNN_CAFFE_OCV":
            boxes = self.__detectFaceOpenCVDnnTF(frame)
        elif self.fd_method == "DLIB_HOG":
            boxes = self.__detectFaceDlibHog(frame)
        elif self.fd_method == "YUNET":
            boxes = self.__detectFaceOpenCVYunet(frame)
        else:
            boxes = self.__detectFaceOpenCVDnnTF(frame)
        return boxes

    def selectBiggestFaceRect(self, bboxes):
        if len(bboxes) > 1:
            areas = []
            for face in bboxes:
                areas.append(face[2] * face[3])
            areas = np.array(areas)
            ia = np.argsort(areas)
            selected_face = bboxes[ia[-1]]
        else:
            selected_face = bboxes[0]

        return selected_face


    def setParameters(self, width, height, image_channels=3):

        # Set input size
        self.detector.setInputSize((width, height))

    def __detectFaceOpenCVYunet(self, frame):
        frameOpencv = frame.copy()
        frameHeight = frameOpencv.shape[0]
        frameWidth = frameOpencv.shape[1]
        detections = self.detector.detect(frameOpencv)

        # print(len(detections[1]))
        # print(detections)
        # print("-----------------")
        # print(detections[1])
        # input("stop en Yunet")
        bboxes = []
        if (detections[1] is not None) and (len(detections[1]) > 0):
            for detection in detections[1]:
                # Converting predicted and ground truth bounding boxes to required format
                pred_bbox = detection
                pred_bbox = [int(i) for i in pred_bbox[:4]]
                bboxes.append(pred_bbox)

        return bboxes

    def __detectFaceOpenCVDnnTF(self, frame):
        frameOpencv = frame.copy()
        frameHeight = frameOpencv.shape[0]
        frameWidth = frameOpencv.shape[1]
        blob = cv2.dnn.blobFromImage(frameOpencv, 1.0, (300, 300), [104, 117, 123], False, False)

        self.detector.setInput(blob)
        detections = self.detector.forward()
        bboxes = []
        for i in range(detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > self.conf_threshold:
                x1 = int(detections[0, 0, i, 3] * frameWidth)
                y1 = int(detections[0, 0, i, 4] * frameHeight)
                x2 = int(detections[0, 0, i, 5] * frameWidth)
                y2 = int(detections[0, 0, i, 6] * frameHeight)
                bboxes.append([x1, y1, x2 - x1, y2 - y1])
                # cv2.rectangle(frameOpencv, (x1, y1), (x2, y2), (0, 255, 0), int(round(frameHeight / 150)), 8)
        return bboxes

    def __detectFaceDlibHog(self, frame, inHeight=300, inWidth=0):

        frameDlibHog = frame.copy()
        frameHeight = frameDlibHog.shape[0]
        frameWidth = frameDlibHog.shape[1]
        if not inWidth:
            inWidth = int((frameWidth / frameHeight) * inHeight)

        scaleHeight = frameHeight / inHeight
        scaleWidth = frameWidth / inWidth

        frameDlibHogSmall = cv2.resize(frameDlibHog, (inWidth, inHeight))
        frameDlibHogSmall = cv2.cvtColor(frameDlibHogSmall, cv2.COLOR_BGR2RGB)
        faceRects = self.detector(frameDlibHogSmall, 0)
        bboxes = []
        for faceRect in faceRects:
            cvRect = [int(faceRect.left() * scaleWidth), int(faceRect.top() * scaleHeight),
                      int(faceRect.right() * scaleWidth) - int(faceRect.left() * scaleWidth), int(faceRect.bottom() * scaleHeight) - int(faceRect.top() * scaleHeight)]
            bboxes.append(cvRect)
            # cv2.rectangle(frameDlibHog, (cvRect[0], cvRect[1]), (cvRect[2], cvRect[3]), (0, 255, 0), int(round(frameHeight / 150)), 4)
        return bboxes

    def getFaceInRect(self, image, input_box):

        rect_crop = None
        if self.fd_method == "DLIB_HOG":
            # print("HERE    DLIB HOG")
            box = copy.deepcopy(input_box)
            adaptedWidth = box[2] * 1.2
            center_x = box[0] + box[2] / 2
            center_y = box[1] + box[3] / 2
            center_y -= adaptedWidth / 12
            x1 = int(center_x - adaptedWidth / 2)
            x2 = int(center_x + adaptedWidth / 2)
            y1 = int(center_y - adaptedWidth / 2)
            y2 = int(center_y + adaptedWidth / 2)
            if x1 < 0:
                x1 = 0
            if x2 > image.shape[1]:
                x2 = image.shape[1]
            if y1 < 0:
                y1 = 0
            if y2 > image.shape[0]:
                y2 = image.shape[0]
            # y2 = box[1] + (x2 - x1)
            faceRectImage = image[y1:y2, x1:x2]
            # adaptedFaceRect = [x1, y1, x2 - x1, y2 - y1]
            rect_crop = [x1, y1, x2 - x1, y2 - y1]

            adaptedWidth2 = box[2] * 0.9
            # center_y -= adaptedWidth2/12
            x1b = int(center_x - adaptedWidth2 / 2)
            x2b = int(center_x + adaptedWidth2 / 2)
            y1b = int(center_y - adaptedWidth2 / 2)
            y2b = int(center_y + adaptedWidth2 / 2)

            if x1b < 0:
                x1b = 0
            if x2b > image.shape[1]:
                x2b = image.shape[1]
            if y1b < 0:
                y1b = 0
            if y2b > image.shape[0]:
                y2b = image.shape[0]

            # adaptedFaceRect = [box[0], box[1], box[2], box[3]]
            # [X, Y, W, H]
            adaptedFaceRect = [x1b, y1b, x2b - x1b, y2b - y1b]
        elif self.fd_method == "DNN_TF_OCV" or self.fd_method == "YUNET":
            # print("HERE    DNN_TF_OCV")
            box = copy.deepcopy(input_box)
            adaptedWidth = box[2] * 1.5
            center_x = box[0] + box[2] / 2
            center_y = box[1] + box[3] / 2
            # center_y -= adaptedWidth/12
            x1 = int(center_x - adaptedWidth / 2)
            x2 = int(center_x + adaptedWidth / 2)
            y1 = int(center_y - adaptedWidth / 2)
            y2 = int(center_y + adaptedWidth / 2)
            if x1 < 0:
                x1 = 0
            if x2 > image.shape[1]:
                x2 = image.shape[1]
            if y1 < 0:
                y1 = 0
            if y2 > image.shape[0]:
                y2 = image.shape[0]
            # y2 = box[1] + (x2 - x1)
            faceRectImage = image[y1:y2, x1:x2]
            # adaptedFaceRect = [x1, y1, x2 - x1, y2 - y1]
            rect_crop = [x1, y1, x2 - x1, y2 - y1]

            adaptedWidth2 = box[2] * 1.1
            # center_y -= adaptedWidth2/12
            x1b = int(center_x - adaptedWidth2 / 2)
            x2b = int(center_x + adaptedWidth2 / 2)
            y1b = int(center_y - adaptedWidth2 / 2)
            y2b = int(center_y + adaptedWidth2 / 2)

            if x1b < 0:
                x1b = 0
            if x2b > image.shape[1]:
                x2b = image.shape[1]
            if y1b < 0:
                y1b = 0
            if y2b > image.shape[0]:
                y2b = image.shape[0]

            # adaptedFaceRect = [box[0], box[1], box[2], box[3]]
            # [X, Y, W, H]
            adaptedFaceRect = [x1b, y1b, x2b - x1b, y2b - y1b]
        else:
            box = copy.deepcopy(input_box)
            adaptedWidth = box[2] * 1.5
            center_x = box[0] + box[2] / 2
            center_y = box[1] + box[3] / 2
            x1 = int(center_x - adaptedWidth / 2)
            x2 = int(center_x + adaptedWidth / 2)
            y1 = int(center_y - adaptedWidth / 2)
            y2 = int(center_y + adaptedWidth / 2)
            if x1 < 0:
                x1 = 0
            if x2 > image.shape[1]:
                x2 = image.shape[1]
            if y1 < 0:
                y1 = 0
            if y2 > image.shape[1]:
                y2 = image.shape[1]
            # y2 = box[1] + box[3]
            faceRectImage = image[y1:y2, x1:x2]
            # adaptedFaceRect = [x1, box[1], x2-x1, box[3]]
            adaptedFaceRect = [x1, y1, x2 - x1, y2 - y1]
            rect_crop = [x1, y1, x2 - x1, y2 - y1]


        return faceRectImage, adaptedFaceRect, rect_crop

    def getLabelFaceDetector(self, time):
        label = ""
        if self.fd_method == "DNN_TF_OCV":
            label = "OpenCV DNN TF Face Detector - FPS : {:.2f}".format(time)
        elif self.fd_method == "DNN_CAFFE_OCV":
            label = "OpenCV DNN Caffe Face Detector - FPS : {:.2f}".format(time)
        elif self.fd_method == "DLIB_HOG":
            label = "DLIB HOG Face Detector - FPS : {:.2f}".format(time)
        else:
            label = "Face Detector - FPS : {:.2f}".format(time)
        return label



