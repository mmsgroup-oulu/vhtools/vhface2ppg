import xml.etree.ElementTree as ET
import numpy as np
import pandas as pd
import os


class LGI_PPGI:
    """
    LGI-PPGI dataset structure:
    -----------------
        datasetDIR/
        |
        |-- vidDIR1/
        |   |-- videoFile1.avi
        |
        |...
        |
        |-- vidDIRM/
            |-- videoFile1.avi
    """
    name = 'LGI_PPGI'
    signalGT = 'BVP'  # GT signal type
    numLevels = 2  # depth of the filesystem collecting video and BVP files
    numSubjects = 4  # number of subjects
    video_EXT = 'avi'  # extension of the video files
    frameRate = 25  # vieo frame rate
    VIDEO_SUBSTRING = 'cv_camera'  # substring contained in the filename
    SIG_EXT = 'xml'  # extension of the BVP files
    SIG_SUBSTRING = 'cms50'  # substring contained in the filename
    SIG_SampleRate = 60  # sample rate of the BVP files

    def read_gt_signals_file(self, filename):
        """
            Load BVP signal. Must return a 1-dim (row array) signal
        """

        tree = ET.parse(filename)
        # get all bvp elements and their values
        bvp_elements = tree.findall('.//*/value2')
        bvp = [int(item.text) for item in bvp_elements]

        n_bvp_samples = len(bvp)
        last_bvp_time = int((n_bvp_samples * 1000) / self.SIG_SampleRate)
        print("Info in lgi_ppgi 1: {},{}".format(n_bvp_samples, last_bvp_time))

        vid_xml_filename = os.path.join(os.path.dirname(filename), 'cv_camera_sensor_timer_stream_handler.xml')
        tree = ET.parse(vid_xml_filename)

        root = tree.getroot()
        last_vid_time = int(float(root[-1].find('value1').text))

        diff = ((last_bvp_time - last_vid_time) / 1000)
        # diff = abs(last_bvp_time - last_vid_time)
        print("Info in lgi_ppgi 2: {},{}".format(last_vid_time, diff))

        # assert diff >= 0, 'Unusable data.'

        print("Skipping %.2f seconds..." % diff)
        if diff >= 0:
            diff_samples = round(diff * self.SIG_SampleRate)
            # data = np.array(bvp[diff_samples:])
            data = np.array(bvp[:-diff_samples])
        else:
            data = np.array(bvp[:])

        number_of_channels = data.shape[0]
        number_of_samples = data.shape[1]
        # print(f"Channels: {number_of_channels}, N:{number_of_samples}")
        # if len(data.shape) == 1:
        #     data = data.reshape(1, -1)  # 2D array raw-wise

        return data[0], number_of_samples, number_of_channels

