# Face2PPG
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Python 3.6](https://img.shields.io/badge/python-3.6+-green.svg)](https://www.python.org/downloads/release/python-360/)


This is an implementation to extract remote photoplesthysmograms from faces. It extracts a rPPG from a video with a face, and save several signals in a .CSV file.




![cover](docs/images/Face2PPG_v2.png)


## Installation

If you want to use our test and evaluation application, clone the repository and follow the instructions.

## Requirements

* Python 3.6+
* Linux and Windows* FFmpeg package installed in the system (Ubuntu or Windows)
* CMake (for dlib package compilation)
* See also requirements.txt and environment.yml files


## Script for installation
For the installation of the _requirements_ and _environment_ run the next script:
* Run: `./install.sh`

If you are under Windows, use directly the _environment.yml_ file to install the 
dependencies (e.g. using conda environment manager: `conda env create -f environment.yml`)

Steps to set up the environment on Windows before create the conda environment:
* Install CMake
* Install Microsoft C++ Build Tools (https://visualstudio.microsoft.com/visual-cpp-build-tools/)
* Download and install FFmpeg (https://www.gyan.dev/ffmpeg/builds/ffmpeg-git-full.7z)
* Set the environment path variable for ffmpeg by running the following command:`setx /m PATH "C:\ffmpeg\bin;%PATH%"` 


For Intel-based platforms, you might need to install `mkl-service` package.


<a name="myfootnote1">1</a>: *Not tested regularly on Windows. Fully tested in Linux Ubuntu O.S.*

### Download the Models used in the framework
The project uses several models related to face detection, face alignment or skin segmentation. They are approximately 1 Gb.
 - Download the models from: [Used Models](https://drive.google.com/file/d/1Vzd608j1w02Y4cbC5kwcoveJ0OlnPR5e/view?usp=sharing)



## Usage
By default, the application read the videos from: `/data/videos`

By default, the results are saved in CSV format in: `/data/videos`.

Please, check the input arguments options in the _main_ functions of each python script or running
`python ./scripts/ppg_extraction.py --help`

![input_arguments](docs/images/input_arguments.png)


### Process a folder with videos
It searches for all the videos with extensions .mp4, .mpeg and .avi in the input folder, and it processes them.
It generates one .CSV file for each video. 

Example: _python ./scripts/ppg_extraction.py -f /path/to/folder/with/videos_

### Process only one video
Example: _python ./scripts/ppg_extraction.py -v /path/to/video/myvideo.mp4_



## Save results with annotations output file 
It saves a file containing the extracted biosignals in a .CSV or .H5 file.
(By default is _True_)

It generates a .CSV file for each video containing several columns with:
1) Number of frame
2) Detected face in current frame (0 or 1)
3) POS signal filtered from the whole face
4) POS signal from the "real" right side of the face
5) POS signal from the "real" left side of the face
6) Raw Red Channel signal
7) Raw Green Channel signal
8) Raw Blue Channel signal

![Results](docs/images/output_results.png)



## TODO list
 - [x] Integrate several face landmark methods
 - [x] Convert RGB signal to POS signal
 - [x] Visualize data - signals
 - [x] Extract POS signals from left and right parts of the face
 - [x] Solve -sr input parameter
 - [x] Add option to process databases
 - [x] Add interface for COHFACE
 - [x] Add interface for LGI-PPGi
 - [ ] Add interface for UBFC database
 - [X] Add CHROM method
 - [X] Add OMIT method
 - [X] Add LGI method
 - [ ] Add tracking face system
 - [ ] Add MTCNN face detector.
 - [ ] Create an addition folder for saving results
 - [ ] Add configuration file with parameters 
 - [ ] Add logging and exceptions handling
 - [ ] Solve issue with the forehead landmarks (68,82) when rotation


## Useful Resources and links of some parts of the code:
- https://github.com/MarekKowalski/DeepAlignmentNetwork
- https://gitlab.com/visualhealth/vhpapers/real-time-facealignment
- https://github.com/phuselab/pyVHR


## Citation

If you use this software in your research, then please cite the following:

Álvarez Casado C. & Bordallo López M.  (2022) Face2PPG: An unsupervised
pipeline for blood volume pulse extraction from faces (ArXiv) [Download PDF](https://arxiv.org/abs/2202.04101) 


## License

This project is licensed under the GNU GENERAL PUBLIC License - see the LICENSE.md file for details