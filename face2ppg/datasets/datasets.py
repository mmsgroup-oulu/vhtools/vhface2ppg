import os
from face2ppg.datasets.cohface import COHFACE
from face2ppg.datasets.lgi_ppgi import LGI_PPGI


class Datasets:
    """
    Manage datasets (parent class for new datasets)
    """

    def __init__(self, input_path=None):
        # -- load filenames
        self.videoFilenames = []  # list of all video filenames
        self.signalFilenames = []  # list of all Sig filenames
        self.numVideos = 0  # num of videos in the dataset
        self.videodataDIR = input_path
        dataset_name = input_path.split("/")[-1]
        self.dataset_name = dataset_name.lower()
        self.videoFilenames = []
        self.sigFilenames = []

        if self.dataset_name == "cohface":
            self.dataset = COHFACE()
        elif self.dataset_name == "lgi-ppgi":
            self.dataset = LGI_PPGI()
        else:
            self.dataset = COHFACE()

        self.load_filenames()
        self.fps = self.dataset.frameRate
        self.signal_fps = self.dataset.SIG_SampleRate

    def load_filenames(self):
        """Load dataset file names: define vars videoFilenames and BVPFilenames"""

        # -- loop on the dir struct of the dataset getting filenames
        for root, dirs, files in os.walk(self.videodataDIR):
            for f in files:
                filename = os.path.join(root, f)
                path, name = os.path.split(filename)
                # print("Tino info in DATASET class: {},{},{},{}".format(f, filename, path, name))

                # -- select video
                if filename.endswith(self.dataset.video_EXT) and (name.find(self.dataset.VIDEO_SUBSTRING) >= 0):
                    self.videoFilenames.append(filename)

                # -- select signal
                if filename.endswith(self.dataset.SIG_EXT) and (name.find(self.dataset.SIG_SUBSTRING) >= 0):
                    self.sigFilenames.append(filename)

        # -- number of videos
        self.numVideos = len(self.videoFilenames)

    def get_video_filename(self, videoIdx=0):
        """Get video filename given the progressive index"""

        return self.videoFilenames[videoIdx]

    def get_signal_filename(self, videoIdx=0):
        """Get Signal filename given the progressive index"""
        return self.sigFilenames[videoIdx]

    def get_gt_signal_video(self, video_name):
        """Get signal given the  video filename"""
        return self.dataset.read_gt_signals_file(video_name)
