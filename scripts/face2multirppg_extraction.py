# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Copyright (c) 2019 - 2023, Constantino Álvarez (CMVS - University of Oulu), Miguel
# Bordallo (CMVS - University of Oulu) and Face2PPG Contributors.
# All rights reserved.
#
# The full license is in the file LICENSE.txt, distributed with this software.
# -----------------------------------------------------------------------------


#############################################################################################################################################
#  USAGE:
#       - With folders:  python face2multirppg_extraction.py -f /media/arritmic/ADATA_001/DATABASES/HEART/VIDEOS_FROM_YOUTUBE
#       - With video: python face2multirppg_extraction.py -v ../data/videos/Tino_hr50_01.mp4 --show True -fld DLIB_ERT
#       - With datasets: python ppg_extraction.py -d /media/arritmic/ADATA_001/DATABASES/HEART/cohface --show True
#  Depends of Face2PPG
#############################################################################################################################################

import os
import sys
from os.path import join
import glob

sys.path.insert(0, os.path.abspath('../'))
from face2ppg.face2multippg import Face2MultiPPG
from face2ppg.helpers.common import *
from face2ppg.datasets.datasets import Datasets
from face2ppg.analysis.bvp_analysis import BVPSignal


def main():
    print('\n')
    print('********************************************************************')
    print('*      FACE2PPG: Multi-rPPG extraction application  v1.23 Beta     *')
    print('********************************************************************')
    print('\n')

    ##########################
    # Input arguments parser #
    ##########################
    ap = argparse.ArgumentParser()

    ap.add_argument("-v", "--video", required=False, help="Path to input video")
    ap.add_argument("-f", "--folder", required=False, help="Path to file with list of input videos")
    ap.add_argument("-d", "--dataset", required=False, help="Path to database folder. E.g. /data/path/to/COHFACE")
    ap.add_argument("-fd", "--face_detector", required=False, default="YUNET", help="Face Detector algorithm: DNN_TF_OCV, DNN_CAFFE_OCV,DLIB_HOG, YUNET... Default: "
                                                                                    "DNN_TF_OCV")
    ap.add_argument("-fld", "--face_landmarks_detector", required=False, default="DAN", help="Face Landmarks Detector algorithm: OCV_LBF, DLIB_ERT, FAN, DAN... "
                                                                                             "Default: DAN")
    ap.add_argument("-fss", "--face_skin_segmentation", required=False, default="LANDMARKS", help="Face Skin Segmentation algorithm: uNET, LANDMARKS, COLOR... Default: "
                                                                                                  "LANDMARKS")
    ap.add_argument("-ppg", "--ppg_method", required=False, default="POS", help="PPG method to transform the RGB signal to PPG: POS, CHROM, LGI, OMIT... Default: POS")
    ap.add_argument("-mr", "--multi_region", type=bool, default=True, required=False,
                    help="Flag to extract multiple rPPG signals from the face. Default: False")
    ap.add_argument("-nr", "--num_regions", type=int, default=64, help="Number of regions to divide the face and extract multiple signals")
    ap.add_argument("-tn", "--triangles_number", type=int, default=131, help="Number of triangles used for computing the normalized face. 17 or 131")
    ap.add_argument("-sv", "--save_videos", type=bool, default=False, required=False, help="Flag for saving the output videos. Default path: /data/videos/output/. "
                                                                                           "Default: False")
    ap.add_argument("-sr", "--save_results", type=bool, default=True, required=False,
                    help="Flag for saving the results in CSV by default. Default path: /path/to/input/videos. Default: True")
    ap.add_argument("-so", "--show", type=str2bool, nargs='?', const=True, default=False, required=False,
                    help="Flag for showing the results while videos are processed. Default: False")
    ap.add_argument("-fr", "--format_results", default="CSV", required=False, help="Which format will be used to save the results. Options HDF5 or CSV. Default CSV")
    ap.add_argument("-of", "--output_folder", required=False, default=None, help="Path to save the results... Default: /Folder/containing/the/videos/Face2PPG_results")

    args = vars(ap.parse_args())

    input_folder = None
    if args["folder"] is not None:
        input_folder = args["folder"]
    elif args["dataset"] is not None:
        input_folder = args["dataset"]
    elif args["video"] is not None:
        input_folder = args["video"].split("/")[0]

    face2ppg_converter = Face2MultiPPG(path_to_videos=input_folder, face_detector=args["face_detector"], landmarks_detector=args["face_landmarks_detector"],
                                       triangles_number=args["triangles_number"],
                                       skin_segmentation=args["face_skin_segmentation"], save_video=args["save_videos"], show_output=args["show"],
                                       format_results=args["format_results"], save_results=args["save_results"], multi_region=args["multi_region"],
                                       number_of_regions=args["num_regions"], ppg_method=args["ppg_method"], output_folder=args["output_folder"])

    ###########################
    # Reading input arguments #
    ###########################
    # Input data. For our data.
    if args["video"] is not None:
        if os.path.exists(args["video"]):
            face2ppg_converter.extract_ppg_from_face(args["video"])
        else:
            print("  >> [MMSTFace2PPG - Warning] The input file does not exist...")
    elif args["folder"] is not None:
        videos = []
        for ext in ('*.avi', '*.mp4', '*.mpeg', '*.mpg'):
            videos.extend(glob.glob(join(args["folder"], ext)))

        print("  >> [MMSTFace2PPG - Info]  >> Number of found videos in the input path: {}".format(len(videos)))
        print("  >> [MMSTFace2PPG - Info]  >> Starting to process videos...\n")

        if videos:
            video_id = 0
            for video_path in videos:
                rppg_signal = face2ppg_converter.extract_ppg_from_face(video_path=video_path, video_index=video_id, total_number_of_videos=len(videos))
                video_id += 1
        else:
            print("  >> [MMSTFace2PPG - Warning]  The input folder passed through arguments is empty...")
    elif args["dataset"] is not None:
        videos = []
        dataset = Datasets(input_path=args["dataset"])

        print("  >> [MMSTFace2PPG - Info]  >> Number of found videos in the input path: {}".format(dataset.numVideos))
        print("  >> [MMSTFace2PPG - Info]  >> Starting to process videos in dataset {}.\n".format(dataset.dataset_name))
        if dataset.videoFilenames:
            index = 0
            for video_path in dataset.videoFilenames:
                # Extract the rPPG signal from the current Video (POS and RGB)
                extracted_pos_signal = face2ppg_converter.extract_ppg_from_face(video_path, video_index=index, total_number_of_videos=dataset.numVideos)

                compute_heart = False
                if compute_heart:
                    # Read the GT BVP signal from the current Video
                    gt_bvp_signal, number_of_samples, number_of_channels = dataset.get_gt_signal_video(dataset.get_signal_filename(videoIdx=index))

                    # Analysis of the extracted (rPPG) and GT BVP signals.
                    GT_BVP_signal = BVPSignal(gt_bvp_signal, dataset.signal_fps, filename=dataset.get_signal_filename(videoIdx=index),
                                              channels=number_of_channels, startTime=0, minHz=0.75, maxHz=4., verb=False, win_size=10, f_method="welch")

                    ES_BVP_signal = BVPSignal(extracted_pos_signal, dataset.fps, channels=1, startTime=0, minHz=0.75, maxHz=4., verb=False, win_size=10, f_method="welch")
                    ES_bpm, ES_times = ES_BVP_signal.get_bpm(timestep=1.0, dataset_fps=dataset.fps)

                    already_computed_GTHR = GT_BVP_signal.get_gt_hr()
                    if not already_computed_GTHR:
                        GT_bpm, GT_times = GT_BVP_signal.get_bpm(timestep=1.0, dataset_fps=dataset.fps, save_data=True)
                    else:
                        GT_bpm, GT_times = GT_BVP_signal.get_gt_hr_data()

                    print("  >> [MMSTFace2PPG - Results]   GT HEART RATE Values:")
                    print(GT_bpm.to_numpy())

                    print("  >> [MMSTFace2PPG - Results]   ES HEART RATE Values:")
                    print(ES_bpm)
                index += 1

        else:
            print("  >> [MMSTFace2PPG - Warning]  The input folder passed through arguments is empty...")
    else:
        videos = []
        for ext in ('*.avi', '*.mp4', '*.mpeg', '*.mpg'):
            videos.extend(glob.glob(join("../data/videos", ext)))

        if videos:
            for video_path in videos:
                face2ppg_converter.extract_ppg_from_face(video_path)
        else:
            print("  >> [MMSTFace2PPG - Warning]  The input default folder passed through arguments is empty...")

    print("Work done!")


if __name__ == "__main__":
    main()
