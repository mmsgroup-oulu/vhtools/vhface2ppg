import math
import os
import sys
from os.path import join

import cv2
import numpy as np
import pandas as pd

sys.path.insert(0, os.path.abspath('..'))

from face2ppg.face.face_detection import FaceDetector
from face2ppg.face.face_landmarks_detection import FaceLandmarksDetector
import glob
from datetime import datetime
from face2ppg.segmentation.dnn_skin_detection import skinDetector
from face2ppg.plotting.VHRTPlotter import RT2DPlotter
from scipy import signal
from face2ppg.helpers.common import *
import face2ppg.analysis.spectral_analysis as sa
from face2ppg.extraction.pos_method import pos_method
from face2ppg.extraction.lgi_method import lgi_method
from face2ppg.extraction.chrom_method import chrom_method
from face2ppg.extraction.omit_method import omit_method
from face2ppg.filtering.filtering import Filtering
import ffmpeg
import mediapipe
from scipy.signal import find_peaks, stft, lfilter, butter, welch, hilbert, firwin, lfilter_zi



def check_rotation(path_video_file):
    # TODO: Review this function carefully and remove it if we can avoid it
    # this returns meta-data of the video file in form of a dictionary
    meta_dict = ffmpeg.probe(path_video_file)

    # from the dictionary, meta_dict['streams'][0]['tags']['rotate'] is the key
    # we are looking for
    # print(meta_dict['streams'][0]['tags'])

    rotateCode = None
    if 'tags' in meta_dict['streams'][0]:
        if 'rotate' in meta_dict['streams'][0]['tags']:
            # print("YES")
            if int(meta_dict['streams'][0]['tags']['rotate']) == -90:
                # print(f" Rotate 1: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                rotateCode = cv2.ROTATE_180
            elif int(meta_dict['streams'][0]['tags']['rotate']) == 180:
                # print(f" Rotate 2: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                # rotateCode = cv2.ROTATE_180
                rotateCode = cv2.ROTATE_90_CLOCKWISE
            elif int(meta_dict['streams'][0]['tags']['rotate']) == 270:
                # print(f" Rotate 3: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                # rotateCode = cv2.ROTATE_90_COUNTERCLOCKWISE
                rotateCode = cv2.ROTATE_180

            print(f"    >> Video Rotation needed: {meta_dict['streams'][0]['tags']['rotate']}")
        else:
            print(f"    >> No video Rotation needed...")
    else:
        print(f"    >> Video metadata not found...")

    return rotateCode


def overlay_image_alpha(img, img_overlay, pos):
    """Overlay img_overlay on top of img at the position specified by
    pos and blend using alpha_mask.

    Alpha mask must contain values within the range [0, 1] and be the
    same size as img_overlay.
    """
    x, y = pos

    # Image ranges
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

    # Overlay ranges
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

    # Exit if nothing to do
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return

    channels = img.shape[2]
    # alpha = alpha_mask[y1o:y2o, x1o:x2o]
    # alpha_inv = 1.0 - alpha
    for c in range(channels):
        img[y1:y2, x1:x2, c] = img_overlay[y1o:y2o, x1o:x2o, c]


def interweavingMerge(array_a, array_b):
    merged_array = np.empty((array_a.size + array_b.size,), dtype=array_a.dtype)
    merged_array[0::2] = array_a
    merged_array[1::2] = array_b
    return merged_array


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


class Face2MultiPPG:

    def __init__(self, path_to_videos, face_detector, landmarks_detector, triangles_number, skin_segmentation, save_video, show_output, format_results, save_results, multi_region,
                 number_of_regions=64, face_detector_confidence=0.7, ppg_method="POS", output_folder=None, fps=30):
        self.frameRate = fps
        self.postfiltering = True  # Filter POS signal after converting
        self.triangles_number = triangles_number
        self.save_video = save_video
        self.show_output = show_output
        self.format_results = format_results
        self.save_results = save_results
        self.ppg_method = ppg_method
        self.multiregion = multi_region
        self.number_of_regions = number_of_regions
        self.face_detector_name = face_detector
        self.slice_regions = int(math.sqrt(number_of_regions))

        if output_folder is None:
            if path_to_videos is None:
                self.output_folder = "../data/Face2PPG_results"
            else:
                self.output_folder = f"{path_to_videos}/Face2PPG_results"
        else:
            self.output_folder = f"{output_folder}/Face2PPG_results"

        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

        #################
        #  rPPG Configuration
        #################
        self.prefiltering = True
        self.bandpassfiltering = True
        self.use_iir_filter = False
        self.filtering = None
        self.filter_order = 65
        self.minHz = 0.75
        self.maxHz = 4.0
        self.zeroMeanSTDnorm = False
        self.detrending = False
        self.colorTransformation = None
        self.kaiser_beta = 25


        ##########################################
        # Detectors settings and initializations
        ##########################################

        # Default method for face detector "DNN_TF_OCV" ( DNN TensorFlow int8 model)
        self.faceDetector = FaceDetector(fd_method=face_detector, conf_threshold=face_detector_confidence)

        # Default method for face landmarks detector "OpenCV LBF"
        self.faceLandmarksDetector = FaceLandmarksDetector(number_of_triangles=triangles_number, fld_method=landmarks_detector)

        # Skin detector
        self.detectorSkin = skinDetector(fss_method=skin_segmentation)

        self.mp_face_mesh = mediapipe.solutions.face_mesh
        self.faceDetectorMediaPiPe = self.mp_face_mesh.FaceMesh(static_image_mode=True)

    def getFaceMediapipe(self, image, face_oval, landmarks):
        df = pd.DataFrame(list(face_oval), columns=["p1", "p2"])
        routes_idx = []

        p1 = df.iloc[0]["p1"]
        p2 = df.iloc[0]["p2"]

        for i in range(0, df.shape[0]):
            obj = df[df["p1"] == p2]
            p1 = obj["p1"].values[0]
            p2 = obj["p2"].values[0]

            route_idx = []
            route_idx.append(p1)
            route_idx.append(p2)
            routes_idx.append(route_idx)

        routes = []
        for source_idx, target_idx in routes_idx:
            source = landmarks[source_idx]
            target = landmarks[target_idx]

            relative_source = (int(image.shape[1] * source.x), int(image.shape[0] * source.y))
            relative_target = (int(image.shape[1] * target.x), int(image.shape[0] * target.y))

            # cv2.line(img, relative_source, relative_target, (255, 255, 255), thickness = 2)

            routes.append(relative_source)
            routes.append(relative_target)

        mask = cv2.fillConvexPoly(np.zeros((image.shape[0], image.shape[1])), np.array(routes), 1).astype(bool)
        out = np.zeros_like(image)
        out[mask] = image[mask]
        p1, p2 = np.array(routes).min(axis=0), np.array(routes).max(axis=0)
        cropped = out[int(p1[1]):int(p2[1]), int(p1[0]):int(p2[0])]

        return cropped

    def find_intersection(self, point1, point2, point3, point4):
        x_diff = (point1[0] - point2[0], point3[0] - point4[0])
        y_diff = (point1[1] - point2[1], point3[1] - point4[1])

        def determinant(a, b):
            return a[0] * b[1] - a[1] * b[0]

        div = determinant(x_diff, y_diff)

        if div == 0:
            raise Exception("Lines do not intersect")

        d = (determinant(point1, point2), determinant(point3, point4))
        x = determinant(d, x_diff) / div
        y = determinant(d, y_diff) / div

        return x, y

    def alignment_angle(self, left_eye, right_eye):

        left_eye_x, left_eye_y = left_eye
        right_eye_x, right_eye_y = right_eye

        # find rotation direction
        if left_eye_y > right_eye_y:
            point_3rd = (right_eye_x, left_eye_y)
            direction = -1  # rotate same direction to clock
        else:
            point_3rd = (left_eye_x, right_eye_y)
            direction = 1  # rotate inverse direction of clock

        # find length of triangle edges
        a = np.linalg.norm(np.array(left_eye) - np.array(point_3rd))
        b = np.linalg.norm(np.array(right_eye) - np.array(point_3rd))
        c = np.linalg.norm(np.array(right_eye) - np.array(left_eye))

        # apply cosine rule
        if b != 0 and c != 0:  # this multiplication causes division by zero in cos_a calculation

            cos_a = (b * b + c * c - a * a) / (2 * b * c)
            angle = np.arccos(cos_a)  # angle in radian
            angle = (angle * 180) / np.pi  # radian to degree

            if direction == -1:
                angle = 90 - angle
            # img = rotate_image(img, direction*angle)

            return direction * angle

        return 0

    def rotate_image(self, img, angle):
        # dividing height and width by 2 to get the center of the image
        height, width = img.shape[:2]
        # get the center coordinates of the image to create the 2D rotation matrix
        center = (width // 2, height // 2)

        # using cv2.getRotationMatrix2D() to get the rotation matrix
        rotate_matrix = cv2.getRotationMatrix2D(center=center, angle=angle, scale=1.0)

        # rotate the image using cv2.warpAffine
        rotated_image = cv2.warpAffine(src=img, M=rotate_matrix, dsize=(width, height))

        return rotated_image

    def get_aligned_face(self, frame, previous_landmarks):
        angle = 0
        # Face Normalization
        eyes_indices = [33, 133, 145, 159, 362, 263, 374, 386]
        face_oval = self.mp_face_mesh.FACEMESH_FACE_OVAL
        face_noaligned = np.zeros_like(frame)

        results = self.faceDetectorMediaPiPe.process(frame)
        landmarks = results.multi_face_landmarks

        if landmarks is None:
            landmarks = previous_landmarks

        if landmarks is not None:
            # Retrieve values at specified indices
            eyes_landmarks = [(landmarks[0].landmark[index].x, landmarks[0].landmark[index].y) for index in eyes_indices]

            le1, le2 = self.find_intersection(eyes_landmarks[0], eyes_landmarks[1], eyes_landmarks[2], eyes_landmarks[3])
            re1, re2 = self.find_intersection(eyes_landmarks[4], eyes_landmarks[5], eyes_landmarks[6], eyes_landmarks[7])

            face_noaligned = self.getFaceMediapipe(frame, face_oval, landmarks[0].landmark)

            # angle = alignment_angle((le1*face_noaligned.shape[1],le2*face_noaligned.shape[0]),
            # (re1*face_noaligned.shape[1],re2*face_noaligned.shape[0]))
            angle = self.alignment_angle((le1 * frame.shape[1], le2 * frame.shape[0]),
                                         (re1 * frame.shape[1], re2 * frame.shape[0]))

            previous_landmarks = landmarks

        if angle != 0:
            face_aligned = self.rotate_image(face_noaligned, angle)
        else:
            face_aligned = face_noaligned

        return face_aligned, previous_landmarks

    def __extractLandmarksSkinSignal(self, face_array, factor_resolution=None):
        """ Extract R,G,B values from skin-based roi of a frame subset """

        processedFaces = np.array(face_array)

        assert processedFaces.size > 0, "Faces are not processed yet! Please call runOffline first"

        faceSignal = []
        faceSignalWholeFace = []

        resize_input = False
        if factor_resolution is not None:
            resize_input = True  # Para experimentos de cambiar la resolución

        time_0 = time.time()

        multiregion_temp = self.multiregion
        i = 0
        for face in processedFaces:
            # print("Info extractSkinSignal. i:{}, r:{}, N:{}, ALGO:{}".format(i, r, len(frameSubset), i + starting_point - startFrame))
            height, width, _ = face.shape
            if resize_input:
                face = cv2.resize(face, (int(width / factor_resolution), int(height / factor_resolution)), interpolation=cv2.INTER_CUBIC)

            rois = []
            # cv2.imshow("SkinSignal-Seg", face)

            # lower = np.array([0, 40, 60], dtype="uint8")
            # upper = np.array([20, 255, 255], dtype="uint8")
            # converted = cv2.cvtColor(face, cv2.COLOR_BGR2HSV)
            # skinMask = cv2.inRange(converted, lower, upper)
            # skinFace = cv2.bitwise_and(face, face, mask=skinMask)
            skinFace = face.copy()

            if multiregion_temp:
                ht, wt, _ = skinFace.shape
                slice_regions = self.slice_regions
                stepy = ht // slice_regions
                stepx = wt // slice_regions
                faceSignalWholeFace.append([skinFace])
                for x in range(slice_regions):
                    for y in range(slice_regions):
                        roi = skinFace[y * stepy:(y + 1) * stepy, x * stepx:(x + 1) * stepx]
                        rois.append(roi)
                faceSignal.append(rois)

            else:
                faceSignalWholeFace.append([skinFace])
                faceSignal.append([skinFace])

            # ht, wt, _ = skinFace.shape
            # stepy = ht // self.slice_regions
            # stepx = wt // self.slice_regions
            # for x in range(self.slice_regions):
            #     x += 1
            #     cv2.line(skinFace, (x * stepx, 0), (x * stepx, ht), (0, 255, 255), 1)
            # for y in range(self.slice_regions):
            #     y += 1
            #     cv2.line(skinFace, (0, y * stepy), (wt, y * stepy), (0, 255, 255), 1)
            #
            # print("Info extractSkinSignal. Lrois:{}, i:{}".format(len(rois), i))
            # cv2.namedWindow("SkinSignal-Seg", cv2.WINDOW_NORMAL)
            # cv2.imshow("SkinSignal-Seg", skinFace)
            # if cv2.waitKey(0) & 0xFF == ord('q'):
            #     break
            #
            # i += 1

        total_time_skin_seg = time.time() - time_0
        # print(f"       >>>> [SPEED PERFORMANCE] Face Skin Segmentation: {total_time_skin_seg / len(processedFaces)}")


        return faceSignalWholeFace, faceSignal

    def getMeanRGB(self, regions_face_array, method="landmarks", video_index=0):

        n_frames = len(regions_face_array)
        n_roi = len(regions_face_array[0])
        time_0 = time.time()
        # print("        >>> getMeanRGB. n_frames:{}, n_roi:{}".format(n_frames, n_roi))
        if method == 'landmarks':
            if self.multiregion:
                rgb_matrix = np.zeros((n_roi, 3, n_frames))
                for i in range(n_frames):
                    for j, roi in enumerate(regions_face_array[i]):
                        # roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
                        norMask = cv2.cvtColor(roi, cv2.COLOR_RGB2GRAY)
                        _, norMaskThres = cv2.threshold(norMask, thresh=1, maxval=255, type=cv2.THRESH_BINARY)
                        mean_pixels = cv2.mean(roi, mask=norMaskThres)

                        if len(roi) == 0:
                            mean_rgb = 0
                        else:
                            mean_rgb = np.array([mean_pixels[0], mean_pixels[1], mean_pixels[2]])

                        rgb_matrix[j, :, i] = mean_rgb

                    # print("        >>> getMeanRGB. mean_rgb:{}, Type:{}, Lmean:{}".format(mean_rgb, type(mean_rgb), len(mean_rgb)))
                    # rgb.append(channels_rgb)

                # ESTUDIO DE CORRELACIÓN DE SEÑALES
                # Voy a calcular la media de todas las regiones, y luego hacer la correlación de la media con las señales de cada roi
                mean_total_rgb = np.zeros([3, n_frames])
                for i in range(n_frames):
                    mean_rgb = 0
                    for j in range(n_roi):
                        mean_rgb += rgb_matrix[j, :, i]

                    # mean_total_rgb[:, i] = mean_rgb / n_roi
                    mean_total_rgb[:, i] = mean_rgb

                # Salvamos las señales en CSV
                rawExtractedSignal = pd.DataFrame()
                self.existSignalsFile = False
                if not self.existSignalsFile:
                    # print("    PIM PIM PIM>>>>> RGB Signal. RGB Dim:{}, TOTAL dim:{}, NRois:{}".format(rgb_matrix.shape, mean_total_rgb.shape, n_roi))
                    rawExtractedSignal["raw_r_signal"] = mean_total_rgb[0, :]
                    rawExtractedSignal["raw_g_signal"] = mean_total_rgb[1, :]
                    rawExtractedSignal["raw_b_signal"] = mean_total_rgb[2, :]

                    for j in range(n_roi):
                        r_signal_name = "raw_r_signal_" + str(j)
                        g_signal_name = "raw_g_signal_" + str(j)
                        b_signal_name = "raw_b_signal_" + str(j)
                        rawExtractedSignal[r_signal_name] = rgb_matrix[j, 0, :]
                        rawExtractedSignal[g_signal_name] = rgb_matrix[j, 1, :]
                        rawExtractedSignal[b_signal_name] = rgb_matrix[j, 2, :]

                    output_path = f"{self.output_folder}/video_{video_index}"
                    filenamez = f"{output_path}/raw_rgb_signals.csv"
                    rawExtractedSignal.to_csv(filenamez, index=False, header=True, float_format='%.31f')

                rgb = mean_total_rgb

            else:
                rgb = np.zeros([3, n_frames])
                for i in range(n_frames):
                    mean_rgb = 0
                    channels_rgb = []
                    for roi in regions_face_array[i]:
                        # roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
                        norMask = cv2.cvtColor(roi, cv2.COLOR_RGB2GRAY)
                        _, norMaskThres = cv2.threshold(norMask, thresh=1, maxval=255, type=cv2.THRESH_BINARY)
                        mean_pixels = cv2.mean(roi, mask=norMaskThres)

                        # if cv2.waitKey(0) & 0xFF == ord('q'):
                        #     break

                        if len(roi) == 0:
                            mean_rgb += 0
                        else:
                            mean_rgb += np.array([mean_pixels[0], mean_pixels[1], mean_pixels[2]])

                    # print("        >>> getMeanRGB. mean_rgb:{}, Type:{}, Lmean:{}".format(mean_rgb, type(mean_rgb), len(mean_rgb)))
                    rgb[:, i] = mean_rgb / n_roi

                # Salvamos las señales en CSV
                rawExtractedSignal = pd.DataFrame()
                self.existSignalsFile = False
                if not self.existSignalsFile:
                    print("    PIM PIM PIM >>>>> RGB Signal in Different to Landmarks method..... ")
                    rawExtractedSignal["raw_r_signal"] = rgb[0, :]
                    rawExtractedSignal["raw_g_signal"] = rgb[1, :]
                    rawExtractedSignal["raw_b_signal"] = rgb[2, :]

                    output_path = f"{self.output_folder}/video_{video_index}"
                    filenamez = f"{output_path}/raw_rgb_signals.csv"
                    rawExtractedSignal.to_csv(filenamez, index=False, header=True, float_format='%.31f')

        else:
            rgb = np.zeros([3, n_frames])
            for i in range(n_frames):
                mean_rgb = 0
                channels_rgb = []
                for roi in regions_face_array[i]:

                    # roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
                    # cv2.imshow("Piel1", roi)
                    # if cv2.waitKey(0) & 0xFF == ord('q'):
                    #     break

                    norMask = cv2.cvtColor(roi, cv2.COLOR_RGB2GRAY)
                    _, norMaskThres = cv2.threshold(norMask, thresh=1, maxval=255, type=cv2.THRESH_BINARY)
                    mean_pixels = cv2.mean(roi, mask=norMaskThres)

                    if len(roi) == 0:
                        mean_rgb += 0
                    else:
                        mean_rgb += np.array([mean_pixels[0], mean_pixels[1], mean_pixels[2]])

                # print("        >>> getMeanRGB. mean_rgb:{}, Type:{}, Lmean:{}".format(mean_rgb, type(mean_rgb), len(mean_rgb)))
                rgb[:, i] = mean_rgb / n_roi

            rawExtractedSignal = pd.DataFrame()
            self.existSignalsFile = False
            if not self.existSignalsFile:
                print("    PIM PIM PIM >>>>> RGB Signal in Different to Landmarks method..... ")
                rawExtractedSignal["raw_r_signal"] = rgb[0, :]
                rawExtractedSignal["raw_g_signal"] = rgb[1, :]
                rawExtractedSignal["raw_b_signal"] = rgb[2, :]

                output_path = f"{self.output_folder}/video_{video_index}"
                filenamez = f"{output_path}/raw_rgb_signals.csv"
                rawExtractedSignal.to_csv(filenamez, index=False, header=True, float_format='%.31f')

        total_time_skin2signal = time.time() - time_0
        # print(f"       >>>> [SPEED PERFORMANCE] Face Skin2rawRGBsignal: {total_time_skin2signal / n_frames}")

        return rgb, rawExtractedSignal

    def getConvertedSignals(self, rawExtractedSignal, method='landmarks', detrendmethod='scipy', detrLambda=10):

        n_frames = rawExtractedSignal.shape[0]
        n_roi = rawExtractedSignal.shape[1] // 3 - 1
        convertedExtractedSignal = pd.DataFrame()

        np.random.seed(222)
        window_type = ('kaiser', self.kaiser_beta)

        if self.bandpassfiltering:
            if self.use_iir_filter:
                b, a = butter(self.iir_filter_order, Wn=[self.minHz, self.maxHz], fs=self.frameRate, btype='bandpass')
            else:
                b = self.filtering.bandpass_firwin(self.filter_order, self.minHz, self.maxHz, self.frameRate, window=window_type)
                a = 1

        # print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NUMBER OF VALID REGIONS en conversion: {}".format(n_roi))

        time_0 = time.time()
        total_time_rgb2bvp_conv = 0
        total_time_rgb2bvp_filt = 0
        if method == 'landmarks':

            if self.multiregion:

                region_rgb = np.zeros([3, n_frames])
                global_rgb = np.zeros([3, n_frames])

                global_rgb[0, :] = rawExtractedSignal["raw_r_signal"]
                global_rgb[1, :] = rawExtractedSignal["raw_g_signal"]
                global_rgb[2, :] = rawExtractedSignal["raw_b_signal"]
                first_time = True  # Para procesar solo una vez senhal la global

                valid_regions = []
                n_valid_regions = 0
                for j in range(n_roi):
                    r_signal_name = "raw_r_signal_" + str(j)
                    g_signal_name = "raw_g_signal_" + str(j)
                    b_signal_name = "raw_b_signal_" + str(j)
                    region_rgb[0, :] = rawExtractedSignal[r_signal_name]
                    region_rgb[1, :] = rawExtractedSignal[g_signal_name]
                    region_rgb[2, :] = rawExtractedSignal[b_signal_name]

                    partial_mean_rgb = (region_rgb[0, :] + region_rgb[1, :] + region_rgb[2, :]) / 3

                    if np.sum(partial_mean_rgb) > 0 and not np.isnan(np.sum(partial_mean_rgb)):
                        valid_regions.append(j)
                        n_valid_regions += 1

                ssr_j = 0
                # print(" Vamos a ver.... Range: {}. Len: {}. LenV: {}".format(range(n_valid_regions), n_valid_regions, len(valid_regions)))

                for j in range(n_valid_regions):
                    r_signal_name = "raw_r_signal_" + str(valid_regions[j])
                    g_signal_name = "raw_g_signal_" + str(valid_regions[j])
                    b_signal_name = "raw_b_signal_" + str(valid_regions[j])

                    region_signal_name = "region_signal_" + str(j)

                    region_rgb[0, :] = rawExtractedSignal[r_signal_name]
                    region_rgb[1, :] = rawExtractedSignal[g_signal_name]
                    region_rgb[2, :] = rawExtractedSignal[b_signal_name]


                    if self.prefiltering:
                        if self.zeroMeanSTDnorm:
                            region_rgb = self.filtering.zeroMeanSTDnorm(region_rgb)
                            if first_time:
                                global_rgb = self.filtering.zeroMeanSTDnorm(global_rgb)

                        # if vid != 44:
                        if self.detrending:
                            if detrendmethod == 'tarvainen':
                                region_rgb[0, :] = self.filtering.detrend(region_rgb[0, :], detrLambda, method='tarvainen')
                                region_rgb[1, :] = self.filtering.detrend(region_rgb[1, :], detrLambda, method='tarvainen')
                                region_rgb[2, :] = self.filtering.detrend(region_rgb[2, :], detrLambda, method='tarvainen')
                                if first_time:
                                    global_rgb[0, :] = self.filtering.detrend(global_rgb[0, :], detrLambda, method='tarvainen')
                                    global_rgb[1, :] = self.filtering.detrend(global_rgb[1, :], detrLambda, method='tarvainen')
                                    global_rgb[2, :] = self.filtering.detrend(global_rgb[2, :], detrLambda, method='tarvainen')
                            else:
                                region_rgb = self.filtering.detrend(region_rgb, detrLambda)
                                if first_time:
                                    global_rgb = self.filtering.detrend(global_rgb, detrLambda)

                        if self.bandpassfiltering:
                            region_rgb = lfilter(b, a, region_rgb)
                            if first_time:
                                global_rgb = lfilter(b, a, global_rgb)

                        t1 = time.time()

                        if first_time:
                            P = self.colorTransformation.apply(global_rgb)
                            convertedExtractedSignal["global_signal"] = P

                        P = self.colorTransformation.apply(region_rgb)
                        if not np.isnan(np.sum(P)):
                            region_signal_name = "region_signal_" + str(ssr_j)
                            convertedExtractedSignal[region_signal_name] = P
                            ssr_j += 1

                        time_consumed = time.time() - t1

                        # print("Time consumed by method {} for region {} signal with {} frames (fps={}) is {}".format(method, j, n_frames, self.frameRate, time_consumed))
                    else:
                        t1 = time.time()
                        if first_time:
                            global_rppg = self.colorTransformation.apply(global_rgb)

                        region_rppg = self.colorTransformation.apply(region_rgb)

                        time_consumed = time.time() - t1
                        # print("Time consumed by method {} for region {} signal with {} frames (fps={}) is {}".format(method, j, n_frames, self.frameRate, time_consumed))


                        if first_time:
                            if self.detrending:
                                global_rppg = self.filtering.detrend(global_rppg)

                            if self.bandpassfiltering:
                                convertedExtractedSignal["global_signal"] = lfilter(b, a, global_rppg)
                            else:
                                convertedExtractedSignal["global_signal"] = global_rppg

                        if not np.isnan(np.sum(region_rppg)):

                            if self.detrending:
                                region_rppg = self.filtering.detrend(region_rppg)

                            if self.bandpassfiltering:
                                convertedExtractedSignal[region_signal_name] = lfilter(b, a, region_rppg)
                            else:
                                convertedExtractedSignal[region_signal_name] = region_rppg

                    first_time = False

        return convertedExtractedSignal




    def extract_ppg_from_face(self, video_path, input_frame_rate=None, video_index=0, total_number_of_videos=1):
        # Starting point
        video = video_path.split('/')
        print("    >> Starting to extract rPPG from video: " + str(video[-1]))
        print("    >> Video path: " + str(video_path))

        # TODO: Maybe there is a better way of doing this
        user_folder = f"{self.output_folder}/video_{video_index}"
        if not os.path.exists(user_folder):
            os.makedirs(user_folder)

        # For saving results in HDF5. TimeStamp. Note: It could be taken before saving the results.
        dateTime = datetime.now()
        timestampStr = dateTime.strftime("%d-%b-%Y")

        # Record data video. Containers for saving the information. TODO: More efficient containers??
        monster_faces = []
        cropped_faces = []
        mediapipe_rectification_faces = []

        df_signals = pd.DataFrame()
        face_detected = []
        frame_number = []
        number_of_faces = []

        # Video Reader
        cap = cv2.VideoCapture(video_path)
        hasFrame, frame = cap.read()
        if not hasFrame:
            exit(-1)

        # TODO: Hardcoded. Needs to be reviewed
        rotate_code = None
        # rotate_code = check_rotation(video_path)

        main_height, main_width, channels_main = frame.shape
        length_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        # rotation_meta = int(cap.get(cv2.CAP_PROP_ORIENTATION_META))

        print(f"    >> Input video length: {length_frames} frames.")
        if input_frame_rate is not None:
            recording_fps = input_frame_rate
        else:
            recording_fps = cap.get(cv2.CAP_PROP_FPS)
        fs = recording_fps

        ##########################################
        # Detectors settings and initializations
        ##########################################
        self.faceDetector.setParameters(width=main_width, height=main_height)

        # Landmark detector
        self.faceLandmarksDetector.setParameters(width=main_width, height=main_height, image_data_type=frame.dtype)

        # Filtering
        self.filtering = Filtering(fps=fs)

        # Color method
        if self.ppg_method == "POS":
            self.colorTransformation = pos_method(fps=fs)
        elif self.ppg_method == "CHROM":
            self.colorTransformation = chrom_method(fps=fs)
        elif self.ppg_method == "LGI":
            self.colorTransformation = lgi_method(fps=fs)
        elif self.ppg_method == "OMIT":
            self.colorTransformation = omit_method(fps=fs)
            self.prefiltering = False
        else:
            self.colorTransformation = pos_method(fps=fs)

        print(f"    >> Using: {self.ppg_method} as rPPG method...")

        # colorTransformation = pos_method(fps=fs)

        # Create a plotter class object. TODO: It needs be arranged according to the input resolution.
        if main_width > main_height:
            plot_width = main_width // 2
            plot_height = main_height // 4
            text_x_position = int(main_height * 0.02)
            text_y_position = int(main_height * 0.04)
        else:
            plot_width = main_height // 2
            plot_height = main_width // 4
            text_x_position = int(main_width * 0.02)
            text_y_position = int(main_width * 0.04)

        # General Variables
        frame_count = 0
        frame_count_detection = 0
        tt_FaceDetector = 0
        tt_FaceLandmarksDetector = 0
        tt_FaceSkinDetector = 0
        face_triangles_number = self.triangles_number
        old_face_detection = None
        previous_landmarks = None

        # Visual settings
        if self.show_output:
            cv2.namedWindow("Video Output", cv2.WINDOW_NORMAL)
            cv2.startWindowThread()

        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)

        ##################################
        #
        #         FACE EXTRACTION
        #
        ##################################
        while 1:

            if not hasFrame:
                break

            frame_number.append(frame_count)
            if rotate_code is not None:
                frame = cv2.rotate(frame, rotate_code)

            output_frame = frame.copy()  # Frame for drawing results

            if self.show_output:
                cv2.putText(output_frame, "{}/{} processed frames.".format(frame_count, length_frames), (text_x_position, text_y_position * 4), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            (0, 0, 255),
                            1, cv2.LINE_AA)

            #####################
            #   Face Detection
            #####################
            t1 = time.time()
            bboxes = self.faceDetector.detectFaces(frame)
            number_of_faces.append(len(bboxes))
            tt_FaceDetector += time.time() - t1
            fpsFaceDetector = frame_count / tt_FaceDetector



            if self.show_output:
                label = self.faceDetector.getLabelFaceDetector(fpsFaceDetector)
                cv2.putText(output_frame, label, (text_x_position, text_y_position), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

            # Print face rectangles
            getFirstFace = True  # TODO: Temporal. Get only first face for normalization.
            for box in bboxes:
                if getFirstFace:
                    cv2.rectangle(output_frame, (box[0], box[1], box[2], box[3]), (0, 255, 0), int(round(output_frame.shape[0] * 0.001)))
                    faceRectImage, adaptedFaceRect, rect_crop_face = self.faceDetector.getFaceInRect(frame, box)
                    face_detected.append(int(1))
                    getFirstFace = False

            # Face Landmarks detection
            if len(bboxes) > 0:

                ################################
                #   Face Landmarks detection
                ################################
                t2 = time.time()
                selected_box = self.faceDetector.selectBiggestFaceRect(bboxes)
                faceRectImage, adaptedFaceRect, rect_crop_face = self.faceDetector.getFaceInRect(frame, selected_box)
                status, landmarks = self.faceLandmarksDetector.detectLandmarks2(frame, [adaptedFaceRect], face_detector=self.face_detector_name)
                cropped_faces.append(faceRectImage)

                lm_x, lm_y = self.faceLandmarksDetector.transformLandmars2arrays(0)
                landmarks85 = self.faceLandmarksDetector.get85Landmarks(frame, 0)
                tt_FaceLandmarksDetector += time.time() - t2
                fpsFaceLandmarksDetector = frame_count / tt_FaceLandmarksDetector

                if self.show_output:
                    label = self.faceLandmarksDetector.getLabelFaceLandmarksDetector(fpsFaceLandmarksDetector)
                    cv2.putText(output_frame, label, (text_x_position, text_y_position * 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

                ###########################
                #   Face Normalization
                ##########################
                normalizedFace = self.faceLandmarksDetector.getNormalizedFace(faceRectImage, np.array([landmarks85[0]]), rect_crop_face, number_of_triangles=131)

                monster_faces.append(normalizedFace)
                old_face_detection = selected_box.copy()

                face_aligned, previous_landmarks = self.get_aligned_face(frame, previous_landmarks)
                mediapipe_rectification_faces.append(face_aligned)

                # cv2.namedWindow("Cropped Face", cv2.WINDOW_NORMAL)
                # cv2.namedWindow("Monster Face", cv2.WINDOW_NORMAL)
                # cv2.namedWindow("Face Aligned", cv2.WINDOW_NORMAL)
                # cv2.imshow("Cropped Face", faceRectImage)
                # cv2.imshow("Monster Face", normalizedFace)
                # cv2.imshow("Face Aligned", face_aligned)
                # if cv2.waitKey(0) & 0xFF == ord('q'):
                #     break


            else:

                print("           -> Skipped frame {}. Not face detected...".format(int(frame_count)))

                if old_face_detection is None:
                    monster_faces.append(self.faceLandmarksDetector.getEmptyMonsterFace())
                    mediapipe_rectification_faces.append(np.zeros_like(frame))
                    cropped_faces.append(np.zeros_like(frame))
                else:
                    faceRectImage, adaptedFaceRect, rect_crop_face = self.faceDetector.getFaceInRect(frame, old_face_detection)
                    cropped_faces.append(faceRectImage)

                    status, landmarks = self.faceLandmarksDetector.detectLandmarks(frame, [adaptedFaceRect])
                    lm_x, lm_y = self.faceLandmarksDetector.transformLandmars2arrays(0)
                    landmarks85 = self.faceLandmarksDetector.get85Landmarks(frame, 0)

                    normalizedFace = self.faceLandmarksDetector.getNormalizedFace(faceRectImage, np.array([landmarks85[0]]), rect_crop_face, number_of_triangles=131)
                    monster_faces.append(normalizedFace)
                    face_aligned, previous_landmarks = self.get_aligned_face(frame, previous_landmarks)
                    mediapipe_rectification_faces.append(face_aligned)

            if frame_count == 1:
                tt_FaceDetector = 0

            frame_count += 1
            if len(bboxes) > 0:
                frame_count_detection += 1

            k = cv2.waitKey(1)
            if k == 27:
                break
            printProgressBar(frame_count, length_frames, prefix=f"          >> Progress for video {video_index}/{total_number_of_videos}", suffix="of the video frames.")

            hasFrame, frame = cap.read()

        ##################################
        #
        #       SIGNAL EXTRACTION
        #
        ##################################

        normalized_faces_array, regions_faces_array = self.__extractLandmarksSkinSignal(monster_faces)
        global_rgb_signal, all_rgb_dataframe = self.getMeanRGB(regions_faces_array, video_index=video_index)
        convertedSignals = self.getConvertedSignals(rawExtractedSignal=all_rgb_dataframe)
        rppg_signal = convertedSignals["global_signal"]


        ##################################
        #
        #         SAVING RESULTS
        #
        ##################################


        if self.save_results:
            output_path = f"{self.output_folder}/video_{video_index}"
            filenamez = f"{output_path}/rPPG_signals_{self.ppg_method.upper()}.csv"
            convertedSignals.to_csv(filenamez, index=False, header=True, float_format='%.31f')

        output_path = f"{self.output_folder}/video_{video_index}/images"
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        for z in range(len(mediapipe_rectification_faces)):
            if z % 100 == 0:
                cv2.imwrite(f"{output_path}/normalized_face_{z}.jpg", monster_faces[z])
                cv2.imwrite(f"{output_path}/oval_face_{z}.jpg", mediapipe_rectification_faces[z])
                cv2.imwrite(f"{output_path}/cropped_face_{z}.jpg", cropped_faces[z])


        # Save Dataframe results.
        print("    >> Finalized to extract rPPG from video: " + str(video[-1]))
        print("       Shape frame array: {}".format(len(frame_number)))


        df_signals["Frame number"] = frame_number
        df_signals["Face detected"] = face_detected

        print("    >> Saving results of video " + str(video[-1]) + " with " + str(len(frame_number)) + " frames...")


        if self.save_results:

            output_path = f"{self.output_folder}/video_{video_index}"
            video_name = video[-1].split(".")[0]
            filenamez = f"{output_path}/extraction_info_{video_name}_{timestampStr}"

            if self.format_results == "HDF5":
                store_hdf5 = df_signals.HDFStore(f"{filenamez}.h5")

            if self.format_results == "HDF5":
                store_hdf5.append(str(video[-1]).split(".")[0], df_signals)
            elif self.format_results == "CSV":
                df_signals.to_csv(f"{filenamez}.csv", index=False, header=True)

        if self.show_output:
            cv2.destroyAllWindows()

        print("\n")
        return rppg_signal
