# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Copyright (c) 2019 - 2023, Constantino Álvarez (CMVS - University of Oulu), Miguel
# Bordallo (CMVS - University of Oulu) and Face2PPG Contributors.
# All rights reserved.
#
# The full license is in the file LICENSE.txt, distributed with this software.
# -----------------------------------------------------------------------------


import os
import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import spectrogram, welch
import heartpy as hp
from hrvanalysis import get_time_domain_features, get_geometrical_features, get_sampen, get_csi_cvi_features, get_frequency_domain_features
import pyhrv.tools as tools
from scipy.signal import find_peaks, stft, lfilter, butter, welch, hilbert, firwin, lfilter_zi, filtfilt
from scipy.stats import pearsonr, spearmanr, kendalltau
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, mean_absolute_error, mean_squared_error, r2_score

sys.path.insert(0, os.path.abspath('../'))
from face2ppg.helpers.common import *



def mmslab_hpass_filter(input_signal, cutoff_freq, sampling_rate):
    """
    Apply a high-pass filter to an input signal.

    Parameters:
    - input_signal (numpy array): The input signal to be filtered.
    - cutoff_freq (float): The cutoff frequency of the high-pass filter in Hz.
    - sampling_rate (float): The sampling rate of the input signal in Hz.

    Returns:
    - filtered_signal (numpy array): The filtered output signal.
    """

    # Calculate the Nyquist frequency (half of the sampling rate)
    nyquist_freq = 0.5 * sampling_rate

    # Normalize the cutoff frequency by the Nyquist frequency
    cutoff_norm = cutoff_freq / nyquist_freq

    # Design a Butterworth high-pass filter
    # The second argument (4 in this case) is the filter order, you can adjust it as needed
    b, a = butter(4, cutoff_norm, btype='high', analog=False)

    # Apply the high-pass filter to the input signal
    filtered_signal = filtfilt(b, a, input_signal)

    return filtered_signal


def mmslab_lpass_filter(input_signal, cutoff_freq, sampling_rate):
    """
    Apply a low-pass filter to an input signal.

    Parameters:
    - input_signal (numpy array): The input signal to be filtered.
    - cutoff_freq (float): The cutoff frequency of the low-pass filter in Hz.
    - sampling_rate (float): The sampling rate of the input signal in Hz.

    Returns:
    - filtered_signal (numpy array): The filtered output signal.
    """

    # Calculate the Nyquist frequency (half of the sampling rate)
    nyquist_freq = 0.5 * sampling_rate

    # Normalize the cutoff frequency by the Nyquist frequency
    cutoff_norm = cutoff_freq / nyquist_freq

    # Design a Butterworth low-pass filter
    # The second argument (4 in this case) is the filter order, you can adjust it as needed
    b, a = butter(4, cutoff_norm, btype='low', analog=False)

    # Apply the low-pass filter to the input signal
    filtered_signal = filtfilt(b, a, input_signal)

    return filtered_signal


def bandpass_firwin(ntaps, lowcut, highcut, fs, window='hamming'):
    nyq = 0.5 * fs
    taps = firwin(ntaps, [lowcut, highcut], nyq=nyq, pass_zero=False, window=window, scale=False)
    return taps


def mmslab_bandpass_filter(input_signal, frameRate, minHz=0.5, maxHz=4.0):
    window_type = ('kaiser', 35)
    b = bandpass_firwin(10, minHz, maxHz, frameRate, window=window_type)
    a = 1
    filtered_signal = lfilter(b, a, input_signal)
    return filtered_signal


def get_hrv(input_signal, frameRate, prefix="hrv"):
    # heart_signal = input_signal.to_numpy()
    heart_signal = input_signal

    try:
        # working_data, heart_measures = hp.process(heart_signal, sample_rate=self.frameRate, bpmmin=45, bpmmax=220, windowsize=1.5)
        # working_data, heart_measures = hp.process(heart_signal, sample_rate=frameRate, bpmmin=50, bpmmax=220, windowsize=1.5, welch_wsize=10)
        working_data, heart_measures = hp.process(heart_signal, sample_rate=frameRate, bpmmin=50, bpmmax=220, windowsize=2.0)
        peaks = np.asarray(working_data['peaklist']) / frameRate


    except hp.exceptions.BadSignalWarning as w:
        # print(heart_signal)
        print(" Que pasa aquiiiii: {}".format(w))  # display message
        working_data = []
        heart_measures = {f'bpm': -999, f'ibi': -999, f'sdnn': -999,
                          f'sdsd': -999, f'rmssd': -999, f'pnn20': -999,
                          f'pnn50': -999, f'hr_mad': -999, f'sd1': -999,
                          f'sd2': -999, f's': -999, f'sd1/sd2': -999,
                          f'breathingrate': -999}
        pass  # or just pass over it if you want

    # print(heart_measures)

    # input("stoppp")

    heart_measures[f'{prefix}_PPG_bpm'] = heart_measures.pop('bpm')
    heart_measures[f'{prefix}_PPG_ibi'] = heart_measures.pop('ibi')
    heart_measures[f'{prefix}_PPG_sdnn'] = heart_measures.pop('sdnn')
    heart_measures[f'{prefix}_PPG_sdsd'] = heart_measures.pop('sdsd')
    heart_measures[f'{prefix}_PPG_rmssd'] = heart_measures.pop('rmssd')
    heart_measures[f'{prefix}_PPG_pnn20'] = heart_measures.pop('pnn20')
    heart_measures[f'{prefix}_PPG_pnn50'] = heart_measures.pop('pnn50')
    heart_measures[f'{prefix}_PPG_br'] = heart_measures.pop('breathingrate')

    if bool(working_data):

        nni = tools.nn_intervals(peaks)

        if len(nni) == 1:
            nni = np.append(nni, 100)

        try:
            time_domain_features = get_time_domain_features(nni)
            # Compute SDNN
            # non_linear_features = nl.nonlinear(rpeaks=peaks, sampling_rate=self.frameRate)
            heart_measures[f'{prefix}_PPG_nni20'] = time_domain_features['nni_20']
            heart_measures[f'{prefix}_PPG_nni50'] = time_domain_features['nni_50']
            heart_measures[f'{prefix}_PPG_median_nni'] = time_domain_features['median_nni']
            heart_measures[f'{prefix}_PPG_mean_nni'] = time_domain_features['mean_nni']
            heart_measures[f'{prefix}_PPG_range_nni'] = time_domain_features['range_nni']
            heart_measures[f'{prefix}_PPG_max_hr'] = time_domain_features['max_hr']
            heart_measures[f'{prefix}_PPG_min_hr'] = time_domain_features['min_hr']
            heart_measures[f'{prefix}_PPG_std_hr'] = time_domain_features['std_hr']


        except hp.exceptions.BadSignalWarning as w:
            print("pepito")
            heart_measures[f'{prefix}_PPG_nni20'] = -999
            heart_measures[f'{prefix}_PPG_nni50'] = -999
            heart_measures[f'{prefix}_PPG_median_nni'] = -999
            heart_measures[f'{prefix}_PPG_mean_nni'] = -999
            heart_measures[f'{prefix}_PPG_range_nni'] = -999
            heart_measures[f'{prefix}_PPG_max_hr'] = -999
            heart_measures[f'{prefix}_PPG_min_hr'] = -999
            heart_measures[f'{prefix}_PPG_std_hr'] = -999

            pass  # or just pass over it if you want

    return heart_measures


def compute_similarity(signal1, signal2):
    """
    Compute the Pearson correlation coefficient to measure the similarity between two signals.

    Parameters:
    - signal1 (array-like): First input signal.
    - signal2 (array-like): Second input signal.

    Returns:
    - similarity (float): Pearson correlation coefficient between the two signals.
    """
    # Ensure that the input signals have the same length
    if len(signal1) != len(signal2):
        raise ValueError("Input signals must have the same length.")

    # Compute the Pearson correlation coefficient
    # similarity, _ = pearsonr(signal1, signal2)
    # similarity, _ = spearmanr(signal1, signal2)
    similarity, _ = kendalltau(signal1, signal2)

    return similarity


from scipy.signal import correlate


def find_best_time_shift(series1, series2):
    """
    Find the best time shift (lag) to align series1 with series2 and compute the similarity.

    Parameters:
    - series1, series2: Input time series arrays of equal length.

    Returns:
    - best_shift: The optimal time shift (lag) for alignment.
    - similarity: The similarity score between the aligned time series.
    """

    # Ensure both series have the same length
    if len(series1) != len(series2):
        raise ValueError("Input time series must have the same length.")

    # Compute the cross-correlation between the two series
    cross_corr = correlate(series1, series2, mode='full')

    # Find the time lag (shift) that maximizes the cross-correlation
    best_shift = np.argmax(cross_corr) - len(series1) + 1

    # Compute the similarity as the maximum value of the cross-correlation
    similarity = np.max(cross_corr)

    return best_shift, similarity


from fastdtw import fastdtw
from scipy.spatial.distance import euclidean


def compute_similarity_dtw(time_series_1, time_series_2):
    # Convert the time series to numpy arrays
    ts1 = np.array(time_series_1).reshape(-1, 1)
    ts2 = np.array(time_series_2).reshape(-1, 1)

    # Compute DTW distance using the fastdtw library
    distance, _ = fastdtw(ts1, ts2, dist=euclidean)

    # Return the DTW distance as the measure of similarity
    # Note that a lower distance indicates higher similarity
    return distance/len(ts1)


def normalized_cross_correlation_similarity(ts1, ts2):
    # Normalizing time series
    ts1_normalized = (ts1 - np.mean(ts1)) / np.std(ts1)
    ts2_normalized = (ts2 - np.mean(ts2)) / np.std(ts2)

    # Calculate cross-correlation
    correlation = np.correlate(ts1_normalized, ts2_normalized, 'full')

    # Normalize by the length of the time series
    normalized_corr = correlation / len(ts1)
    # normalized_corr = correlation

    # Finding the max normalized correlation and corresponding time lag
    max_corr = np.max(normalized_corr)
    time_lag = np.argmax(normalized_corr) - (len(ts1) - 1)

    return max_corr, time_lag


def get_hr_welch(input_signal, sampling_rate):
    # Compute Welch of the signal
    F, P = welch(input_signal, nperseg=500, noverlap=300, window='hamming', fs=sampling_rate, nfft=4096)

    band = np.argwhere((F > 0.5) & (F < 4.0)).flatten()
    Pfreqs = 60 * F[band]
    Power = P[band]
    Pmax = np.argmax(Power)  # power max
    bpm = Pfreqs[Pmax]  # freq max
    return bpm


def quality_score(gt_ppg_signal, fake_ppg_signal, sample_rate):
    number_of_subscores = 0

    gt_hrv = get_hrv(gt_ppg_signal, frameRate=sample_rate)
    fake_hrv = get_hrv(fake_ppg_signal, frameRate=sample_rate)

    ######################
    #   Heart Rate (HR)
    ######################
    hr_gt = gt_hrv["hrv_PPG_bpm"]
    hr_fake = fake_hrv["hrv_PPG_bpm"]
    if np.isnan(hr_gt):
        hr_gt = get_hr_welch(gt_ppg_signal, sampling_rate=sample_rate)
    if np.isnan(hr_fake):
        hr_fake = get_hr_welch(fake_ppg_signal, sampling_rate=sample_rate)

    # 200 comes from (240 - 40) -> (Max HR and Min HR)
    nor_mape_hr = np.abs((hr_gt - hr_fake) / 200)
    score_hr = 1 - nor_mape_hr
    if 0 <= score_hr <= 1:
        number_of_subscores += 1
    else:
        score_hr = 0


    # ######################
    # #   SDNN
    # ######################
    # sdnn_gt = gt_hrv["hrv_PPG_sdnn"]
    # sdnn_fake = fake_hrv["hrv_PPG_sdnn"]
    # score_sdnn = 0
    # if np.isnan(sdnn_gt) or np.isnan(sdnn_fake):
    #     score_sdnn = 0
    # else:
    #     mape_sdnn = np.abs((sdnn_gt - sdnn_fake) / 200)
    #     score_sdnn = 1 - mape_sdnn
    #     number_of_subscores += 1


    #########################
    #   IBI N-to-N distance
    #########################
    ibi_gt = gt_hrv["hrv_PPG_ibi"]
    ibi_fake = fake_hrv["hrv_PPG_ibi"]
    score_ibi = 0
    if np.isnan(ibi_gt) or np.isnan(ibi_fake):
        score_ibi = 0
    else:
        mape_ibi = np.abs((ibi_gt - ibi_fake) / 1500)  # 1500 is the maximum distance in ms.
        score_ibi = 1 - mape_ibi
        number_of_subscores += 1


    #########################
    #   Similarity
    #########################
    similarity, _ = normalized_cross_correlation_similarity(gt_ppg_signal, fake_ppg_signal)
    score_similarity = 0
    if np.isnan(similarity):
        score_similarity = 0
    else:
        score_similarity = similarity
        number_of_subscores += 1

    # print(f"Similarity: {score_similarity}")

    #########################
    #   Similarity DTW
    #########################
    distance_dtw = compute_similarity_dtw(gt_ppg_signal, fake_ppg_signal)
    score_similarity_dtw = 0
    if np.isnan(distance_dtw):
        score_similarity = 0
    else:
        score_similarity_dtw = 1 - distance_dtw
        number_of_subscores += 1

    # print(f"Similarity DTW: {score_similarity_dtw}")

    ######################
    #   Final SCORE
    ######################
    quality_score = score_hr + score_ibi + score_similarity + score_similarity_dtw
    quality_score /= number_of_subscores

    return quality_score


def main():
    print('\n')
    print('********************************************************************')
    print('*        FACE2PPG: Test Quality Score Function  v0.01 Beta         *')
    print('********************************************************************')
    print('\n')

    # Load biosignals
    data_input_path = "../data/signals"
    gt_ppg_signals = np.load(f"{data_input_path}/GT.npy")
    rppg_signals = np.load(f"{data_input_path}/input.npy")
    fake_ppg_signals = np.load(f"{data_input_path}/fake.npy")

    # Configuration
    sample_rate = 51.2  # Fixed by Manu.
    number_of_signals_in_container = 280  # Fixed by Honghan

    # Containers parameters
    scores_fakes = []
    scores_rppgs = []

    # Main Loop
    for i in range(number_of_signals_in_container):
        print(f" - Processing window number {i}...")
        print(f"        * Window range: {512 * i} to {512 * i + 512} // ({len(gt_ppg_signals)})")
        window_gt_ppg = gt_ppg_signals[512 * i:512 * i + 512]
        window_fake_ppg = fake_ppg_signals[512 * i:512 * i + 512]
        window_rppg = rppg_signals[512 * i:512 * i + 512]

        # Filtering of the rPPG signals. To ensure that they are in the proper band
        window_rppg = mmslab_hpass_filter(window_rppg, 0.5, sampling_rate=sample_rate)
        window_rppg = mmslab_lpass_filter(window_rppg, 3.9, sampling_rate=sample_rate)

        score_fake = quality_score(window_gt_ppg, window_fake_ppg, sample_rate)
        scores_fakes.append(score_fake)
        print(f"    >> SCORE Fake PPG {i}: {score_fake}")

        score_rppg = quality_score(window_gt_ppg, window_rppg, sample_rate)
        scores_rppgs.append(score_rppg)
        print(f"    >> SCORE rPPG {i}: {score_rppg}")
        print("\n")
        input("stop")

if __name__ == "__main__":
    main()
