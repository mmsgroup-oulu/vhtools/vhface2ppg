''''| Author: Constantino Alvarez Casado
	| Date: 27/05/2020
	| 
'''

import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from face2ppg.segmentation.UNET import UNet

import argparse
import logging
import os
import time

import torch
import torch.nn.functional as F
from PIL import Image
from torchvision import transforms
from face2ppg.helpers.dataset import BasicDataset



def plot_img_and_mask(img, mask):
    classes = mask.shape[2] if len(mask.shape) > 2 else 1
    fig, ax = plt.subplots(1, classes + 1)
    ax[0].set_title('Input image')
    ax[0].imshow(img)
    if classes > 1:
        for i in range(classes):
            ax[i + 1].set_title(f'Output mask (class {i + 1})')
            ax[i + 1].imshow(mask[:, :, i])
    else:
        ax[1].set_title(f'Output mask')
        ax[1].imshow(mask)
    plt.xticks([]), plt.yticks([])
    plt.show()


def mask_to_image(mask):
    return Image.fromarray((mask * 255).astype(np.uint8))


def predict_img(net,
                full_img,
                device,
                scale_factor=1,
                out_threshold=0.5):
    net.eval()

    img = torch.from_numpy(BasicDataset.preprocess(full_img, scale_factor))

    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        output = net(img)

        if net.n_classes > 1:
            probs = F.softmax(output, dim=1)
        else:
            probs = torch.sigmoid(output)

        probs = probs.squeeze(0)

        tf = transforms.Compose(
            [
                transforms.ToPILImage(),
                transforms.Resize(full_img.size[1]),
                transforms.ToTensor()
            ]
        )

        probs = tf(probs.cpu())
        full_mask = probs.squeeze().cpu().numpy()

    return full_mask > out_threshold


class skinDetector(object):

    # class constructor
    def __init__(self, fss_method='uNET'):
        self.fss_method = fss_method
        self.range = 30
        self.scale = 0.5
        self.mask_threshold = 0.5
        self.model = "../models/skin_segmentation/vh_skindnn_model_1_03g.pth"
        print("  >> Loading VHDeepSkinUNET model for skin segmentation...")
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("       - Using device {}".format(self.device))
        self.net = UNet(n_channels=3, n_classes=1)

        self.net.to(device=self.device)
        self.net.load_state_dict(torch.load(self.model, map_location=self.device))
        print("       - Skin Segmentation Unet loaded!")

    def find_skin_fast(self, img):
        # converting from gbr to hsv color space
        img_HSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        # skin color range for hsv color space
        HSV_mask = cv2.inRange(img_HSV, (0, 15, 0), (17, 170, 255))
        HSV_mask = cv2.morphologyEx(HSV_mask, cv2.MORPH_OPEN, np.ones((3, 3), np.uint8))

        # converting from gbr to YCbCr color space
        img_YCrCb = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
        # skin color range for hsv color space
        YCrCb_mask = cv2.inRange(img_YCrCb, (0, 135, 85), (255, 200, 135))
        YCrCb_mask = cv2.morphologyEx(YCrCb_mask, cv2.MORPH_OPEN, np.ones((3, 3), np.uint8))

        # merge skin detection (YCbCr and hsv)
        global_mask = cv2.bitwise_and(YCrCb_mask, HSV_mask)
        global_mask = cv2.medianBlur(global_mask, 3)
        global_mask = cv2.morphologyEx(global_mask, cv2.MORPH_OPEN, np.ones((3, 3), np.uint8))

        self.HSV_result = cv2.bitwise_not(HSV_mask)
        self.YCrCb_result = cv2.bitwise_not(YCrCb_mask)
        self.global_result = cv2.bitwise_not(global_mask)
        return self.global_result

    def find_skin(self, img):
        t1 = time.time()
        im = Image.fromarray(img, 'RGB')
        # print(type(im))
        # print(im.size)
        #
        # print(type(img))
        # print(img.shape)
        mask = predict_img(net=self.net, full_img=im, scale_factor=self.scale, out_threshold=self.mask_threshold, device=self.device)

        result = np.array((mask * 255).astype(np.uint8))
        # result2 = Image.fromarray((mask * 255).astype(np.uint8))
        # result2.save("out_mask.jpg")
        # # plot_img_and_mask(im, mask)
        maski = Image.fromarray(result.astype(np.uint8))
        open_cv_image = np.array(maski)
        main_height, main_width, _ = img.shape
        open_cv_image2 = cv2.resize(open_cv_image, (main_width, main_height))
        # print(type(open_cv_image))
        # print(open_cv_image.shape)
        # cv2.imshow("Algo", open_cv_image)

        print("Tiempo unet: {}".format(time.time() - t1))

        return open_cv_image2

    def getLabelFaceSkinDetector(self, time):
        label = ""
        if self.fss_method == "uNET":
            label = "Skin segmentation: UNET - FPS : {:.2f}".format(time)
        elif self.fss_method == "LANDMARKS":
            label = "Skin segmentation: LANDMARKS - FPS : {:.2f}".format(time)
        elif self.fss_method == "DLIB_ERT":
            label = "Skin segmentation: COLOR - FPS : {:.2f}".format(time)
        else:
            abel = "Skin segmentation - FPS : {:.2f}".format(time)
        return label
